<?php

use Illuminate\Database\Seeder;
use ELends\Item;
use ELends\Category;
use ELends\Ad;
use ELends\Booking;
use ELends\FavoriteAd;
use ELends\Interest;
use ELends\ServiceCategory;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */


    public function run()
    {
        $cat1 = new Category();
        $cat1->image = "http://static.wixstatic.com/media/763ce0_f2527951693b4bc792a802f8f6d367f3~mv2.gif";
        $cat1->category = "Bridal Wear";
        $cat1->save();

        $cat2 = new Category();
        $cat2->image = "http://pluspng.com/img-png/wedding-dress-and-tux-png-pin-black-dress-clipart-wedding-suit-6-600.png";
        $cat2->category = "Groom Wear";
        $cat2->save();

        $cat3 = new Category();
        $cat3->image = "https://cdn3.iconfinder.com/data/icons/healthy-and-fitness/512/Health-Fitness-Bodybuilding-sport-shoes-pair-run-512.png";
        $cat3->category = "Shoes";
        $cat3->save();

        $cat4 = new Category();
        $cat4->image = "https://cdn1.iconfinder.com/data/icons/jewery/500/necklace-512.png";
        $cat4->category = "Jewelery";
        $cat4->save();

       for($j=0; $j<=10; $j++){
           $item = new Item();
           $item->name  = 'Product1 '.($j+1);
           $item->description  = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.';
           $item->color  = 'Gray';
           $item->size  = 'Medium';
           $item->category_id  = $cat1->id;
           $item->user_id  = 1;
           $item->save();
           for($k=0; $k<4; $k++){
               $itemPicture = new \ELends\ItemPictures();
               $itemPicture->item_id = $item->id;
               $itemPicture->path  = 'images/p'.($k+1).'.jpg';
               $itemPicture->save();
           }
       }

        for($j=0; $j<=10; $j++){
            $item = new Item();
            $item->name  = 'Product1 '.($j+1);
            $item->description  = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.';
            $item->color  = 'Gray';
            $item->size  = 'Medium';
            $item->user_id  = 2;
            $item->category_id  = $cat2->id;
            $item->save();
            for($k=0; $k<4; $k++){
                $itemPicture = new \ELends\ItemPictures();
                $itemPicture->item_id = $item->id;
                $itemPicture->path  = 'images/p'.($k+1).'.jpg';
                $itemPicture->save();
            }
        }

        for($j=0; $j<=10; $j++){
            $item = new Item();
            $item->name  = 'Product1'.($j+1);
            $item->description  = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.';
            $item->category_id  = $cat3->id;
            $item->user_id  = 3;
            $item->color  = 'Gray';
            $item->size  = 'Medium';
            $item->save();
            for($k=0; $k<4; $k++){
                $itemPicture = new \ELends\ItemPictures();
                $itemPicture->item_id = $item->id;
                $itemPicture->path  = 'images/p'.($k+1).'.jpg';
                $itemPicture->save();
            }
        }

        for($j=0; $j<=10; $j++){
            $item = new Item();
            $item->name  = 'Product1'.($j+1);
            $item->user_id  = 1;
            $item->description  = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.';
            $item->color  = 'Gray';
            $item->size  = 'Medium';
            $item->category_id  = $cat4->id;
            $item->save();

            for($k=0; $k<4; $k++){
                $itemPicture = new \ELends\ItemPictures();
                $itemPicture->item_id = $item->id;
                $itemPicture->path  = 'images/p'.($k+1).'.jpg';
                $itemPicture->save();
            }
        }

        $serv_category = new \ELends\ServiceCategory();
        $serv_category->name = "Marriage Hall";
        $serv_category->save();

        $serv_category = new \ELends\ServiceCategory();
        $serv_category->name = "Catering";
        $serv_category->save();

        $serv_category = new \ELends\ServiceCategory();
        $serv_category->name = "Transport";
        $serv_category->save();

        $serv_category = new \ELends\ServiceCategory();
        $serv_category->name = "Movie Maker";
        $serv_category->save();

        $serv_category = new \ELends\ServiceCategory();
        $serv_category->name = "Photographer";
        $serv_category->save();

        $serv_category = new \ELends\ServiceCategory();
        $serv_category->name = "Lightening";
        $serv_category->save();




//        $serv = new \ELends\Service();
//        $serv->name = 'Catering Service';
//        $serv->service_category_id = 2;
//        $serv->image  = 'images/s1.jpg';
//        $serv->detail = 'lorem ipsum gud well';
//        $serv->user_id = 1;
//        $serv->save();
//
//        $serv = new \ELends\Service();
//        $serv->name = 'Lightening Service';
//        $serv->service_category_id = 6;
//        $serv->image  = 'images/s2.jpg';
//        $serv->detail = 'lorem ipsum gud well';
//        $serv->user_id = 1;
//        $serv->save();
//
//        $serv = new \ELends\Service();
//        $serv->name = 'Marriage Hall Booking';
//        $serv->service_category_id = 1;
//        $serv->image  = 'images/s3.jpg';
//        $serv->detail = 'lorem ipsum gud well';
//        $serv->user_id = 1;
//        $serv->save();
//
//        $serv = new \ELends\Service();
//        $serv->name = 'Transport Service';
//        $serv->service_category_id = 3;
//        $serv->image  = 'images/s4.jpg';
//        $serv->detail = 'lorem ipsum gud well';
//        $serv->user_id = 1;
//        $serv->save();
//
//        $serv = new \ELends\Service();
//        $serv->name = 'Movie Makers Service';
//        $serv->service_category_id = 4;
//        $serv->image  = 'images/s5.jpg';
//        $serv->detail = 'lorem ipsum gud well';
//        $serv->user_id = 1;
//        $serv->save();
//
//        $serv = new \ELends\Service();
//        $serv->name = 'Photography Service';
//        $serv->service_category_id = 5;
//        $serv->image  = 'images/s6.jpg';
//        $serv->detail = 'lorem ipsum gud well';
//        $serv->user_id = 1;
//        $serv->save();
//
//        for($j = 0; $j<6; $j++){
//            $ad = new Ad();
//            $ad->title = 'Here goes the title for Ad ' . ($j + 1);
//            $ad->user_id = 1;
//            $ad->ad_type = "service";
//            $ad->is_featured = 1;
//            $ad->service_id = ($j+1);
//            $ad->rent = '0';
//            $ad->description = 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.';
//            $ad->expiry_date = "2018-10-07 08:19:10";
//            $ad->save();
//        }



        for ($j = 0; $j <= 10; $j++) {
            $ad = new Ad();
            $ad->title = 'Here goes the title for Ad ' . ($j + 1);
            $ad->user_id = 1;
            $ad->ad_type = "item";
            $ad->is_featured = 1;
            $ad->item_id = ($j+1);
            $ad->description = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed dignissim leo, at viverra metus. Vestibulum lacus mi, varius ut vehicula eu, vulputate sit amet arcu. Duis condimentum ultricies congue. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin mattis massa in laoreet maximus. Quisque ante sapien, eleifend quis sagittis quis, porta eu velit. Donec vulputate nisl in ex consequat, quis imperdiet mi euismod. Nam nunc leo, tempor quis erat a, efficitur laoreet elit. Sed efficitur tortor vitae purus rhoncus hendrerit. Cras vel pharetra arcu. Integer cursus felis eget sem ullamcorper, venenatis gravida dui consectetur. Etiam consectetur urna quis eros blandit viverra. Sed pulvinar sem sapien, quis fringilla felis accumsan id. Etiam ut ultricies est.';
            $ad->rent = '500';
            $ad->expiry_date = "2018-10-07 08:19:10";
            $ad->save();
        }




        $booking=new Booking();
        $booking->user_id=4;
        $booking->hall_id=null;
        $booking->service_id=null;
        $booking->item_id=1;
        $booking->ad_id=1;
        $booking->cancelled_by=null;
        $booking->cancel_msg=null;
        $booking->request_status="request-sent";
        $booking->booked_for="2018-10-07 08:19:10";
        $booking->booked_at="2018-10-07 08:19:10";
        $booking->save();

        $booking=new Booking();
        $booking->user_id=4;
        $booking->hall_id=null;
        $booking->service_id=null;
        $booking->item_id=2;
        $booking->ad_id=2;
        $booking->cancelled_by=null;
        $booking->cancel_msg=null;
        $booking->request_status="request-sent";
        $booking->booked_for="2018-10-07 08:19:10";
        $booking->booked_at="2018-10-07 08:19:10";
        $booking->save();

        $booking=new Booking();
        $booking->user_id=4;
        $booking->hall_id=null;
        $booking->service_id=null;
        $booking->item_id=3;
        $booking->ad_id=3;
        $booking->cancelled_by=null;
        $booking->cancel_msg=null;
        $booking->request_status="request-sent";
        $booking->booked_for="2018-10-07 08:19:10";
        $booking->booked_at="2018-10-07 08:19:10";
        $booking->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 2;
        $status->status="REQUEST";
        $status->message="Hi, i'm interested in your product. Please contact me";
        $status->save();


        $booking=new Booking();
        $booking->user_id=4;
        $booking->hall_id=null;
        $booking->service_id=null;
        $booking->item_id=4;
        $booking->ad_id=4;
        $booking->cancelled_by=null;
        $booking->cancel_msg=null;
        $booking->request_status="request-sent";
        $booking->booked_for="2018-10-07 08:19:10";
        $booking->booked_at="2018-10-07 08:19:10";
        $booking->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 4;
        $status->status="REQUEST";
        $status->message="Hi, i'm interested in your product. Please contact me";
        $status->save();

        $booking=new Booking();
        $booking->user_id=4;
        $booking->hall_id=null;
        $booking->service_id=null;
        $booking->item_id=5;
        $booking->ad_id=5;
        $booking->cancelled_by=null;
        $booking->cancel_msg=null;
        $booking->request_status="request-sent";
        $booking->booked_for="2018-10-07 08:19:10";
        $booking->booked_at="2018-10-07 08:19:10";
        $booking->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 5;
        $status->status="REQUEST";
        $status->message="Hi, i'm interested in your product. Please contact me";
        $status->save();


        $status = new \ELends\TimelineStatus();
        $status->booking_id = 2;
        $status->status="REQUEST";
        $status->message="Hi, i'm interested in your product. Please contact me";
        $status->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 3;
        $status->status="REQUEST";
        $status->message="Hi, i'm interested in your product. Please contact me";
        $status->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 1;
        $status->status="REQUEST";
        $status->message="Hi, i'm interested in your product. Please contact me";
        $status->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 1;
        $status->status="ACCEPTED";
        $status->message="Hi, I am accepting your booking request. Please take care of the product";
        $status->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 1;
        $status->status="DELIVERED";
        $status->message="Hi, i have delivered the product";
        $status->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 1;
        $status->status="RECEIVED";
        $status->message="Hi, I received the product. Thanks";
        $status->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 1;
        $status->status="RETURNED";
        $status->message="Hi, your product was great. Thanks a lot";
        $status->save();

        $status = new \ELends\TimelineStatus();
        $status->booking_id = 1;
        $status->status="COMPLETED";
        $status->message="Hi, i received back the product and marking the order as complete";
        $status->save();



//
//
//        $booking=new Booking();
//        $booking->user_id=3;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=2;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="";
//        $booking->request_status="request-accepted";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//        $booking=new Booking();
//
//        $booking->user_id=1;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=3;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//        $booking=new Booking();
//
//        $booking->user_id=1;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=4;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="Item is not aavailable";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=2;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=5;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="Item Not Available";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=2;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=6;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=2;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=7;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=2;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=8;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="Item is not aavailable";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=3;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=9;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="Item Not Available";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=3;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=10;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=3;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=11;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();
//
//
//
//        $booking=new Booking();
//
//        $booking->user_id=3;
//        $booking->hall_id=null;
//        $booking->service_id=null;
//        $booking->item_id=12;
//        $booking->cancelled_by=1;
//        $booking->cancel_msg="Item is not aavailable";
//        $booking->request_status="request-sent";
//        $booking->booked_for="2018-10-07 08:19:10";
//        $booking->booked_at="2018-10-07 08:19:10";
//        $booking->save();





//        for($i=0;$i<5; $i++){
//            $favt_ad=new FavoriteAd();
//
//             $favt_ad->user_id=1;
//            $favt_ad->ad_id=($i+1);
//            $favt_ad->save();
//
//        }
//
//
//        for($i=0;$i<5; $i++){
//            $favt_ad=new FavoriteAd();
//
//             $favt_ad->user_id=2;
//            $favt_ad->ad_id=($i+1);
//            $favt_ad->save();
//
//
//        }
//
//
//        for($i=0;$i<5; $i++){
//            $favt_ad=new FavoriteAd();
//
//             $favt_ad->user_id=3;
//            $favt_ad->ad_id=($i+1);
//            $favt_ad->save();
//
//
//        }


        for($i=0;$i<2;$i++){
            $interest=new Interest();
             
            $interest->user_id=1;
            $interest->category_id=($i+1);
            $interest->save();


        }

        for($i=2;$i<4;$i++){
            $interest=new Interest();
             
            $interest->user_id=2;
            $interest->category_id=($i+1);
            $interest->save();


        }

        for($i=0;$i<2;$i++){
            $interest=new Interest();
             
            $interest->user_id=3;
            $interest->category_id=($i+1);
            $interest->save();


        }

        $chat = new \ELends\Chat();
        $chat->booking_id = 1;
        $chat->user_id = 4;
        $chat->save();
        for ($j=1;$j<5;$j++)
        {
            $message = new \ELends\Message();
            $message->chat_id = $chat->id;
            $message->user_id = 4;
            $message->message = 'message 1'." ".$j;
            $message->save();
        }

        $chat = new \ELends\Chat();
        $chat->booking_id = 1;
        $chat->user_id = 3;
        $chat->save();

        for ($j=1;$j<5;$j++)
        {
            $message = new \ELends\Message();
            $message->chat_id = $chat->id;
            $message->user_id = 4;
            $message->message = 'message 2'." ".$j;
            $message->save();
        }



        $event = new \ELends\Event();
        $event->name = "Mehandi";
        $event->save();

        $event = new \ELends\Event();
        $event->name = "Barat";
        $event->save();

        $event = new \ELends\Event();
        $event->name = "Nikah";
        $event->save();

        $event = new \ELends\Event();
        $event->name = "Valima";
        $event->save();

        $event = new \ELends\Event();
        $event->name = "Party";
        $event->save();

        $event = new \ELends\Event();
        $event->name = "Birthday";
        $event->save();


    }

}
