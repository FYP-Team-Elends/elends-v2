<?php

use Illuminate\Database\Seeder;
use ELends\Role;
use ELends\Permission;
use ELends\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user= new User();
        $user->first_name  = "Shahzaib";
        $user->last_name  = "Khan";
        $user->email = "shahzaib@gmail.com";
        $user->city = "Lahore, Pakistan";
        $user->lat = 31.52036960000001;
        $user->lng = 74.3587473;
        $user->password = bcrypt('Secret');
        $user->email_verified_at = Carbon\Carbon::now();;
        $user->phone = "03123435345";
        $user->picture = "/storage/Profile_images/shahzaib@gmail.com-Profile_Picture.png";
        $user->save();

        $normal = new Role();
        $normal->name         = 'ServiceUser';
        $normal->display_name = 'Service User'; // optional
        $normal->description  = 'Get Services'; // optional
        $normal->save();

        $user->attachRole($normal);


        $admin= new User();
        $admin->first_name  = "Test";
        $admin->last_name  = "Admin";
        $admin->email = "admin@gmail.com";
        $admin->city = "Lahore";
        $admin->password = bcrypt('Secret');
        $admin->email_verified_at = Carbon\Carbon::now();;
        $user->phone = "03123435345";
        $user->picture = "/storage/Profile_images/shahzaib@gmail.com-Profile_Picture.png";
        $admin->save();


        $owner = new Role();
        $owner->name         = 'Admin';
        $owner->display_name = 'Admin User'; // optional
        $owner->description  = 'Manage Services'; // optional
        $owner->save();

        $admin->attachRole($owner);

        $user1= new User();
        $user1->first_name  = "Maham";
        $user1->last_name  = "Developer";
        $user1->email = "maham@gmail.com";
        $user1->city = "Lahore, Pakistan";
        $user1->lat = 31.52036960000001;
        $user1->lng = 74.3587473;
        $user1->password = bcrypt('Secret');
        $user->phone = "03123435345";
        $user->picture = "/storage/Profile_images/shahzaib@gmail.com-Profile_Picture.png";
        $user1->email_verified_at = Carbon\Carbon::now();;
        $user1->save();

        $user= new User();
        $user->first_name  = "Faraz";
        $user->last_name  = "Developer";
        $user->email = "faraz@gmail.com";
        $user->city = "Lahore, Pakistan";
        $user->password = bcrypt('Secret');
        $user->phone = "03123435345";
        $user->lat = 31.52036960000001;
        $user->lng = 74.3587473;
        $user->picture = "/storage/Profile_images/shahzaib@gmail.com-Profile_Picture.png";
        $user->email_verified_at = Carbon\Carbon::now();;
        $user->save();
        $user->attachRole($normal);

        $serviceProvider = new Role();
        $serviceProvider->name         = 'ServiceProvider';
        $serviceProvider->display_name = 'Service Provider'; // optional
        $serviceProvider->description  = 'Service Provider can post services'; // optional
        $serviceProvider->save();
        $user1->attachRole($serviceProvider);
    }
}
