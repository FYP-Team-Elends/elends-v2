<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimelineStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeline_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id')->unsigned();
            $table->enum('status', ['REQUEST', 'REQUEST_CANCELLED', 'ACCEPTED', 'REJECTED', 'DELIVERED', 'RECEIVED', 'RETURNED', 'COMPLETED']);
            $table->string('message');
            $table->foreign('booking_id')->references('id')
                ->on('bookings')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeline_statuses');
    }
}
