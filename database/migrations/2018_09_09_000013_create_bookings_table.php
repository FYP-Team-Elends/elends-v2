<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('hall_id')->nullable();
            $table->unsignedInteger('ad_id')->nullable();
            $table->unsignedInteger('service_id')->nullable();
            $table->unsignedInteger('item_id')->nullable();
            $table->unsignedInteger('cancelled_by')->nullable();
            $table->String('cancel_msg')->nullable();
            $table->enum('request_status',['request-sent','request-accepted','request-rejected','booking-cancelled']);
            $table->dateTime('booked_for');
            $table->integer('timeline_status')->default(0);
            $table->dateTime('booked_at');
            $table->foreign('ad_id')->references('id')->on('ads')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('cancelled_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('hall_id')->references('id')->on('marrige_halls')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
