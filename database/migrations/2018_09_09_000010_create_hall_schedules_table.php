<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHallSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hall_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('week_day');
            $table->time('time_to');
            $table->time('time_from');
            $table->timestamp('updated_on');
            $table->boolean('availability');
            $table->unsignedInteger('hall_id');
            $table->foreign('hall_id')->references('id')->on('marrige_halls')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hall_schedules');
    }
}
