<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarrigeHallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marrige_halls', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->string('latlng');
            $table->unsignedInteger('user_id');
            $table->string('cover');
            $table->string('picture');
            $table->string('contact_number');
            $table->string('email');
            $table->integer('hall_capacity');
            $table->string('hall_type');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marrige_halls');
    }
}
