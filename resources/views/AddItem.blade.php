
@extends('layouts.master')

@section('content')

    <!-- NAVIGATION -->
    <div id="navigation">
        <!-- container -->
        <div class="container">


        </div>
        <!-- /container -->
    </div>
    <!-- /NAVIGATION -->

    <!-- HOME -->
    <div style="padding: 0px">
        <!-- container -->
        <div class="main-container">
            <!-- home wrap -->
            <div class="wide-intro">
                <!-- home slick -->
                <div class="dtable hw100">

                    <!-- banner -->
                    <div class="dtable hw100" style="background-image: url('./img/banner03.jpg');">

                        <div class="container text-center" style="margin-top: 160px">
                            <h1 class="intro-title animated fadeInDown">E-Lends<br><span class="white-color font-weak">Lend or Post anything you want</span></h1>
                            <a href="{{url('AddItem')}}"class="primary-btn">POST AD</a>
                        </div>
                    </div>

                    <!-- /banner -->
                </div>
                <!-- /home slick -->
            </div>
            <!-- /home wrap -->
        </div>
        <!-- /container -->
    </div>
    <!-- /HOME -->

    <div>
        <div class="container">
            <!-- slider -->
        <!-- <div class="agile-trend-ads">
            <h2>Featured Ads</h2>
            <ul id="flexiselDemo3">
                <li>
                    @foreach($ads as $ad)
            <div class="col-md-3 biseller-column">
                <a href="{{url('viewproduct/'.$ad->id)}}">
                            <img src="{{$ad->item->path}}" alt="" />
                            <span class="price">{{$ad->item->day_rent}} PKR / Day</span>
                        </a>
                        <div class="w3-ad-info">
                            <h5>{{$ad->title}}</h5>
                            <span>{{$ad->created_at}}</span>
                        </div>
                    </div>
                    @endforeach

                </li>
            </ul>
        </div> -->


            <div class="col-lg-12 content-box layout-section">
                <div class="row row-featured row-featured-category">
                    <div class="col-lg-12 box-title no-border">
                        <div class="inner">
                            <h2>
                                <span class="title-3">Browse by <span style="font-weight: bold;" class="">Category</span></span>
                            </h2>
                        </div>
                    </div>


                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/multimedia">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-laptop.png?v=1" class="img-responsive" alt="img">
                            <h6> Multimedia </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/automobiles">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-car.png?v=1" class="img-responsive" alt="img">
                            <h6> Automobiles </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/real-estate">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-home.png?v=1" class="img-responsive" alt="img">
                            <h6> Real estate </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/services">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/ion-clipboard.png?v=1" class="img-responsive" alt="img">
                            <h6> Services </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/job-search">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-search.png?v=1" class="img-responsive" alt="img">
                            <h6> Job Search </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/jobs-offers">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/mfglabs-users.png?v=1" class="img-responsive" alt="img">
                            <h6> Jobs Offers </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/local-events">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-calendar.png?v=1" class="img-responsive" alt="img">
                            <h6> Local Events </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/learning">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-graduation-cap.png?v=1" class="img-responsive" alt="img">
                            <h6> Learning </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/community">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-users.png?v=1" class="img-responsive" alt="img">
                            <h6> Community </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/fashion-home-garden">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/fa-home.png?v=1" class="img-responsive" alt="img">
                            <h6> Fashion, Home &amp; Garden </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/pets-mascots">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/map-icon-pet-store.png?v=1" class="img-responsive" alt="img">
                            <h6> Pets &amp; Mascots </h6>
                        </a>
                    </div>
                    <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                        <a href="http://laraclassified.bedigit.com/category/free-dating">
                            <img src="http://laraclassified.bedigit.com/storage/app/categories/skin-blue/ion-ios-heart.png?v=1" class="img-responsive" alt="img">
                            <h6> Free Dating </h6>
                        </a>
                    </div>


                </div>
            </div>
        </div>
        <div class="h-spacer"></div>

        <div class="container" style="margin-bottom: 100px">
            <div class="col-lg-12 content-box layout-section">
                <div class="col-lg-12 box-title no-border">
                    <div class="inner">
                        <h2>
                            <span class="title-3">Latest <span style="font-weight: bold;">Ads</span></span>
                        </h2>
                    </div>
                </div>

                <div class="adds-wrapper noSideBar">
                    <div class="item-list make-grid">

                        <div class="col-sm-2 no-padding photobox">
                            <div class="add-image">
                                <a href="http://laraclassified.bedigit.com/nvmhgj/5909">
                                    <img class="thumbnail no-margin" src="http://laraclassified.bedigit.com/storage/files/pk/5909/thumb-320x240-d7b6398ecd648bd232d4c723fe216f14.png?v=1" alt="img">
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-7 add-desc-box">
                            <div class="add-details">
                                <h5 class="add-title">
                                    <a href="http://laraclassified.bedigit.com/nvmhgj/5909">Nvmhgj </a>
                                </h5>

                                <span class="info-row">
                                <span class="date">2 days ago </span><br>

                                <span class="item-location"><i class="fa fa-map-marker"></i>&nbsp;
                                <a href="http://laraclassified.bedigit.com/search?l=1174872" class="info-link">Karachi</a>
                                </span>
                            </span>
                            </div>

                        </div>

                        <div class="col-sm-3 text-right price-box">
                            <h4 class="">45,545,454/Day</h4>
                            <a class="btn btn-default btn-sm make-favorite" id="5909"><i class="fa fa-heart"></i><span> Add to Favorites </span></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- //slider -->
    </div>

    <!-- FOOTER -->
    <!--footer section start-->
@stop