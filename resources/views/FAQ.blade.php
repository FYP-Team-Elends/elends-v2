<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FAQ</title>
    <!--  FAQ - START -->
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css" />

    <script type="text/javascript" src="jsf/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
    <!-- End-->
    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/bootstrap-select.css"><!-- bootstrap-select-CSS -->
    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/flexslider.css" type="text/css" media="screen" /><!-- flexslider-CSS -->
    <link rel="stylesheet" href="csss/font-awesome.min.css" /><!-- fontawesome-CSS -->
    <link rel="stylesheet" href="csss/menu_sideslide.css" type="text/css" media="all"><!-- Navigation-CSS -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->

    <!--fonts-->
    <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--//fonts-->

    <!-- js -->
    <script type="text/javascript" src="jss/jquery.min.js"></script>
    <!-- js -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="jss/bootstrap.js"></script>
    <script src="jss/bootstrap-select.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="css/slick.css" />
    <link type="text/css" rel="stylesheet" href="css/slick-theme.css" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />

</head>
<body>
<!-- HEADER -->
<div id="header">
    <!-- top Header -->
    <div id="top-header">
        <div class="container">
            <div class="pull-left">
                <span>Welcome to E-Lends!</span>
            </div>
            <div class="pull-right">
                <ul class="header-top-links">
                    <li><a href="#">Store</a></li>
                    <li><a href="#">Newsletter</a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /top Header -->
    <div class="container">
        <div class="pull-left">
            <!-- Logo -->
            <div class="header-logo" style="float: left;">
                <a class="logo" href="#">
                    <img src="./img/logo.png" alt="">
                </a>
            </div>
            <!-- /Logo -->

            <!-- Search -->
            <div class="header-search">
                <form>
                    <input class="input search-input" type="text" placeholder="Enter your keyword">
                    <select class="input search-categories">
                        <option value="0">Select Event</option>
                        <option value="1">Mehandi</option>
                        <option value="1">Barat</option>
                        <option value="1">Nikha</option>
                        <option value="1">Valima</option>
                        <option value="1">Party</option>
                        <option value="1">Birthday</option>
                        <option value="1">Bridal Shower</option>
                    </select>
                    <button class="search-btn"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- /Search -->
        </div>
        <div class="pull-right">
            <ul class="header-btns">
                <!-- Account -->
                <li class="header-account dropdown default-dropdown">
                    <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">
                            <i class="fa fa-user-o"></i>
                        </div>
                        <strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
                    </div>
                    <a href="#" class="text-uppercase">Login</a> / <a href="#" class="text-uppercase">Join</a>
                    <ul class="custom-menu">
                        <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
                        <li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li>
                        <li><a href="#"><i class="fa fa-exchange"></i> My Ads</a></li>
                        <li><a href="#"><i class="fa fa-check"></i> Messages</a></li>
                        <li><a href="#"><i class="fa fa-unlock-alt"></i> Settings</a></li>
                        <li><a href="#"><i class="icon mini abs power"></i> Log out</a></li>
                    </ul>
                </li>
                <!-- /Account -->

                <!-- Cart -->
                <li class="header-cart dropdown default-dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">
                            <i class="fa fa-heart-o"></i>
                        </div>
                        <strong class="text-uppercase">My Favourites:</strong>
                        <br>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- NAVIGATION -->
<div id="navigation">
    <!-- container -->
    <div class="container">
        <div id="responsive-nav">
            <!-- category nav -->
            <div class="category-nav show-on-click">
                <span class="category-header">Categories <i class="fa fa-list"></i></span>
                <ul class="category-list">
                    <li class="dropdown side-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Women’s Clothing <i class="fa fa-angle-right"></i></a>
                        <div class="custom-menu">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr class="hidden-md hidden-lg">
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr class="hidden-md hidden-lg">
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="row hidden-sm hidden-xs">
                                <div class="col-md-12">
                                    <hr>
                                    <a class="banner banner-1" href="#">
                                        <img src="./img/banner05.jpg" alt="">
                                        <div class="banner-caption text-center">
                                            <h2 class="white-color">NEW COLLECTION</h2>
                                            <h3 class="white-color font-weak">HOT DEAL</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="#">Men’s Clothing</a></li>
                    <li class="dropdown side-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Phones & Accessories <i class="fa fa-angle-right"></i></a>
                        <div class="custom-menu">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr>
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr class="hidden-md hidden-lg">
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr>
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                </div>
                                <div class="col-md-4 hidden-sm hidden-xs">
                                    <a class="banner banner-2" href="#">
                                        <img src="./img/banner04.jpg" alt="">
                                        <div class="banner-caption">
                                            <h3 class="white-color">NEW<br>COLLECTION</h3>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="#">Computer & Office</a></li>
                    <li><a href="#">Consumer Electronics</a></li>
                    <li class="dropdown side-dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Jewelry & Watches <i class="fa fa-angle-right"></i></a>
                        <div class="custom-menu">
                            <div class="row">
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr>
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr class="hidden-md hidden-lg">
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr>
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr class="hidden-md hidden-lg">
                                </div>
                                <div class="col-md-4">
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                    <hr>
                                    <ul class="list-links">
                                        <li>
                                            <h3 class="list-links-title">Categories</h3></li>
                                        <li><a href="#">Women’s Clothing</a></li>
                                        <li><a href="#">Men’s Clothing</a></li>
                                        <li><a href="#">Phones & Accessories</a></li>
                                        <li><a href="#">Jewelry & Watches</a></li>
                                        <li><a href="#">Bags & Shoes</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li><a href="#">Bags & Shoes</a></li>
                    <li><a href="#">View All</a></li>
                </ul>
            </div>
            <!-- /category nav -->

            <!-- menu nav -->
            <div class="menu-nav">
                <span class="menu-header">Menu <i class="fa fa-bars"></i></span>
            </div>
            <!-- menu nav -->
        </div>
    </div>
    <!-- /container -->
</div>
<!-- /NAVIGATION -->
<div class="row"></div>
<div class="container" style=" padding-bottom: 20px;">

    <div class="page-header">
        <h2 class="head">All you need to know</h2>
    </div>

    <!-- Bootstrap FAQ - START -->
    <div class="container" style="padding-top: 0px; margin-top: 0px">
        <br />
        <br />
        <br />
        <br />

        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><i class="fa fa-question-circle"></i>Is account registration required</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse in">
                    <div class="panel-body">
                        Account registration at <strong>E-Lends</strong> is only required if you will be posting the ads or booking any service.
                        This ensures a valid communication channel for all parties involved in any transactions.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen"><i class="fa fa-question-circle"></i>How to edit my Ad</a>
                    </h4>
                </div>
                <div id="collapseTen" class="panel-collapse collapse">
                    <div class="panel-body">
                        <b>'My Account' option:- (only for Registered Users)</b>
                          <ul>
                              <li>• Login to your account</li>
                              <li>• Go to the "Active ads" under the Ads tab</li>
                              <li>• Select the ad you want to edit and click the button "Edit"</li>
                              <li>• You can change the title, description, rent and contact details as per your wish.</li>
                              <li>• Click on 'Update Ad'. Done!</li>
                          </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven"><i class="fa fa-question-circle"></i>How to delete my Ad</a>
                    </h4>
                </div>
                <div id="collapseEleven" class="panel-collapse collapse">
                    <div class="panel-body">
                        <b>My Account option (Only Registered users):-</b>
                        <ul>
                          <li>• Log in to "My Account".</li>
                            <li>• Go to the "Active ads" under the Ads tab.</li>
                            <li> • Select the ad, you want to delete and click "Deactivate Ad" button.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><i class="fa fa-question-circle"></i>How to edit my contact information</a>
                    </h4>
                </div>
                <div id="collapseTwo" class="panel-collapse collapse">
                    <div class="panel-body">
                        <b>Follow the steps given below:</b>
                        <ul>
                           <li> •Login to My Account</li>
                            <li>•Click the Settings tab</li>
                            <li> •Click 'Edit contact details' button to change city, name or phone number.</li>
                            <li><strong class="s">Please note:</strong>You will receive an OTP code to change phone number.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><i class="fa fa-question-circle"></i>How to create a good Ad.</a>
                    </h4>
                </div>
                <div id="collapseThree" class="panel-collapse collapse">
                    <div class="panel-body">
                        Describe the product/item you want to sell in more detail. Incomplete description receives phone calls and emails asking for more details.<br>
                        Check the spelling and do not write with CAPSLOCK.
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive"><i class="fa fa-question-circle"></i>Where can I find my messages</a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse">
                    <div class="panel-body">
                        <b>Follow the steps given below:</b>
                        <ul>
                            <li> •Go to My Account</li>
                            <li>•Click the message tab</li>
                        </ul>
                        <br />
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix"><i class="fa fa-question-circle"></i>How to create an account in E-Lends</a>
                    </h4>
                </div>
                <div id="collapseSix" class="panel-collapse collapse">
                    <div class="panel-body">
                        To register with E-Lends, just follow these step:
                        <ul>
                            <li>• Go to E-Lends</li>
                            <li>• Click "join" button on the top right corner of page</li>
                            <li>• Enter the requested information</li>
                            <li>• click "signup"</li>
                            <li>Make sure that you enter a valid <strong>email id</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>

    </div>
    <div class="one-half">
        <div class="w-iconbox iconpos_left size_medium style_default color_primary">
            <div class="w-iconbox-icon" >
                <i class="fa fa-comments-o" style="color:#1b98e0;"></i>
            </div>
            <h4 class="w-iconbox-title">I looked through the FAQ and didn't find what i was looking for!</h4>
            <div class="w-iconbox-text"><span>No problem you can contact our support department by sending email at e-lends@gmail.com</span><br>
            </div>
        </div>

    </div>
</div>

            <!-- FOOTER -->
            <footer id="footer" class="section section-grey">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- footer widget -->
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="app">
                                <h3>DOWNLOAD APP</h3>
                                <img src="./img/app.jpg" style="width: 180px">
                            </div>
                        </div>
                        <!-- /footer widget -->

                        <!-- footer widget -->
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="footer">
                                <h3 class="footer-header">Information</h3>
                                <ul class="list-links">
                                    <li><a href="#">Location Map</a></li>
                                    <li><a href="#">Popular Seraches</a></li>
                                    <li><a href="#">term of use</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /footer widget -->

                        <div class="clearfix visible-sm visible-xs"></div>

                        <!-- footer widget -->
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="footer">
                                <h3 class="footer-header">HELP</h3>
                                <ul class="list-links">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">FAQ</a></li>
                                    <li><a href="#">Sitemap</a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- /footer widget -->

                        <!-- footer subscribe -->
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <div class="footer">
                                <h3 class="footer-header">Contact Us</h3>
                                <div class="clearfix"> </div>
                                <ul class="location">
                                    <li><span class="glyphicon glyphicon-earphone"></span></li>
                                    <li>+0 561 111 235</li>
                                </ul>
                                <div class="clearfix"> </div>
                                <ul class="location">
                                    <li><span class="glyphicon glyphicon-envelope"></span></li>
                                    <li><a href="mailto:info@example.com">mail@example.com</a></li>
                                </ul>
                                <div class="clearfix"> </div>
                                <ul class="location">
                                    <li><span class="glyphicon glyphicon-briefcase"></span></li>
                                    <li><strong>Business Packages</strong></li>
                                    <li><small>(featured ads, advertising)</small></li><br>
                                    <li><a href="#">click here</a></li>
                                </ul>
                                <div class="clearfix"> </div>
                            </div>
                        </div>
                        <!-- /footer subscribe -->
                    </div>
                    <!-- /row -->
                    <!-- /row -->
                </div>
                <!-- /container -->
            </footer>
            <!--footer section start-->
            <footer>
                <div class="agileits-footer-bottom text-center">
                    <div class="container">
                        <div class="w3-footer-logo">
                            <h1><a href="index.blade.php"><span>E-</span>Lend</a></h1>
                        </div>
                        <div class="w3-footer-social-icons">
                            <ul>
                                <li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a></li>
                                <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a></li>
                                <li><a class="flickr" href="#"><i class="fa fa-flickr" aria-hidden="true"></i><span>Flickr</span></a></li>
                                <li><a class="googleplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i><span>Google+</span></a></li>
                                <li><a class="dribbble" href="#"><i class="fa fa-dribbble" aria-hidden="true"></i><span>Dribbble</span></a></li>
                            </ul>
                        </div>
                        <div class="col-md-8 col-md-offset-2 text-center">
                            <!-- footer copyright -->
                            <div class="footer-copyright">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | We <i class="fa fa-heart-o" style="color: red" aria-hidden="true"></i> to make you happy</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </div>
                            <!-- /footer copyright -->
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </footer>
            <!--footer section end-->

            <!-- jQuery Plugins -->
            <script src="js/jquery.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/slick.min.js"></script>
            <script src="js/nouislider.min.js"></script>
            <script src="js/jquery.zoom.min.js"></script>
            <script src="js/main.js"></script>

            <!-- Navigation-Js-->
            <script type="text/javascript" src="jss/main.js"></script>
            <script type="text/javascript" src="jss/classie.js"></script>
            <!-- //Navigation-Js-->
            <!-- js -->
            <script type="text/javascript" src="jss/jquery.min.js"></script>
            <!-- js -->
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="jss/bootstrap.js"></script>
            <script src="jss/bootstrap-select.js"></script>
            <script>
                $(document).ready(function () {
                    var mySelect = $('#first-disabled2');

                    $('#special').on('click', function () {
                        mySelect.find('option:selected').prop('disabled', true);
                        mySelect.selectpicker('refresh');
                    });

                    $('#special2').on('click', function () {
                        mySelect.find('option:disabled').prop('disabled', false);
                        mySelect.selectpicker('refresh');
                    });

                    $('#basic2').selectpicker({
                        liveSearch: true,
                        maxOptions: 1
                    });
                });
            </script>
            <style>
                .s{
                    color: red;
                }

                .w-iconbox-text{
                    font-family: "Open Sans", sans-serif;
                    font-size: 14px;
                    line-height: 24px;
                    font-weight: 400;
                }

                .w-iconbox.size_medium .w-iconbox-icon {
                    font-size: 48px;
                }

                .w-iconbox.style_default .w-iconbox-icon {
                    background-color: transparent !important;
                    box-shadow: none !important;
                    color: inherit;
                }
                .w-iconbox.iconpos_left .w-iconbox-icon {
                    float: left;
                }
                .w-iconbox-icon {
                    line-height: 1;
                    position: relative;
                    transition: background-color 0.3s, color 0.3s, box-shadow 0.1s;
                }
                .w-iconbox-icon {
                    text-align: center;
                    margin: 0 auto;
                }
                .g-cols.offset_medium > .one-half {
                    width: 47%;
                }

                .w-iconbox .w-iconbox-title .h4{
                    margin-bottom: 0;
                    font-family: "Open Sans", sans-serif;
                    font-weight: 400;
                    font-size: 22px;
                    letter-spacing: 0px;
                    line-height: 1.4;
                    margin: 0 0 1.5rem;
                }
                .w-iconbox-title {
                    transition: color 0.3s;
                }

                h2.head {
                    margin: 35px 0;
                    font-size: 30px;
                    color: #000;
                    margin-bottom: 20px;
                    text-align: center;
                }

                .container {
                    padding-right: 15px;
                    padding-left: 15px;
                    margin-right: auto;
                    margin-left: auto;
                }
                .page-header {
                    padding-bottom: 0px;
                    text-align: center;
                    margin: 40px 0 20px;
                    border-bottom: 1px solid #eee;
                    margin-bottom: 0px;
                }
                .w-iconbox.iconpos_left {
                    text-align: left;
                }
                .w-iconbox {
                    position: relative;
                }
                * {
                    box-sizing: border-box;
                }
                .g-cols.offset_medium > div:first-child, .g-cols.offset_medium > div.full-width {
                    margin-left: 0;
                }
                .g-cols.offset_medium > .one-half {
                    width: 47%;
                }
                .fa-comments-o:before {
                    content: "\f0e6";
                }
                .w-iconbox.color_primary .w-iconbox-icon{
                    color: #1b98e0;
                }


                .w-iconbox.style_default .w-iconbox-icon {
                    background-color: transparent !important;
                    box-shadow: none !important;
                    color: inherit;
                }

                .w-iconbox.iconpos_left .w-iconbox-icon {
                    float: left;
                }
                .w-iconbox-icon {
                    line-height: 1;
                    position: relative;
                    transition: background-color 0.3s, color 0.3s, box-shadow 0.1s;
                }
                .w-iconbox-icon {
                    text-align: center;
                    margin: 0 auto;
                }
                .w-iconbox.iconpos_left:after {
                    display: table;
                    content: '';
                    clear: both;
                }
                .w-iconbox-icon {
                    color: #1b98e0;
                }

                .fa {
                    display: inline-block;
                    font: normal normal normal 14px/1 FontAwesome;
                    font-size: inherit;
                    text-rendering: auto;
                    margin-right: 10px;
                    -webkit-font-smoothing: antialiased;
                }
                .fa-question-circle:before {
                    content: "\f059";
                }
                .faqHeader {
                    font-size: 27px;
                    margin: 20px;
                }

                .panel-heading [data-toggle="collapse"]:after {
                    font-family: 'Glyphicons Halflings';
                    content: "\e072"; /* "play" icon */
                    float: right;
                    color: #F58723;
                    font-size: 18px;
                    line-height: 22px;
                    /* rotate "play" icon from > (right arrow) to down arrow */
                    -webkit-transform: rotate(-90deg);
                    -moz-transform: rotate(-90deg);
                    -ms-transform: rotate(-90deg);
                    -o-transform: rotate(-90deg);
                    transform: rotate(-90deg);
                }

                .panel-heading [data-toggle="collapse"].collapsed:after {
                    /* rotate "play" icon from > (right arrow) to ^ (up arrow) */
                    -webkit-transform: rotate(90deg);
                    -moz-transform: rotate(90deg);
                    -ms-transform: rotate(90deg);
                    -o-transform: rotate(90deg);
                    transform: rotate(90deg);
                    color: #454444;
                }
            </style>

            <!-- Bootstrap FAQ - END -->




</body>
<!-- Navigation-JavaScript -->
<script src="jss/classie.js"></script>
<script src="jss/main.js"></script>
<!-- //Navigation-JavaScript -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="jss/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->

</html>