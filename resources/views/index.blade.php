
@extends('layouts.master')

@section('content')

<!-- NAVIGATION -->
<div id="navigation">
    <!-- container -->
    <div class="container">
        
        
    </div>
    <!-- /container -->
</div>
<!-- /NAVIGATION -->

<!-- HOME -->
<div style="padding: 0px">
    <!-- container -->
    <div class="main-container">
        <!-- home wrap -->
        <div class="wide-intro">
            <!-- home slick -->
            <div class="dtable hw100">

                <!-- banner -->
                <div class="dtable hw100" style="background-image: url('./img/banner03.jpg');">
                    
                    <div class="container text-center" style="margin-top: 160px">
                        <h1 class="intro-title animated fadeInDown">E-Lends<br><span class="white-color font-weak">Lend or Post anything you want</span></h1>
                        <a href="{{url('AddItem')}}"class="primary-btn">POST AD</a>
                    </div>
                </div>

                <!-- /banner -->
            </div>
            <!-- /home slick -->
        </div>
        <!-- /home wrap -->
    </div>
    <!-- /container -->
</div>
<!-- /HOME -->

<div>
    <div class="container">


    
        <div class="col-lg-12 content-box layout-section">
            <div class="row row-featured row-featured-category">
                <div class="col-lg-12 box-title no-border">
                    <div class="inner">
                        <h2>
                            <span class="title-3">Browse by <span style="font-weight: bold;" class="">Category</span></span>
                        </h2>
                    </div>
                </div>
                
                                    {{--@foreach($catagories as $catagory)
                                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 f-category" style="height: 152px;">
                                             <a href="{{'#catagory/'.$catagory->id.'/all'}}">
                                                    <img src="{{$catagory->image}}" class="img-responsive" alt="img">
                                                    <h6> {{$catagory->category}} </h6>
                                             </a>
                                        </div>

                                    @endforeach--}}
            </div>
        </div>
    </div>
    <div class="h-spacer"></div>
   {{--  @if(Auth::check())
        <div class="container" style="margin-bottom: 20px">
            <div class="col-lg-12 content-box layout-section">
                <div class="col-lg-12 box-title no-border">
                        <div class="inner">
                            <h2>
                                <span class="title-3">Recent Viewed <span style="font-weight: bold;">Ads</span></span>
                            </h2>
                        </div>
                    </div>

                       <div class="adds-wrapper noSideBar">
                           @foreach($recentads as $recentad)
                               <div class="item-list make-grid">

                                   <div class="col-sm-2 no-padding photobox">
                                       <div class="add-image">
                                           <a href="{{url('viewproduct/'.$ad->id)}}">
                                               <img class="thumbnail no-margin" src="http://localhost:8000/images/1.jpg" alt="img">
                                           </a>
                                       </div>
                                   </div>

                                   <div class="col-sm-7 add-desc-box">
                                       <div class="add-details">
                                           <h5 class="add-title">
                                               {{$recentad->title}}
                                           </h5>

                                           <span class="info-row">
                                --}}{{--<span class="date"> {{$ad->ad->created_at}} </span><br>--}}{{--

                                <span class="item-location"><i class="fa fa-map-marker"></i>&nbsp;
                                <a href="#" class="info-link">{{'Karachi'/*.$ad->item->user->location*/}}</a>
                                </span>
                            </span>
                                       </div>

                                   </div>

                                   <div class="col-sm-3 text-right price-box">
                                       --}}{{--<h4 class=""><b>{{$ad->ad-rent}}PKR </b>/Day</h4>--}}{{--
                                       <a href="#" class="btn btn-default btn-sm make-favorite" id="5909"><i class="fa fa-heart"></i><span> Add to Favorites </span></a>
                                   </div>

                               </div>
                           @endforeach
                        </div>

            </div>
        </div>

        --}}{{--<div class="container" style="margin-bottom: 100px">--}}{{--
            --}}{{--<div class="col-lg-12 content-box layout-section">--}}{{--
                --}}{{--<div class="col-lg-12 box-title no-border">--}}{{--
                    --}}{{--<div class="inner">--}}{{--
                        --}}{{--<h2>--}}{{--
                            --}}{{--<span class="title-3">Latest <span style="font-weight: bold;">Ads</span> Picks For You</span>--}}{{--
                        --}}{{--</h2>--}}{{--
                    --}}{{--</div>--}}{{--
                --}}{{--</div>--}}{{--

                --}}{{--<div class="adds-wrapper noSideBar">--}}{{--
                    --}}{{--@foreach($ads as $ad)--}}{{--
                        --}}{{--<div class="item-list make-grid">--}}{{--

                            --}}{{--<div class="col-sm-2 no-padding photobox">--}}{{--
                                --}}{{--<div class="add-image">--}}{{--
                                    --}}{{--<a href="{{'#ad/'.$ad->id.'show'}}">--}}{{--
                                        --}}{{--<img class="thumbnail no-margin" src="{{$ad->item->path}}" alt="img">--}}{{--
                                    --}}{{--</a>--}}{{--
                                --}}{{--</div>--}}{{--
                            --}}{{--</div>--}}{{--

                            --}}{{--<div class="col-sm-7 add-desc-box">--}}{{--
                                --}}{{--<div class="add-details">--}}{{--
                                    --}}{{--<h5 class="add-title">--}}{{--
                                        --}}{{--<a href="#">{{$ad->title}} </a>--}}{{--
                                    --}}{{--</h5>--}}{{--

                                    --}}{{--<span class="info-row">--}}{{--
                                --}}{{--<span class="date"> {{$ad->created_at}} </span><br>--}}{{--

                                --}}{{--<span class="item-location"><i class="fa fa-map-marker"></i>&nbsp;--}}{{--
                                --}}{{--<a href="#" class="info-link">{{'Karachi'/*.$ad->item->user->location*/}}</a>--}}{{--
                                --}}{{--</span>--}}{{--
                            --}}{{--</span>--}}{{--
                                --}}{{--</div>--}}{{--

                            --}}{{--</div>--}}{{--

                            --}}{{--<div class="col-sm-3 text-right price-box">--}}{{--
                                --}}{{--<h4 class=""><b>--}}{{----}}{{--{{$ad->rent}}--}}{{----}}{{-- Pkr </b>Per\Day</h4>--}}{{--
                                --}}{{--<a href="#" class="btn btn-default btn-sm make-favorite" id="5909"><i class="fa fa-heart"></i><span> Add to Favorites </span></a>--}}{{--
                            --}}{{--</div>--}}{{--

                        --}}{{--</div>--}}{{--
                    --}}{{--@endforeach--}}{{--
                --}}{{--</div>--}}{{--

            --}}{{--</div>--}}{{--
        --}}{{--</div>--}}{{--


    @else--}}

        <div class="container" style="margin-bottom: 100px">
        <div class="col-lg-12 content-box layout-section">
            <div class="col-lg-12 box-title no-border">
                <div class="inner">
                    <h2>
                        <span class="title-3">Latest <span style="font-weight: bold;">Ads</span></span>
                    </h2>
                </div>
            </div>

            <div class="adds-wrapper noSideBar">

            </div>

        </div>
    </div>



</div>

<!-- FOOTER -->
<!--footer section start-->
@stop
