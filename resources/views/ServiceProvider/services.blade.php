@extends('adminlte::page')
@section('title', 'AdminLTE')

@section('content_header')
    <h1>Services</h1>
@stop
@section('content')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn7IlopznAGZ4LYnBmtwAHOSdLSkk5TU&sensor=false&libraries=places&language=en" async defer></script>


    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
        .pac-container {
            z-index: 10000 !important;
        }
    </style>


    <div class="modal fade" id="postserviceModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h3 class="modal-title">Post Service</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="box-body no-padding">
                        @if (count($errors)>0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <form role="form" action="{{url('/ServiceProvider/addService')}}" id="postAdForm" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" class="form-control" id="ad_title" name="title" placeholder="Enter the title of service" required>
                                </div>

                                <div class="form-group">
                                    <label>Select Service Category</label>
                                    <select name="Categories" class="form-control" id="Categories">
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">About Service</label>
                                    <textarea id="ad_description" name="description" style="height: 130px" class="form-control" placeholder="Write text here..." required></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <input type="tel" pattern="^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$" class="form-control" id="ad_title" name="tel_no" placeholder="" max="10" required>
                                </div>

                                <div class="form-group">
                                    <label>Location of Service</label>
                                    <input type="text" class="form-control" placeholder="Enter Location.." id="loc" name="loc" required="true">
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="lat" id="lat" value="0.00">
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="lng" id="lng" value="0.00">
                                </div>

                                <script>
                                    var input = document.getElementById('loc');
                                    var autocomplete = new google.maps.places.Autocomplete(input);

                                    autocomplete.addListener('place_changed', function() {
                                        var place = autocomplete.getPlace();
                                        console.log("Lat :" + place.geometry.location.lat());
                                        console.log("Lng :" + place.geometry.location.lng());
                                        $("#lat").val(place.geometry.location.lat());
                                        $("#lng").val(place.geometry.location.lng());
                                    });
                                </script>


                                <div class="form-group">
                                    <label id="price" for="exampleInputFile">Price Per Person</label>
                                    <input  class="form-control" type="number" id="date" name="rent" min="0" required >
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputFile">Add Picture</label>
                                    <input type="file" name="picture" id="pic" required>
                                    <p class="help-block">Only JPG or PNG Types are allowed</p>
                                </div>
                            </div>

                            <!-- /.box-body -->

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" style="float: right" >Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">All Services</h3>
                    <button type="button" class="btn btn-success add-new" data-toggle="modal" data-target="#postserviceModal" style="float: right"><i class="fa fa-plus"></i> Add New Service</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">

                    <div class="adds-wrapper noSideBar">
                        @if(sizeof($services))
                        @foreach($services as $service)
                            <div class="item-list make-grid">
                                <div class="col-sm-2 no-padding photobox">
                                    <div class="add-image">
                                        <a href="{{url('ServiceProvider/viewService/'.$service->id)}}">
                                            <img class="thumbnail no-margin center-cropped" src="{{$service->picture}}"  alt="img">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-sm-7 add-desc-box">
                                    <div class="add-details">
                                        <h5 class="add-title">
                                            <a href="#">{{$service->title}}</a>
                                        </h5>
                                        <span class="info-link">
                                            {{$service->service_category->name}}
                                        </span>
                                        <span class="info-row">
                                            <span class="date">{{$service->created_at}}</span><br>
                                        </span>
                                        <h4 class="item-price">
                                            {{$service->price_per_person}} Rs/<small>{{ $service->service_category_id == 4 || $service->service_category_id == 5 || $service->service_category_id == 6 ? 'Event' : 'Person' }}</small>
                                        </h4>
                                    </div>
                                </div>

                            </div>
                        {{--//Update Model--}}

                        @endforeach
                        @else
                            <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px; text-align: center">No Services</h4>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>





@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#Categories').on('change', function() {
                if (this.value == 4 || this.value == 5 || this.value == 6)
                {
                   // alert( $('#price').html() == "Price Per Event" );
                    $('#price').html("Price Per Event");
                }else{
                    $('#price').html("Price Per Person");
                }
            });
            $('#CategoriesUpd').on('change', function() {
                if (this.value == 4 || this.value == 5 || this.value == 6)
                {
                    // alert( $('#price').html() == "Price Per Event" );
                    $('#priceupd').html("Price Per Event");
                }else{
                    $('#priceupd').html("Price Per Person");
                }
            });
        });
    </script>
@stop

