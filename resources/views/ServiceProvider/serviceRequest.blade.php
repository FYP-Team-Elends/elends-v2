@extends('adminlte::page')
@section('title', $title)

@section('content')
        <div class="row">
            <div class="col-md-12">
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Booking Requests</h3>
                    </div>
                    <div class="box-body no-padding">
                    @if(sizeof($servicebookings)>0)
                    @foreach($servicebookings as $booking)
                       <div class="col-lg-12 col-sm-12 col-md-12">
                        <div class="col-sm-2 col-md-2 col-lg-2 no-padding photobox" style="width: 200px; height: 200px">
                            <a href="#">
                                <img class="thumbnail no-margin" src="{{$booking->service->picture}}" alt="img" style="width: 200px;height: 150px">
                            </a>
                        </div>
                        <div class="col-sm-8 col-md-8 col-lg-8">
                            <h3 class="add-title" style="margin-top: 10px">
                                {{$booking->service->title}}
                            </h3>
                            <span><i class="fas fa-ad" style="font-size: 20px;"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 10px; padding-bottom: 8px"><a href="#" style="color: #bf5329">{{$booking->function_type}}</a></span></span></a><br>
                            <span><i class="far fa-envelope"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 16px; padding-bottom: 8px">{{$booking->message}}</span></span><br>
                            <span><i class="far fa-clock"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 16px; padding-bottom: 8px">{{$booking->created_at}}</span></span><br>
                            <div class="col">
                                <a type="button" class="btn btn-success btn-sm make-favorite" data-toggle="modal" data-target="{{'#viewserviceModal'.$booking->id}}" style="margin-top: 20px"><i class="fa fa-eye"></i> View Request</a>
                                     {{--{{dd(url('ServiceProvider/chat/'.$booking->id))}}--}}
                                <a href="{{url('ServiceProvider/chat/'.$booking->id)}}" class="btn btn-default btn-sm make-favorite" style="margin-top: 20px">
                                    <i class="fas fa-envelope"></i><span> Send Message </span>
                                </a>
                            </div>
                        </div>
                    </div>

                    {{--Mpdel--}}

                        <div class="modal fade" id="{{'viewserviceModal'.$booking->id}}">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">

                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h3 class="modal-title">{{$booking->service->title}}</h3>
                                    </div>

                                    <!-- Modal body -->
                                    <div class="modal-body">
                                        <div class="box-body no-padding">
                                            <form id="viewform"  disabled>
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label>Function Type</label>
                                                        <input type="text" class="form-control" id="ad_title" name="title" placeholder="Ad Title" value="{{$booking->function_type}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Total Guests</label><i class="fa fa-users"></i>
                                                        <input type="text" class="form-control"  value="{{$booking->guests}}"  disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="exampleInputEmail1">Message about Request</label>
                                                        <textarea id="ad_description" name="description" style="height: 130px" class="form-control"   disabled>{{$booking->message}}</textarea>
                                                    </div>


                                                    <div class="form-group">
                                                        <label for="exampleInputFile">Location</label>
                                                        <input  class="form-control" type="text" id="loc" name="loc" value="{{$booking->service->location_name}}" disabled >
                                                    </div>

                                                </div>

                                                <!-- /.box-body -->
                                            </form>
                                            <div class="modal-footer">
                                                @if($booking->request_status != "completed" && $booking->request_status != "rejected")
                                                <a class="btn btn-success btn-sm make-favorite" style="margin-top: 20px">

                                                    <form action="{{ url('ServiceProvider/BookingRequests') }}" method="post"  >
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$booking->id}}">
                                                        <input type="hidden" name="status" value="{{$booking->request_status == "accepted" ? 'completed' : 'accepted'}}">
                                                        <button type="submit" style="background: transparent;border: none"><strong><i class="fas fa-check"></i><span> {{$booking->request_status == "accepted" ? 'Mark Completed' : 'Confirm'}} </span></strong></button>
                                                    </form>
                                                </a>
                                                <a class="btn btn-warning btn-sm make-favorite" style="margin-top: 20px">
                                                    <form action="{{ url('ServiceProvider/BookingRequests') }}" method="post" >
                                                        @csrf
                                                        <input type="hidden" name="id" value="{{$booking->id}}">
                                                        <input type="hidden" name="status" value="rejected">
                                                        <button type="submit"  style="background: transparent;border: none"><strong><i class="far fa-times-circle"></i><span> Cancel</span></strong></button>
                                                    </form>
                                                </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                    @endforeach
                    @else
                        <div style="margin: 80px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                            <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                            <h4 style="margin:15px; text-align: center">No {{$title}}</h4>
                        </div>
                    @endif
                </div>
                </div>
            </div>

        </div>
        <!-- //slider -->

    <!-- FOOTER -->
    <!--footer section start-->
@stop

@section('adminlte_js')
    <script src="{{asset('vendor/adminlte/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop
