@extends('adminlte::page')
@section('title', 'Product')

@section('content')

    <style>
        .center-cropped {
            object-fit: none;/* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 400px;
            width: 800px;
        }

        .pac-container {
            z-index: 10000 !important;
        }
    </style>

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <div class="main-container">

        <div class="modal fade" id="updateServiceModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">


                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3 class="modal-title">Update Service</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="box-body no-padding">
                            <form role="form" action="{{url('/ServiceProvider/updateService')}}" id="updateAdForm" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" value="{{$service->id}}">

                                <div class="box-body">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" id="ad_title" name="title" placeholder="Ad Title" value="{{$service->title}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="Categories" class="form-control" id="CategoriesUpd">
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" {{$category->id == $service->service_category_id ? 'Selected="selected"': "" }}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">About</label>
                                        <textarea id="ad_description" name="description" style="height: 130px" class="form-control" placeholder="About service"  required>{{$service->about}}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Contact Number</label>
                                        <input type="tel" pattern="^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$" class="form-control" id="ad_title" name="tel_no" placeholder="" value="{{$service->contact}}" max="10" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputFile">Location</label>
                                        <input  class="form-control" type="text" id="loc" name="loc" value="{{$service->location_name}}" required >
                                    </div>

                                    <div class="form-group">
                                        <label id="priceupd" for="exampleInputFile">{{ $service->service_category_id == 4 || $service->service_category_id == 5 || $service->service_category_id == 6 ? 'Price Per Event' : 'Price Per Person' }}</label>
                                        <input  class="form-control" type="number" id="date" name="rent" value="{{$service->price_per_person}}" required >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Update Picture</label>
                                        <input type="file" name="picture" id="exampleInputFile">
                                        <p class="help-block">Only JPG or PNG Types are allowed</p>
                                    </div>
                                </div>

                                <!-- /.box-body -->

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-success" style="float: right" >Update</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="modal fade" id="deleteServiceModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h3 class="modal-title">Delete '{{$service->title}}'</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="box-body no-padding">
                            <form role="form" action="{{url('ServiceProvider/deleteService/'.$service->id)}}" id="updateAdForm" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div>
                                    <p>Do you really want to delete this Service?</p>
                                </div>

                                <!-- /.box-body -->

                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger" style="float: right" >Delete Service</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="col-md-12">
            @if(!Auth::User()->picture || !Auth::User()->city)
                <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
                    <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Your Profile is incomplete, Please complete it</span>
                    <a href="{{url('AdminLte/myProfie')}}">click to view profile</a>
                </div>
            @endif
            <br>
        </div>

        <div class="h-spacer"></div>
        <div class="container">
            <div class="row">


                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <h2 class="enable-long-words">
                            <strong>
                                <a href="#">
                                    {{$service->title}}
                                </a>
                            </strong>
                        </h2>
                        <span class="info-row">
							<span class="category">{{$service->service_category->name}}</span> -&nbsp;
							<span class="item-location"><i class="fa fa-map-marker"></i> {{$service->locaion_name}} </span>
						</span>

                        <div class="ads-image">
                            <img style="width:100%; height: 500px;" src="{{$service->picture}}" alt="image">
                        </div>
                        <!--ads-image-->

                        <div class="ads-details">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab-details" data-toggle="tab"><h4>Service Details</h4></a>
                                </li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-details">
                                    <div class="row" style="padding: 10px;">
                                        <div class="ads-details-info col-md-12 col-sm-12 col-xs-12 enable-long-words from-wysiwyg">

                                            <!-- Location -->
                                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                                <div>
                                                    <span><i class="fa fa-map-marker"></i> Location: </span>
                                                    <span>
                                                        <a href="#">
                                                        {{$service->location_name}}
														</a>
													</span>
                                                </div>
                                            </div>
                                            <!-- Price / Salary -->

                                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                                <div>
														<span>
															@if($service->service_category->category == 'Movie Maker'||
															    $service->service_category->category == 'Photographer'||
															    $service->service_category->category == 'Lightening')
                                                                Price Per Event:
                                                            @else
                                                                Price Per Person:
                                                            @endif
														</span>
                                                    <span>
                                                       {{$service->price_per_person}} PKR
                                                    </span>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                            <hr>

                                            <!-- Description -->
                                            <div class="detail-line-content">
                                                <p>{{$service->about}}</p>
                                            </div>

                                            <!-- Custom Fields -->

                                            <!-- Tags -->
                                            <div style="clear: both;"></div>



                                            <!-- Custom Fields -->

                                            <!-- Tags -->

                                            <!-- Actions -->
                                        </div>

                                        <br>&nbsp;<br>
                                    </div>
                                </div>


                            </div>
                            <!-- /.tab content -->

                        </div>
                    </div>
                    <!--/.ads-details-wrapper-->
                </div>
                <div class="col-sm-3 page-sidebar-right">
                    <aside>
                            <div class="panel sidebar-panel panel-contact-seller">
                                <div class="panel-heading">Manage</div>
                                <div class="panel-content user-info">
                                    <div class="panel-body text-center">
                                        <div class="seller-info">
                                            <h3 class="no-margin">{{$service->user->first_name}}</h3>
                                            <p>
                                                Location:&nbsp;
                                                <strong>
                                                    <a href="#">
                                                        {{$service->user->city}}
                                                    </a>
                                                </strong>
                                            </p>
                                        </div>
                                        <div class="user-ads-action">
                                            <a class="btn btn-default btn-block" data-toggle="modal" data-target="#updateServiceModal" href="#">
                                                <i class="far fa-pencil-square"></i> Update Detail
                                            </a>
                                            <br>
                                            @if($service->isActive)
                                                <a class="btn btn-default btn-block btn-success" href="{{url('/ServiceProvider/availability?id='.$service->id.'&status=0')}}">
                                                    <i class="far fa-pencil-square"></i> Active
                                                </a>
                                            @else
                                                <a class="btn btn-default btn-block btn-default" href="{{url('/ServiceProvider/availability?id='.$service->id.'&status=1')}}">
                                                    <i class="far fa-pencil-square"></i> Deactivated
                                                </a>
                                            @endif
                                            <br>
                                            <a class="btn btn-default btn-block btn-danger" data-toggle="modal" data-target="#deleteServiceModal" href="#">
                                                <i class="far fa-pencil-square"></i> Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </aside>
                </div>
                <!--/.page-content-->
            </div>

        </div>

        <div class="h-spacer"></div>

    </div>

@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>

@stop
