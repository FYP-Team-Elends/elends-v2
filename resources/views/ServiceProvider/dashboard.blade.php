@extends('adminlte::page')
@section('title', 'AdminLTE')

@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('content')
    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
        .corner {
            width: 0;
            height: 0;
            border-top: 80px solid #ffcc00;
            border-bottom: 80px solid transparent;
            border-left: 80px solid transparent;
            position:absolute;
            right:0;
        }

        .corner span {
            position:absolute;
            top: -65px;
            width: 70px;
            left: -65px;
            text-align: center;
            font-size: 16px;
            font-family: arial;
            transform: rotate(45deg);
            display:block;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>

    <div class="row">
        <div class="col-md-12" id="message">
            @if(!Auth::User()->email_verified_at)
                <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
                    <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Your Email is not verified, Please verify your Email</span>
                </div>
                <br>
            @endif
            @if(!Auth::User()->picture || !Auth::User()->city)
                <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
                    <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Your Profile is incomplete, Please complete it</span>
                    <a href="{{url('AdminLte/myProfie')}}">click to view profile</a>
                </div>
            @endif
            <br>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="ion ion-ios-analytics-outline"></i>
                </span>


                <div class="info-box-content">
                    <span class="info-box-text">Total Services</span>
                    <span class="info-box-number">{{$Complete_Bookings_count}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-pull-request"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Requests Received</span>
                    <span class="info-box-number"> {{$Request_received_count}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-bookmarks"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Bookings</span>
                    <span class="info-box-number">{{$Complete_Bookings_count}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-pull-request"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Requests Cancelled</span>
                    <span class="info-box-number">{{$Request_cancelled_count}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box col-md-12">

                <div class="row">
                    <div class="box-header with-border col-md-6">
                        <h3 class="box-title">Total Service Requests</h3>

                        <div class="col-md-12" style="width: 100%; height: 80%">
                            {!! $chart_for_booking_request->container() !!}
                        </div>
                    </div>
                    <div class="box-header with-border col-md-6">
                        <h3 class="box-title">Total Service Bookings</h3>
                        <div class="col-md-12" style="width: 100%; height: 80%">
                            {!! $chart_for_complete_booking->container() !!}
                        </div>
                    </div>


                </div>


                <!-- /.box-header -->
                {{--<div class="box-header with-border col-md-6">
                    <h3 class="box-title">Users On The Base Of City</h3>

                    <div class="col-md-12" style="width: 100%; height: 80%">
                        {!! $chart1->container() !!}
                    </div>
                </div>--}}
            </div>
        </div>
    </div>

@stop
@section('adminlte_js')
    <script src="{{asset('vendor/adminlte/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('vendor/highcharts/highcharts.js')}}"></script>
    <script src="{{asset('vendor/highcharts/exporting.js')}}"></script>
    <script src="{{asset('vendor/highcharts/export-data.js')}}"></script>

    <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>
    {!! $chart_for_booking_request->script() !!}
    {!! $chart_for_complete_booking->script() !!}
@stop

