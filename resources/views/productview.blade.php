@extends('layouts.master')

@section('content')
    <div class="main-container">

        <div class="h-spacer"></div>


        <div class="container">
            <div class="row">
                <div class="col-sm-9 page-content col-thin-right">
                    <div class="inner inner-box ads-details-wrapper">
                        <h2 class="enable-long-words">
                            <strong>
                                <a href="#">
                                    {{$ad->title}}
                                </a>
                            </strong>
                        </h2>
                        <span class="info-row">
							<span class="date"><i class=" icon-clock"> </i> 1 month ago </span> -&nbsp;
							<span class="category">Pets &amp; Mascots</span> -&nbsp;
							<span class="item-location"><i class="fa fa-map-marker"></i> Lahore </span> -&nbsp;

						</span>

                        <div class="ads-image">
                            <h1 class="pricetag">
                                {{$ad->rent}} PKR/DAY
                            </h1>
                            <div class="bx-wrapper" style="max-width: 100%;"><div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 273px;"><ul class="bxslider" style="width: 1215%; position: relative; transition-duration: 0s; transform: translate3d(-496px, 0px, 0px);">

                                        <li style="float: left; list-style: none; position: relative; width: 496px;" aria-hidden="false"><img src="http://localhost:8000/images/1.jpg" alt="img"></li>
                                        <li style="float: left; list-style: none; position: relative; width: 496px;" class="bx-clone" aria-hidden="true"><img src="http://localhost:8000/images/1.jpg" alt="img"></li>
                                    </ul></div>
                                </div>
                            <div class="product-view-thumb-wrapper">
                                <div class="bx-wrapper" style="max-width: 100px;"><div class="bx-viewport" aria-live="polite" style="width: 100%; overflow: hidden; position: relative; height: 75px;"><ul id="bx-pager" class="product-view-thumb" style="width: 1215%; position: relative; transition-duration: 0s; transform: translate3d(-100px, 0px, 0px);"><li aria-hidden="true" style="float: left; list-style: none; position: relative; width: 90px; margin-right: 10px;" class="bx-clone">
                                                <a class="thumb-item-link active" data-slide-index="0" href="">
                                                    <img src="http://localhost:8000/images/1.jpg" alt="img">
                                                </a>
                                            </li>
                                            <li aria-hidden="false" style="float: left; list-style: none; position: relative; width: 90px; margin-right: 10px;">
                                                <a class="thumb-item-link active" data-slide-index="0" href="">
                                                    <img src="http://localhost:8000/images/1.jpg" alt="img">
                                                </a>
                                            </li>
                                            <li aria-hidden="true" style="float: left; list-style: none; position: relative; width: 90px; margin-right: 10px;" class="bx-clone">
                                                <a class="thumb-item-link active" data-slide-index="0" href="">
                                                    <img src="http://localhost:8000/images/1.jpg" alt="img">
                                                </a>
                                            </li></ul></div>
                                    </div>
                            </div>
                        </div>
                        <!--ads-image-->






                        <div class="ads-details">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab-details" data-toggle="tab"><h4>Ad Details</h4></a>
                                </li>

                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-details">
                                    <div class="row" style="padding: 10px;">
                                        <div class="ads-details-info col-md-12 col-sm-12 col-xs-12 enable-long-words from-wysiwyg">

                                            <!-- Location -->
                                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                                <div>
                                                    <span><i class="fa fa-map-marker"></i> Location: </span>
                                                    <span>
																												<a href="http://laraclassified.bedigit.com/free-ads/lahore/1172451">
															Lahore
														</a>
													</span>
                                                </div>
                                            </div>
                                            <!-- Price / Salary -->
                                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                                <div>
														<span>
															Rent:
														</span>
                                                    <span>
                                                        {{$ad->rent}}
                                                    </span>
                                                </div>
                                            </div>
                                            <div style="clear: both;"></div>
                                            <hr>

                                            <!-- Description -->
                                            <div class="detail-line-content">
                                                <p>{{$ad->description}}</p>
                                            </div>

                                            <!-- Custom Fields -->

                                            <!-- Tags -->
                                            <div style="clear: both;"></div>

                                            <!-- Actions -->
                                        </div>

                                        <br>&nbsp;<br>
                                    </div>
                                </div>

                                <div class="reviews-widget tab-pane" id="tab-reviews">
                                    <div class="row">


                                        <div class="well" id="reviews-anchor">
                                            <div class="row">
                                                <div class="col-md-12">
                                                </div>
                                            </div>

                                            <div class="row" id="post-review-box">
                                                <div class="col-md-12">
                                                    <form action="http://laraclassified.bedigit.com/post/5564/review/create" method="post">
                                                        <input type="hidden" name="_token" value="jZaRePYYpXCSkNg4RsWIMz5Xrhmke6DB9cGggLiq">
                                                        <input type="hidden" name="rating" id="rating">

                                                        <textarea name="comment" id="comment" rows="5" class="form-control animated" placeholder="Enter your review here..." style="overflow: hidden visible; overflow-wrap: break-word; resize: none;"></textarea>

                                                        <div class="text-right">
                                                            <div class="stars starrr" data-rating="0"><a href="#" class="fa fa-star-o"></a><a href="#" class="fa fa-star-o"></a><a href="#" class="fa fa-star-o"></a><a href="#" class="fa fa-star-o"></a><a href="#" class="fa fa-star-o"></a></div>
                                                            <button class="btn btn-success btn-lg" type="submit">Leave a Review</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>


                            </div>
                            <!-- /.tab content -->

                        </div>
                    </div>
                    <!--/.ads-details-wrapper-->
                </div>
                <!--/.page-content-->

                <div class="col-sm-3 page-sidebar-right">
                    <aside>
                        <div class="panel sidebar-panel panel-contact-seller">
                            <div class="panel-heading">Contact Advertiser</div>
                            <div class="panel-content user-info">
                                <div class="panel-body text-center">
                                    <div class="seller-info">
                                        <h3 class="no-margin">{{$ad->item->user->first_name}}</h3>
                                        <p>
                                            Location:&nbsp;
                                            <strong>
                                                <a href="http://laraclassified.bedigit.com/free-ads/lahore/1172451">
                                                    Lahore
                                                </a>
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="user-ads-action">
                                        <a href="#contactUser" data-toggle="modal" class="btn btn-default btn-block">
                                            <i class="far fa-envelope"></i> Send a message
                                        </a>
                                        <a href="#contactUser" data-toggle="modal" class="btn btn-default btn-block">
                                            <i class="far fa-heart"></i> Add to wishlist
                                        </a>
                                        <a href="tel:+923475349388" class="btn btn-success btn-block showphone">
                                            <i class="fas fa-phone"></i>
                                            <img src="data:image/png;charset=utf-8;base64,iVBORw0KGgoAAAANSUhEUgAAAGsAAAAMCAYAAABlTkPuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAADnElEQVRYw62YT2hVRxTGv3uRUESkBFtKkSASSnh0JV1IyKKEICJBgrgIUkSCSCrBZiEipZsQXIgUkSIiUroQkVJEQVSkC3EhXYikIiJFixRrqUjwTytJquHn5nswDPPunVfvwOVlzvlmvnPPnHvOmUgZA3gPOAo8AV4BPwOfBPpR4CawAPwO7InW7wJuA0vAQ2C6A08v8FFCfoL0+MH6+wnd3ab4M31QywFM2dYFYA4Y+z+YusP6EZgHdgNbgVvesPR8GfgWGPEvwLjXfua1B4BNwGHrdyZ4ZoHHQBnJ1wODwTNspxwMDutshGk1xZ/hg1oOYMJ++saYk54PdYOpO6iPvWBvIOu3MdsccWPRmjngWjBfHelvApciWY+j9lCGTV8Ar9tfgZ12pAL/Tvx1PsjhAK4DVyLMH8CJXEwcweeBs5Gt/cb92hYURfFA0ktJraIoFouiuBCt+VPSmgD/MtKXkv6NZOOSeiUdz4ihLyVdLori75yAa4C/0geZHKWkxQizKOm/XEwZRFWP52Uwl6Tn/v0wONRS0htJH3TwT0vSg1SEA/skDUg6Gqm/knShKIq/ar6qlqRBSSfV5XgH/mwfVHCckzTq2lYCWyWtlXQqG2NharwODLjvHN0P9Dl/LwMzCYcMe/32DvKFRAMyZN1QUB9Od3D4MeBRWFds329OYwsu/uua5M/xQRWH9aesf2zMcNcYYIOfa8DV9jzSP/ImS8CMa8ZEomO6B9xIGPE+sBHYZyMOBLqfgLlgfjF1WN7/GTAbyb92cR8Cxm3rrSb5c3xQw7EFeOoGZcod49PIz7WYuprV1q0ABmzQqI3uS0TFsziqO7Tir/z3Wr/0ZDv9utCeCVJxe91OR3Nfzf4Ttm+gYf5aH3TgWA28CJsXYKVb/du5mOzDCjBrnBLORfJJv/RIRu046BftBXZQPXqCdTeAqxn7j3jtcJP8dT6o4Gin2c0RZsbylTmYbgv0mFvJO+HlEfjcB7WnQ+panwiKJ0Hq2BA9d4BLUYr4NGyVo/1WJdLisr+aRvirfJDB0bLtkxHme+Af9w21mNxD2hjcqo+EdwpfWOeBK9GldNBp45Dz7t7o0jxdwfdLomYcdgOxIpJvsXy/L5L7/R+G09Y3xV/lg1oONz3zAWY20aDUYnIvxrvji19QR5LdpA+rx6QPXZjvxdGT6awyjt5AN+1ucMlRP9s+1Ab5q3xQywGsAr5zl7cE3AWmusG8BQTbzo62PAR1AAAAAElFTkSuQmCC">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>





                    </aside>
                </div>
            </div>

        </div>

        <div class="h-spacer"></div>

    </div>
@stop