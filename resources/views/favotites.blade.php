<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>My Favorites</title>
    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/bootstrap-select.css"><!-- bootstrap-select-CSS -->
    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/font-awesome.min.css" /><!-- fontawesome-CSS -->
    <link rel="stylesheet" href="csss/menu_sideslide.css" type="text/css" media="all"><!-- Navigation-CSS -->
    <!--fonts-->
    <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--//fonts-->
    <!-- js -->
    <script type="text/javascript" src="jss/jquery.min.js"></script>
    <!-- js -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="jss/bootstrap.js"></script>
    <script src="jss/bootstrap-select.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="css/slick.css" />
    <link type="text/css" rel="stylesheet" href="css/slick-theme.css" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />
    <link type="text/css" rel="stylesheet" href="css/favstyle.css">
</head>
<body>
<!-- HEADER -->
<div id="header">
    <!-- top Header -->
    <div id="top-header">
        <div class="container">
            <div class="pull-left">
                <span>Welcome to E-Lends!</span>
            </div>
            <div class="pull-right">
                <ul class="header-top-links">
                    <li><a href="#">Store</a></li>
                    <li><a href="#">Newsletter</a></li>
                    <li><a href="#">FAQ</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /top Header -->
    <div class="container">
        <div class="pull-left">
            <!-- Logo -->
            <div class="header-logo" style="float: left;">
                <a class="logo" href="#">
                    <img src="./img/logo.png" alt="">
                </a>
            </div>
            <!-- /Logo -->

            <!-- Search -->
            <div class="header-search">
                <form>
                    <input class="input search-input" type="text" placeholder="Enter your keyword">
                    <select class="input search-categories">
                        <option value="0">Select Event</option>
                        <option value="1">Mehandi</option>
                        <option value="1">Barat</option>
                        <option value="1">Nikha</option>
                        <option value="1">Valima</option>
                        <option value="1">Party</option>
                        <option value="1">Birthday</option>
                        <option value="1">Bridal Shower</option>
                    </select>
                    <button class="search-btn"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- /Search -->
        </div>
        <div class="pull-right">
            <ul class="header-btns">
                <!-- Account -->
                <li class="header-account dropdown default-dropdown">
                    <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">
                            <i class="fa fa-user-o"></i>
                        </div>
                        <strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
                    </div>
                    <a href="#" class="text-uppercase">Login</a> / <a href="#" class="text-uppercase">Join</a>
                    <ul class="custom-menu">
                        <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
                        <li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li>
                        <li><a href="#"><i class="fa fa-exchange"></i> My Ads</a></li>
                        <li><a href="#"><i class="fa fa-check"></i> Messages</a></li>
                        <li><a href="#"><i class="fa fa-unlock-alt"></i> Settings</a></li>
                        <li><a href="#"><i class="icon mini abs power"></i> Log out</a></li>
                    </ul>
                </li>
                <!-- /Account -->

                <!-- Cart -->
                <li class="header-cart dropdown default-dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">
                            <i class="fa fa-heart-o"></i>
                        </div>
                        <strong class="text-uppercase">My Favourites:</strong>
                        <br>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<!-- /Favourites -->
<div class="wrapper">
    <div class="clr">
        <div class="pageinfo clr " style="margin-top: 10px; margin-bottom: 20px">
            <h2 class="large">Your Favorite Ads</h2>
            <p class="small">Here, You will find your favorite Ads </p>
        </div>
        <ul class="tabs2 clr">
            <li class="fleft">
                 <span class="fleft tab selected">
               <span class="fbold">Favorites Ads</span>
             </span>

            </li>
        </ul>
    </div>
</div>
<section class="container" id="body-container">
    <div class="wrapper">
        <div class="content pding0" style="margin-bottom:  1px; ">
            <h2 class="pading30_0 text-center " style="font-size: 18px"> You don't have Favorites Ads</h2>
            <div class="text-center favemptybox brtope5 pdingtop30">
                <p class="c27" style="color: rgba(172,172,172,0.95)">You can mark an Ad as favorite on Ads list or on the Ad page</p>
                <img src="img/fav.PNG" style="margin-top: 40px; opacity:0.4; z-index:9;">
            </div>
            <p class="pding30 text-center cab" style="margin-top: 10px;color: #acacac;">Mark your favorite Ads and keep them organized - also on your phone</p>
        </div>
    </div>
</section>
<!-- /Favourites -->

<!-- /Favourites -->
<!-- FOOTER -->
<footer id="footer" class="section section-grey">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="app">
                    <h3>DOWNLOAD APP</h3>
                    <img src="./img/app.jpg" style="width: 180px">
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Information</h3>
                    <ul class="list-links">
                        <li><a href="#">Location Map</a></li>
                        <li><a href="#">Popular Seraches</a></li>
                        <li><a href="#">term of use</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <div class="clearfix visible-sm visible-xs"></div>

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">HELP</h3>
                    <ul class="list-links">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Sitemap</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer subscribe -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Contact Us</h3>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-earphone"></span></li>
                        <li>+0 561 111 235</li>
                    </ul>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-envelope"></span></li>
                        <li><a href="mailto:info@example.com">mail@example.com</a></li>
                    </ul>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-briefcase"></span></li>
                        <li><strong>Business Packages</strong></li>
                        <li><small>(featured ads, advertising)</small></li><br>
                        <li><a href="#">click here</a></li>
                    </ul>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- /footer subscribe -->
        </div>
        <!-- /row -->
        <!-- /row -->
    </div>
    <!-- /container -->
</footer>
<!--footer section start-->
<footer>
    <div class="agileits-footer-bottom text-center">
        <div class="container">
            <div class="w3-footer-logo">
                <h1><a href="index.blade.php"><span>E-</span>Lend</a></h1>
            </div>
            <div class="w3-footer-social-icons">
                <ul>
                    <li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a></li>
                    <li><a class="flickr" href="#"><i class="fa fa-flickr" aria-hidden="true"></i><span>Flickr</span></a></li>
                    <li><a class="googleplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i><span>Google+</span></a></li>
                    <li><a class="dribbble" href="#"><i class="fa fa-dribbble" aria-hidden="true"></i><span>Dribbble</span></a></li>
                </ul>
            </div>
            <div class="col-md-8 col-md-offset-2 text-center">
                <!-- footer copyright -->
                <div class="footer-copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | We <i class="fa fa-heart-o" style="color: red" aria-hidden="true"></i> to make you happy</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </div>
                <!-- /footer copyright -->
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</footer>
<!--footer section end-->

<!-- jQuery Plugins -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

</body>

<!-- Navigation-JavaScript -->
<script src="js/classie.js"></script>
<script src="js/main.js"></script>
<!-- //Navigation-JavaScript -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- //here ends scrolling icon -->
</html>