
@extends('layouts.master')

@section('content')

	<!-- NAVIGATION -->
	<div id="navigation">
		<!-- container -->
		<div class="container">


		</div>
		<!-- /container -->
	</div>
	<!-- /NAVIGATION -->

	<!-- HOME -->

	<!-- /HOME -->

	<div>
		<div class="container" style="margin-top: 50px; margin-bottom: 100px">
			<div class="row row-featured row-featured-category">
				<div class="col-lg-12 col-sm-12 col-md-12 box-title no-border">
					<div class="inner">
						<h2>
						<span class="title-3">
							Requests for
							<span style="font-weight: bold;">Booking</span>
							<span>(3)</span>
						</span>
						</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="col-sm-2 col-md-2 col-lg-2 no-padding photobox" style="width: 200px; height: 200px">
							<a href="http://laraclassified.bedigit.com/все-по-50-руб.-пмр/5941">
								<img class="thumbnail no-margin" src="http://laraclassified.bedigit.com/storage/files/us/5941/thumb-320x240-278f01efe915dabaee834e837435d014.jpg?v=1" alt="img">
							</a>
						</div>
						<div class="col-sm-8 col-md-8 col-lg-8">
							<h3 class="add-title" style="margin-top: 10px">
								Hamza Rasool
							</h3>
							<span><i class="fas fa-ad" style="font-size: 20px;"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 10px; padding-bottom: 8px"><a href="#" style="color: #bf5329">Bridal dresses are on rent</a></span></span></a><br>
							<span><i class="far fa-clock"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 16px; padding-bottom: 8px">12-12-2018</span></span><br>
							<a class="btn btn-danger btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-check"></i><span> Confirm Booking </span>
							</a>
							<a class="btn btn-warning btn-sm make-favorite" style="margin-top: 20px">
								<i class="far fa-times-circle"></i><span> Cancel Booking </span>
							</a>
							<a class="btn btn-default btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-envelope"></i><span> Send Message </span>
							</a>
						</div>

					</div>
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="col-sm-2 col-md-2 col-lg-2 no-padding photobox" style="width: 200px; height: 200px">
							<a href="http://laraclassified.bedigit.com/все-по-50-руб.-пмр/5941">
								<img class="thumbnail no-margin" src="http://laraclassified.bedigit.com/storage/files/us/5941/thumb-320x240-278f01efe915dabaee834e837435d014.jpg?v=1" alt="img">
							</a>
						</div>
						<div class="col-sm-8 col-md-8 col-lg-8">
							<h3 class="add-title" style="margin-top: 10px">
								Hamza Rasool
							</h3>
							<span><i class="fas fa-ad" style="font-size: 20px;"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 10px; padding-bottom: 8px"><a href="#" style="color: #bf5329">Bridal dresses are on rent</a></span></span></a><br>
							<span><i class="far fa-clock"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 16px; padding-bottom: 8px">12-12-2018</span></span><br>
							<a class="btn btn-danger btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-check"></i><span> Confirm Booking </span>
							</a>
							<a class="btn btn-warning btn-sm make-favorite" style="margin-top: 20px">
								<i class="far fa-times-circle"></i><span> Cancel Booking </span>
							</a>
							<a class="btn btn-default btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-envelope"></i><span> Send Message </span>
							</a>
						</div>

					</div>
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="col-sm-2 col-md-2 col-lg-2 no-padding photobox" style="width: 200px; height: 200px">
							<a href="http://laraclassified.bedigit.com/все-по-50-руб.-пмр/5941">
								<img class="thumbnail no-margin" src="http://laraclassified.bedigit.com/storage/files/us/5941/thumb-320x240-278f01efe915dabaee834e837435d014.jpg?v=1" alt="img">
							</a>
						</div>
						<div class="col-sm-8 col-md-8 col-lg-8">
							<h3 class="add-title" style="margin-top: 10px">
								Hamza Rasool
							</h3>
							<span><i class="fas fa-ad" style="font-size: 20px;"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 10px; padding-bottom: 8px"><a href="#" style="color: #bf5329">Bridal dresses are on rent</a></span></span></a><br>
							<span><i class="far fa-clock"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 16px; padding-bottom: 8px">12-12-2018</span></span><br>
							<a class="btn btn-danger btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-check"></i><span> Confirm Booking </span>
							</a>
							<a class="btn btn-warning btn-sm make-favorite" style="margin-top: 20px">
								<i class="far fa-times-circle"></i><span> Cancel Booking </span>
							</a>
							<a class="btn btn-default btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-envelope"></i><span> Send Message </span>
							</a>
						</div>
					</div>
					<div class="col-lg-12 col-sm-12 col-md-12">
						<div class="col-sm-2 col-md-2 col-lg-2 no-padding photobox" style="width: 200px; height: 200px">
							<a href="http://laraclassified.bedigit.com/все-по-50-руб.-пмр/5941">
								<img class="thumbnail no-margin" src="http://laraclassified.bedigit.com/storage/files/us/5941/thumb-320x240-278f01efe915dabaee834e837435d014.jpg?v=1" alt="img">
							</a>
						</div>
						<div class="col-sm-8 col-md-8 col-lg-8">
							<h3 class="add-title" style="margin-top: 10px">
								Hamza Rasool
							</h3>
							<span><i class="fas fa-ad" style="font-size: 20px;"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 10px; padding-bottom: 8px"><a href="#" style="color: #bf5329">Bridal dresses are on rent</a></span></span></a><br>
							<span><i class="far fa-clock"></i><span style="margin-top: auto; margin-bottom: auto; margin-left: 16px; padding-bottom: 8px">12-12-2018</span></span><br>
							<a class="btn btn-danger btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-check"></i><span> Confirm Booking </span>
							</a>
							<a class="btn btn-warning btn-sm make-favorite" style="margin-top: 20px">
								<i class="far fa-times-circle"></i><span> Cancel Booking </span>
							</a>
							<a class="btn btn-default btn-sm make-favorite" style="margin-top: 20px">
								<i class="fas fa-envelope"></i><span> Send Message </span>
							</a>
						</div>
					</div>

				</div>
			</div>

		</div>
		<!-- //slider -->
	</div>

	<!-- FOOTER -->
	<!--footer section start-->
@stop