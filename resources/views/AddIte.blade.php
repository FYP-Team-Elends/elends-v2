@extends('layouts.app')

@section('content')


<!-- My Items -->
<div class="submit-ad main-grid-border">
    <div class="container">
        {{--@include('inc.messages')--}}
        <h2 class="w3-head">Add Item</h2>
        <div class="post-ad-form">

            <form action="{{url('/submit_item')}}" method="POST" enctype="multipart/form-data" >
                {{ csrf_field() }}
                <label>Item name <span>*</span></label>
                <input type="text" class="phone custom-tooltip" name="name" placeholder="" style="width: 40%;" >

                <script>
                    /* $(document).ready(function(){
                     $('#tool').click(function(){
                         $('[data-toggle=tooltip]').tooltip('toggle')});

                 });*/
                    $(document).ready(function(){
                        $('.custom-tooltip').tooltip({
                            title:
                            "<ul style='list-style-type: none ; text-align: left; color: #0C0C0C; padding-left: 2px;'>\n" +
                            "  <li><b>Want to get your ad noticed?</b></li><br>\n" +
                            "  <li>• Give your ad an attractive title.</li><br>\n" +
                            "  <li>• Check the spellings and don't use the CAPITAL LETTERS or symbols.</li><br>\n" +
                            "</ul>  ", html: true, placement: "right"
                            ,trigger: "click"});

                    });

                </script>
                <div class="clearfix"></div>
                <label><b>Select Category </b><span>*</span></label>
                <select class="" name="category">
                    <option value=null>Select Category</option>
                    @foreach($category as $item)
                    <option value="{{$item->id}}">{{$item->category}}</option>
                        @endforeach
                </select>
                {{--{{dd($category)}}--}}
                <div class="clearfix"></div>
                <label><b>Select Sub-category </b><span>*</span></label>
                <select class="" name="subcategory" >
                    <option value=null>Select Sub-category</option>
                    @foreach($subcategory as $value)

                    <option value="{{$value->id}}">{{$value->sub_category}}</option>
                    @endforeach
                </select>
                <div class="clearfix"></div>
                <label>Item Description <span>*</span></label>
                <textarea   name="description" class="mess custom-tooltip1" placeholder="Write few lines about your product including the brand, model."></textarea>
                <script>
                    /* $(document).ready(function(){
                     $('#tool').click(function(){
                         $('[data-toggle=tooltip]').tooltip('toggle')});

                 });*/
                    $(document).ready(function(){
                        $('.custom-tooltip1').tooltip({
                            title: "<ul style='list-style-type: none ; text-align: left; color: #0C0C0C; padding-left: 2px;'> <li><b>Items with good description sublet faster!</b></li><br> <li>• Mention the condition, features and brand.</li><br> <li>• Remember, good description needs at least 2-3 sentences.</li><br> </ul> ", html: true, placement: "right" ,trigger: "click"});

                    });
                </script>
                <div class="clearfix"></div>
                <div class="upload-ad-photos">
                    <label>Item Photos:</label>
                    <input type="file" name="path">
                    <div class="photos-upload-view custom-tooltip2">
                        {{--<form id="upload" action="index.blade.php" method="POST" enctype="multipart/form-data">--}}

                            {{--<input type="hidden" name="images[]"  id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" />--}}
                            {{--<div>--}}
                                {{--<input type="file"  id="fileselect" name="path"  />--}}
                                {{--<div id="filedrag">or drop files here</div>--}}
                            {{--</div>--}}

                            {{--<div id="submitbutton">--}}
                                {{--<button type="submit">Upload Photos</button>--}}
                            {{--</div>--}}

                        {{--</form>--}}

                        <script>
                            /* $(document).ready(function(){
                             $('#tool').click(function(){
                                 $('[data-toggle=tooltip]').tooltip('toggle')});

                         });*/
                            $(document).ready(function(){
                                $('.custom-tooltip2').tooltip({
                                    title: "<ul style='list-style-type: none ; text-align: left;padding-left: 2px;'> <li><b style='color: red'>Ads with 4-6 photos get more responses!</b></li><br> <li style='color: #0C0C0C'>• Ensure the item is properly visible from multiple angles.</li><br> <li style='color: #0C0C0C'>• Accepted formats are .jpg, .gif & .png. and Size < 5MB.</li><br> </ul> ", html: true, placement: "right"});

                            });
                        </script>

                    </div>
                    <div class="clearfix"></div>
                    <script src="jss/filedrag.js"></script>
                </div>

                        <div class="clearfix"></div>
                        <p class="post-terms">By clicking <strong>Sbmit Button</strong> you accept our <a href="terms.html" target="_blank">Terms of Use </a> and <a href="privacy.html" target="_blank">Privacy Policy</a></p>
                        <input type="submit" value="Add">
                        <div class="clearfix"></div>
                    </form>
                </div>
            {{--</form>--}}

        </div>
    </div>
</div>
<!-- // My Items -->



@endsection
