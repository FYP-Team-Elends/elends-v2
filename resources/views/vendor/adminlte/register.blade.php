@extends('adminlte::master')

@section('adminlte_css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/css/auth.css') }}">
    @yield('css')
@stop

@section('body_class', 'register-page')

@section('body')
    <div class="register-box">
        <div class="register-logo">
            <a href="{{ url(config('adminlte.dashboard_url', 'home')) }}">{!! config('adminlte.logo', '<b>Admin</b>LTE') !!}</a>
        </div>

        <div class="register-box-body">
            <p class="login-box-msg">{{ session('RegisterRequest') == "ServiceProvider" ? trans('Register as Service Provider') : trans('Register as Service User')/*trans('adminlte::adminlte.register_message')*/ }}</p>
            <form action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('first_name') ? 'has-error' : '' }}">
                    <input type="text" pattern="^[a-zA-Z ]*$" title="Enter Text Only" name="first_name" class="form-control" value="{{ old('first_name') }}"
                           placeholder="{{ "First Name" }}">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('first_name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('first_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('last_name') ? 'has-error' : '' }}">
                    <input type="text" pattern="^[a-zA-Z ]*$" name="last_name" title="Enter Text Only" class="form-control" value="{{ old('last_name') }}"
                           placeholder="{{ "Last Name" }}">
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('last_name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
                    <input type="email" name="email" pattern="^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$" title="abc@xyz.com" class="form-control" value="{{ old('email') }}"
                           placeholder="{{ trans('adminlte::adminlte.email') }}">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
                    <input pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" type="password" name="password" title="Password must be at least 6 characters, no more than 6 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit." class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.password') }}">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                    <input pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" type="password" name="password_confirmation" class="form-control"
                           placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
                <button type="submit"
                        class="btn btn-primary btn-block btn-flat"
                >{{ trans('adminlte::adminlte.register') }}</button>
                <div  style= "text-align: center; margin-bottom:10px">
                    <b>OR</b>
                    <span class="txt1">
                            <p>Sign In with</p>
						</span>
                </div>
                <div class="login with row auth-links">
                    <div>
                        <a href="{{url('login/facebook')}}" class="btn-facebook m-b-10 col-md-5 col-lg-5 btn btn-primary btn-block btn-flat">
                            <i class="fab fa-facebook-square"></i>
                            Facebook
                        </a>
                    </div>
                    <div style="text-align: center; margin-top: 20px" class="form-group">
                        <div class="row"></div>
                    </div>
                    <div>
                        <a href="{{url('/login/google')}}" class="btn-google m-b-10 col-md-5 col-lg-5 btn btn-primary btn-block btn-flat">
                            <i class="fab fa-google-plus-square"></i>
                            Google
                        </a>
                    </div>
                </div>
            </form>
            <div class="auth-links">
                <a href="{{ url(config('adminlte.login_url', 'login')) }}"
                   class="text-center">{{ trans('adminlte::adminlte.i_already_have_a_membership') }}</a>
            </div>
        </div>
        <!-- /.form-box -->
    </div><!-- /.register-box -->
@stop

@section('adminlte_js')
    @yield('js')
@stop
