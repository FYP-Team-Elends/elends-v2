@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop
@section('content')
    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
        .corner {
            width: 0;
            height: 0;
            border-top: 80px solid #ffcc00;
            border-bottom: 80px solid transparent;
            border-left: 80px solid transparent;
            position:absolute;
            right:0;
        }

        .corner span {
            position:absolute;
            top: -65px;
            width: 70px;
            left: -65px;
            text-align: center;
            font-size: 16px;
            font-family: arial;
            transform: rotate(45deg);
            display:block;
        }
    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>

    <div class="row">
        <div class="col-md-12" id="message">
            @if(!Auth::User()->email_verified_at)
                <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
                    <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Your Email is not verified, Please verify your Email</span>
                </div>
                <br>
            @endif
            @if(!Auth::User()->picture || !Auth::User()->city)
                <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
                    <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Your Profile is incomplete, Please complete it</span>
                    <a href="{{url('AdminLte/myProfie')}}">click to view profile</a>
                </div>
            @endif
              <br>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <i class="ion ion-ios-analytics-outline"></i>
                </span>


                <div class="info-box-content">
                    <span class="info-box-text">Total Items</span>
                    <span class="info-box-number">{{$itemcount}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="ion ion-filing"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Posted Ads</span>
                    <span class="info-box-number">{{$postedadcount}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="ion ion-ios-bookmarks"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Bookings</span>
                    <span class="info-box-number">{{$bookingadscount}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="ion ion-pull-request"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Requests Received</span>
                    <span class="info-box-number">{{$reqbookingadscount}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-md-12">
            <div class="box col-md-12">
                <div class="box-header with-border col-md-12">
                    <h3 class="box-title">Requests received</h3>

                    <div class="col-md-12" style="width: 100%; height: 80%">
                        {!! $chart->container() !!}
                    </div>
                </div>
                <!-- /.box-header -->
                {{--<div class="box-header with-border col-md-6">
                    <h3 class="box-title">Users On The Base Of City</h3>

                    <div class="col-md-12" style="width: 100%; height: 80%">
                        {!! $chart1->container() !!}
                    </div>
                </div>--}}
            </div>
        </div>
    </div>

    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Ads you might be interested in</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="adds-wrapper noSideBar">
                        @if(count($intAds)>0)

                        @if(sizeof($intAds)>0)
                            @foreach($intAds as $aditem)
                                <div class="item-list make-grid">
                                    <div class="col-sm-2 no-padding photobox">
                                        <div class="add-image">
                                            <a href="{{url('viewproduct/'.$aditem->id)}}">
                                                <div class="corner"><span>Rs. {{$aditem->rent}}</span></div>
                                                <img class="thumbnail no-margin center-cropped" src="{{$aditem->item->pictures[0]->path}}" alt="img">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-sm-7 add-desc-box">
                                        <div class="add-details">
                                            <h5 class="add-title">
                                                <a style="white-space: nowrap;" href="#">{{$aditem->title}} </a>
                                            </h5>
                                            <span class="info-link">
                                                    {{$aditem->item->category->category}}
                                            </span>
                                            <span class="info-row">
                                            <span class="date"> {{ \Carbon\Carbon::parse($aditem->created_at)->format('d M')}}  </span><br>
                                        </span>
                                            <a href="Favorites/add/{{$aditem->id}}" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                                Add to Favorites
                                            </a>
                                        </div>
                                    </div>

                                </div>

                            @endforeach
                        @endif

                            @else

                                <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px; text-align: center"> No ads</h4>
                                </div>
                            @endif
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Recently viewed ads</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">

                        <div class="box-body no-padding">
                            <div class="adds-wrapper noSideBar">

                                @if(sizeof($recentads)>0)
                                @foreach($recentads as $recentad)

                                     <div class="item-list make-grid">
                             <div class="col-sm-2 no-padding photobox">
                                 <div class="add-image">
                                     <a href="{{url('viewproduct/'.$recentad->ad->id)}}">
                                         <div class="corner"><span>Rs. {{$aditem->rent}}</span></div>
                                         <img class="thumbnail no-margin center-cropped" src="{{$recentad->ad->item->pictures[0]->path}}" alt="img">
                                     </a>
                                 </div>
                             </div>

                             <div class="col-sm-7 add-desc-box">
                                 <div class="add-details">
                                     <h5 class="add-title">
                                         <a style="white-space: nowrap;" href="#">{{$recentad->ad->title}} </a>
                                     </h5>
                                     <span class="info-link">
                                         {{$recentad->ad->item->category->category}}
                                     </span>
                                     <span class="info-row">
                                         <span class="date"> {{ \Carbon\Carbon::parse($recentad->created_at)->format('d M')}} </span><br>
                                     </span>
                                     <a href="Favorites/add/{{$recentad->id}}" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                         Add to Favorites
                                     </a>
                                 </div>
                             </div>

                         </div>
                                @endforeach
                                @else
                                    <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                        <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                        <h4 style="margin:15px; text-align: center">No ads</h4>
                                    </div>
                                @endif

                            </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.box-body -->
        </div>

    </div>

@stop
@section('adminlte_js')
    <script src="{{asset('vendor/adminlte/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('vendor/highcharts/highcharts.js')}}"></script>
    <script src="{{asset('vendor/highcharts/exporting.js')}}"></script>
    <script src="{{asset('vendor/highcharts/export-data.js')}}"></script>

    <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>
    {!! $chart->script() !!}
@stop

