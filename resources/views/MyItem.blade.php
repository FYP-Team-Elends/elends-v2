@extends('layouts.app')
@section('content')


<div class="wrapper">
    <div class="clr" style="padding-top: 0px;">
        <div class="agile-trend-ads">
            <ul id="flexiselDemo3">
                <li>
                        <div class="col-md-4 biseller-column">
                            <a href="#">
                                <img src="{{ asset($item->pictures[0]->path)}}" alt="123456">
                            </a>
                            <div class="w3-ad-info">
                                <h5>{{$item->name}}</h5>
                            </div>
                            <p>{{$item->description}}</p><br>
                            <small style="color: #a5a5a5">Added on{{$item->created_at}} by {{$item->user->first_name.' '.$item->user->last_name}}</small>
                            <a class="ad" style="margin-top: 2px" href="{{url('SubmitAd')}}" title="Submit AD"> Submit Ad</a>
                        </div>
                </li>
            </ul>



        </div>
        </li>

        </ul>
    </div>
</div>
</div>


@endsection
