@extends('adminlte::page')
@section('title', 'All Ads')

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h4>All Ads</h4>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <div id="message"></div>

                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Posted By</th>
                        <th>Rent Per Day</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Booking</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($ads as $ad)
                    <tr>
                        <td>{{$ad->title}}</td>
                        <td>{{$ad->user->first_name}}</td>
                        <td>{{$ad->rent}}</td>
                        <td><input type="submit" value="Edit" class="btn-sm btn-warning" style="width: 80px"></td>
                        <td><input type="submit" value="Delete" class="btn-sm btn-danger" style="width: 80px"></td>
                        <td><input type="submit" value="Booking" class="btn-sm btn-success" style="width: 80px"></td>
                    </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@endsection