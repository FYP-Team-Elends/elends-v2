@extends('adminlte::page')
@section('title', 'All Services')

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h4>All Services</h4>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <div id="message"></div>

                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th>Service Title</th>
                        <th>Posted By</th>
                        <th>Contact</th>
                        <th>Location</th>
                        <th>Edit</th>
                        <th>Delete</th>
                        <th>Booking</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                    <tr>
                        <td>{{$service->title}}</td>
                        <td>{{$service->user->first_name}}</td>
                        <td>{{$service->contact}}</td>
                        <td><a href="{{'https://www.google.com/maps/search/?api=1&query='.$service->lat.','.$service->lng}}" target="_blank">{{$service->location_name}}</a></td>
                        <td><input type="submit" value="Edit" class=" btn-sm btn-warning"></td>
                        <td><input type="submit" value="Delete" class="btn-sm btn-danger"></td>
                        <td><input type="submit" value="Booking" class="btn-sm btn-success"></td>
                    </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@endsection