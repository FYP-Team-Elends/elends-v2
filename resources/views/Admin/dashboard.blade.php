@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
                <span class="info-box-icon bg-lightgray">
                    <i class="ion ion-ios-analytics-outline ion ion-filing"></i>
                </span>
            <div class="info-box-content">
                <span class="info-box-text">Total Users</span>
                <span class="info-box-number">{{$itemcount}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->
   <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-lightgray">
                <i class="ion ion-ios-analytics-outline ion ion-filing"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Total Ads</span>
                <span class="info-box-number">{{$postedadcount}}</span>
            </div>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-lightgray"><i class="ion ion-ios-analytics-outline ion ion-filing"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Posted Services</span>
                    <span class="info-box-number">{{$servicescount}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
   <div class="col-md-3 col-sm-6 col-xs-12">
         <div class="info-box">
             <span class="info-box-icon bg-lightgray"><i class="ion ion-ios-analytics-outline ion ion-filing"></i></span>

             <div class="info-box-content">
                 <span class="info-box-text">Total Booking</span>
                 <span class="info-box-number">{{$reqbookingadscount}}</span>
             </div>
             <!-- /.info-box-content -->
         </div>
         <!-- /.info-box -->
     </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Users On The Base Of City</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="container">
                            <div class="col-md-12" style="width: 100%; height: 80%">
                                {!! $chart->container() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@stop

@section('adminlte_js')
    <script src="{{asset('vendor/adminlte/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('vendor/highcharts/highcharts.js')}}"></script>
    <script src="{{asset('vendor/highcharts/exporting.js')}}"></script>
    <script src="{{asset('vendor/highcharts/export-data.js')}}"></script>

    <script src=https://cdnjs.cloudflare.com/ajax/libs/echarts/4.0.2/echarts-en.min.js charset=utf-8></script>
    {!! $chart->script() !!}

@stop