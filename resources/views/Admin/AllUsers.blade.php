@extends('adminlte::page')
@section('title', 'Manage User Roles')

@section('content')

    <h2>Manage Users</h2>

    <div class="table-responsive">
        <div id="message"></div>
        <table class="table table-striped table-sm">
            <thead>
            <tr>

                <th>Name</th>
                <th>Email</th>
                @foreach($roles as $role)
                     <th>{{$role->name}}</th>
                @endforeach
                <th>Action</th>
            </tr>
            </thead>
            <script>
                function ManageUser(user_id) {
                    form_id = 'form-' + user_id;
                    var form = document.getElementById(form_id);
                    var formData = $(form).serialize();
                    $.ajax({
                        url: " {{ url('Admin/manageRoles') }}",
                        method: 'post',
                        data: formData,
                        success: function (data) {
                            if (data['success']) {
                                $("#message").text(data['message']);
                            }

                        }
                    });
                }
            </script>

            <tbody>
                <div id="user-role">
                  @foreach($users as $user)
                        <form id="{{'form-'.$user->id}}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <tr>
                            <td>{{$user->first_name.' '.$user->last_name}}</td>

                            <td>{{$user->email}}</td>

                            @foreach($roles as $role)

                                <td>
                                    <input type="checkbox" name="{{$role->name}}" {{$user->hasRole($role->name)? 'checked':''}}>
                                </td>
                            @endforeach

                                <td><i onclick="ManageUser({{$user->id}});" class="btn btn-success" id="{{$user->id}}">Update</i></td>
                            </tr>
                        </form>
                  @endforeach
                </div>
            </tbody>
        </table>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@endsection