@extends('adminlte::page')
@section('title', 'Dashboard')
@section('content')
    <style>

        input[type=date]::-webkit-inner-spin-button,
        input[type=date]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
    </style>

    <div class="box-header with-border" style="background-color: white">
        <h4>Custom Report</h4>
        <form method="get" action="{{url('/Admin/report')}}">
            <input type="date" id="fromDate" name="fromDate" value="<?php if(request()->fromDate){echo request()->fromDate;}else{echo Carbon\Carbon::now()->startOfMonth()->toDateString();}?>" class="form-control" style="width: auto; float: left" required>
            <input type="date" id="toDate" name="toDate" value="<?php if(request()->toDate){echo request()->toDate;}else{echo Carbon\Carbon::now()->toDateString();}?>" class="form-control" style="width: auto; margin-left: 30px; float: left" required >
            <input type="submit" style="margin: 7px">
        </form>
    </div>


    <div class="box-body" style="background-color: white; margin-top: 10px">
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <div class="col-md-12" style="width: 100%; height: 80%; margin-top: 10px">
                        {!! $report_chart_for_bookings->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-body" style="background-color: white; margin-top: 10px">
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <div class="col-md-12" style="width: 100%; height: 80%; margin-top: 10px">
                        {!! $report_chart_for_total_users->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-body" style="background-color: white; margin-top: 10px">
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <div class="col-md-12" style="width: 100%; height: 80%; margin-top: 10px">
                        {!! $report_chart_for_service_booking->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-body" style="background-color: white; margin-top: 10px">
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <div class="col-md-12" style="width: 100%; height: 80%; margin-top: 10px">
                        {!! $report_chart_for_posted_ad->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="box-body" style="background-color: white; margin-top: 10px">
        <div class="row">
            <div class="col-md-12">
                <div id="container">
                    <div class="col-md-12" style="width: 100%; height: 80%; margin-top: 10px">
                        {!! $report_chart_for_posted_service->container() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('adminlte_js')
    <script src="{{asset('vendor/adminlte/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('vendor/highcharts/highcharts.js')}}"></script>
    <script src="{{asset('vendor/highcharts/exporting.js')}}"></script>
    <script src="{{asset('vendor/highcharts/export-data.js')}}"></script>
    {!! $report_chart_for_bookings->script() !!}
    {!! $report_chart_for_total_users->script() !!}
    {!! $report_chart_for_service_booking->script() !!}
    {!! $report_chart_for_posted_ad->script() !!}
    {!! $report_chart_for_posted_service->script() !!}

@stop