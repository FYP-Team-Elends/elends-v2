@extends('adminlte::page')
@section('title', 'Unapproved Services')

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h4>Unapproved services</h4>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <div id="message"></div>

                <table class="table table-striped table-sm">
                    <thead>
                    @if(!count($services)==0)
                        <tr>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Service Provider</th>
                        <th>Contact Number</th>
                        <th>Location</th>
                        <th>Price Per Person</th>
                        <th>Approve</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($services as $service)
                        <tr>
                            <td>{{$service->title}}</td>
                            <td>{{$service->service_category->name}}</td>
                            <td><a data-toggle="modal" data-target="#myModal">{{$service->user->first_name.' '.$service->user->last_name}}</a></td>
                            <td>{{$service->contact}}</td>
                            <td><a href="{{'https://www.google.com/maps/search/?api=1&query='.$service->lat.','.$service->lng}}" target="_blank">{{$service->location_name}}</a></td>
                            <td>{{$service->price_per_person}}</td>
                            <td>
                                <form method="post" action="{{url('/Admin/approveService')}}">
                                    @csrf
                                    <input type="hidden" name="service_id" value="{{$service->id}}">
                                    <input type="submit" value="Approve" class="btn btn-success">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    @else
                        <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                            <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                            <h4 style="margin:15px; text-align: center">No unapproved services</h4>
                        </div>
                    @endif
                </table>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@endsection