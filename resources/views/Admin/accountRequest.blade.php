@extends('adminlte::page')
@section('title', 'Dashboard')

@section('content')
                <!-- /.box-header -->
                        <div class="container box box-success">
                            <div class="col-md-12" id="message"></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="box-header with-border">
                                        <h3 class="box-title">Account Request</h3>
                                    </div>
                                    <div class="table-responsive">

                                        <table id="mytable" class="table table-bordred table-striped">

                                            <thead>
                                            <th>First Name</th>
                                            <th>Last Name</th>
                                            <th>City</th>
                                            <th>Contact</th>
                                            <th>Email</th>
                                            <th>Request For</th>
                                            <th>Accept</th>
                                            <th>Reject</th>
                                            </thead>
                                            <tbody>
                                            <script>
                                                function AcceptRequest(request_id) {
                                                    form_id = 'form-' + request_id;
                                                    tr = '#tr-'+request_id;
                                                    var form = document.getElementById(form_id);
                                                    var formData = $(form).serialize();
                                                    $.ajax({
                                                        url: " {{ url('/Admin/AcceptRequest') }}",
                                                        method: 'post',
                                                        data: formData,
                                                        success: function (data) {
                                                            if (data['success']) {
                                                                $("#message").text(data['message']);
                                                                $(tr).remove();
                                                            }
                                                            else {
                                                                $("#message").text(data['message']);
                                                            }
                                                        }
                                                    });
                                                }
                                                function RejectRequest(request_id) {
                                                    form_id = 'form-' + request_id;
                                                    tr = '#tr-'+request_id;
                                                    var form = document.getElementById(form_id);
                                                    var formData = $(form).serialize();
                                                    $.ajax({
                                                        url: " {{url('/Admin/RejectRequest')}}",
                                                        method: 'post',
                                                        data: formData,
                                                        success: function (data) {
                                                            if (data['success']) {
                                                                $("#message").text(data['message']);
                                                                $(tr).remove();
                                                            }

                                                        }
                                                    });
                                                }
                                            </script>

                                                @foreach($regusers as $reguser)
                                                <tr id="{{'tr-'.$reguser->id}}">
                                                <form id="{{'form-'.$reguser->id}}">
                                                    @csrf
                                                    <input type="hidden" name="request_id" value="{{$reguser->id}}">
                                                    <input type="hidden" name="request_for" value="{{$reguser->register_as}}">
                                                    <td>{{$reguser->user->first_name}}</td>
                                                    <td>{{$reguser->user->last_name}}</td>
                                                    <td>{{$reguser->user->city}}</td>
                                                    <td>{{$reguser->user->phone}}</td>
                                                    <td>{{$reguser->user->email}}</td>
                                                    <td>{{$reguser->register_as}}</td>
                                                    <td><a onclick="AcceptRequest({{$reguser->id}});" class="btn btn-success btn-xs" ><span class="glyphicon glyphicon-ok"></span></a></td>
                                                    <td><a onclick="RejectRequest({{$reguser->id}});" class="btn btn-danger btn-xs" ><span class="glyphicon glyphicon-remove"></span></a></td>
                                                </form>
                                                </tr>
                                                @endforeach



                                            </tbody>

                                        </table>

                                        <div class="col-md-12 " style="text-align:center">{{ $regusers->links()}}</div></div>

                                    </div>

                                </div>
                            </div>
                        </div>


@stop
@section('adminlte_js')
    <script src="{{asset('vendor/adminlte/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop