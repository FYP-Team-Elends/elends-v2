@extends('adminlte::page')
@section('title', 'All Items')

@section('content')

    <div class="box box-success">
        <div class="box-header with-border">
            <h4>All Items</h4>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <div id="message"></div>

                <table class="table table-striped table-sm">
                    <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Posted By</th>
                            <th>Category</th>
                            <th>Ads</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($items as $item)
                    <tr>
                        <td><img src="{{asset($item->pictures[0]->path)}}" width="50px" height="50px" alt="image"></td>
                        <td>{{$item->user->first_name}}</td>
                        <td>{{$item->category->category}}</td>
                        <td><input type="submit" value="View Ads" class="btn-sm btn-success" style="width: 80px"></td>
                        <td><input type="submit" value="Edit" class="btn-sm btn-warning" style="width: 80px"></td>
                        <td><input type="submit" value="Delete" class="btn-sm btn-danger" style="width: 80px"></td>
                    </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@endsection