<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Submit Ad</title>
    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/bootstrap-select.css"><!-- bootstrap-select-CSS -->
    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/flexslider.css" type="text/css" media="screen" /><!-- flexslider-CSS -->
    <link rel="stylesheet" href="csss/font-awesome.min.css" /><!-- fontawesome-CSS -->
    <link rel="stylesheet" href="csss/menu_sideslide.css" type="text/css" media="all"><!-- Navigation-CSS -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->

    <!--fonts-->
    <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--//fonts-->

    <!-- js -->
    <script type="text/javascript" src="jss/jquery.min.js"></script>
    <!-- js -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="jss/bootstrap.js"></script>
    <script src="jss/bootstrap-select.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="css/slick.css" />
    <link type="text/css" rel="stylesheet" href="css/slick-theme.css" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />
    <style>
        input.title {
            text: 'this isss';
        }
    </style>

</head>
<body>
<!-- HEADER -->
<div id="header">
    <div class="container">
        <div class="pull-left">
            <!-- Logo -->
            <div class="header-logo" style="float: left;">
                <a class="logo" href="#">
                    <img src="./img/logo.png" alt="">
                </a>
            </div>
            <!-- /Logo -->

            <!-- Search -->
            <div class="header-search">
                <form>
                    <input class="input search-input" type="text" placeholder="Enter your keyword">
                    <select class="input search-categories">
                        <option value="0">Select Event</option>
                        <option value="1">Mehandi</option>
                        <option value="1">Barat</option>
                        <option value="1">Nikha</option>
                        <option value="1">Valima</option>
                        <option value="1">Party</option>
                        <option value="1">Birthday</option>
                        <option value="1">Bridal Shower</option>
                    </select>
                    <button class="search-btn"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- /Search -->
        </div>
        <div class="pull-right">
            <ul class="header-btns">
                <!-- Account -->
                <li class="header-account dropdown default-dropdown">
                    <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">
                            <i class="fa fa-user-o"></i>
                        </div>
                        <strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
                    </div>
                    <a href="#" class="text-uppercase">Login</a> / <a href="#" class="text-uppercase">Join</a>
                    <ul class="custom-menu">
                        <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
                        <li><a href="#"><i class="fa fa-heart-o"></i> My Wishlist</a></li>
                        <li><a href="#"><i class="fa fa-exchange"></i> My Ads</a></li>
                        <li><a href="#"><i class="fa fa-check"></i> Messages</a></li>
                        <li><a href="#"><i class="fa fa-unlock-alt"></i> Settings</a></li>
                        <li><a href="#"><i class="icon mini abs power"></i> Log out</a></li>
                    </ul>
                </li>
                <!-- /Account -->
            </ul>
        </div>
    </div>
</div>

<!-- Submit Ad -->
<div class="submit-ad main-grid-border">
    <div class="container">
        <h2 class="w3-head">Submit an Ad</h2>
        <div class="post-ad-form">

            <form action="{{url('/submit_Ad')}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <label>Ad Title <span>*</span></label>
                <input type="text" class="phone custom-tooltip" placeholder="" style="width: 40%;">

                <script>
                    /* $(document).ready(function(){
                     $('#tool').click(function(){
                         $('[data-toggle=tooltip]').tooltip('toggle')});

                 });*/
                    $(document).ready(function(){
                        $('.custom-tooltip').tooltip({
                            title:
                            "<ul style='list-style-type: none ; text-align: left; color: #0C0C0C; padding-left: 2px;'>\n" +
                            "  <li><b>Want to get your ad noticed?</b></li><br>\n" +
                            "  <li>• Give your ad an attractive title.</li><br>\n" +
                            "  <li>• Check the spellings and don't use the CAPITAL LETTERS or symbols.</li><br>\n" +
                            "</ul>  ", html: true, placement: "right"
                            ,trigger: "click"});

                    });

                </script>
                <div class="clearfix"></div>
                {{--<label><b>Select Category </b><span>*</span></label>
                <select class="" name="category">
                    <option value=null>Select Category</option>
                    @foreach($category as $item)
                        <option value="{{$item->id}}">{{$item->category}}</option>
                    @endforeach
                </select>
                <div class="clearfix"></div>--}}
                <label>Ad Description <span>*</span></label>
                <textarea class="mess custom-tooltip1" placeholder="Write few lines about your product including the brand, model."></textarea>
                <script>
                    /* $(document).ready(function(){
                     $('#tool').click(function(){
                         $('[data-toggle=tooltip]').tooltip('toggle')});

                 });*/
                    $(document).ready(function(){
                        $('.custom-tooltip1').tooltip({
                            title: "<ul style='list-style-type: none ; text-align: left; color: #0C0C0C; padding-left: 2px;'> <li><b>Items with good description sublet faster!</b></li><br> <li>• Mention the condition, features and brand.</li><br> <li>• Remember, good description needs at least 2-3 sentences.</li><br> </ul> ", html: true, placement: "right" ,trigger: "click"});

                    });
                    </script>
                <div class="clearfix"></div>
               {{-- <div class="upload-ad-photos">
                    <label>Upload Photos:</label>
                    <div class="photos-upload-view custom-tooltip2">
                        <form id="upload" action="index.blade.php" method="POST" enctype="multipart/form-data">

                            <input type="hidden" id="MAX_FILE_SIZE" name="MAX_FILE_SIZE" value="300000" />

                            <div>
                                <input type="file" id="fileselect" name="fileselect[]" multiple="multiple" />
                                <div id="filedrag">or drop files here</div>
                            </div>

                            <div id="submitbutton">
                                <button type="submit">Upload Photos</button>
                            </div>

                        </form>

                        <script>
                            /* $(document).ready(function(){
                             $('#tool').click(function(){
                                 $('[data-toggle=tooltip]').tooltip('toggle')});

                         });*/
                            $(document).ready(function(){
                                $('.custom-tooltip2').tooltip({
                                    title: "<ul style='list-style-type: none ; text-align: left;padding-left: 2px;'> <li><b style='color: red'>Ads with 4-6 photos get more responses!</b></li><br> <li style='color: #0C0C0C'>• Ensure the item is properly visible from multiple angles.</li><br> <li style='color: #0C0C0C'>• Accepted formats are .jpg, .gif & .png. and Size < 5MB.</li><br> </ul> ", html: true, placement: "right"});

                            });
                        </script>

                    </div>
                    <div class="clearfix"></div>
                    <script src="jss/filedrag.js"></script>
                </div>--}}
                <div class="personal-details">

                   {{-- <label>Your Name <span>*</span></label>
                        <input type="text" class="name custom-tooltip3" placeholder="">
                        <script>
                            /* $(document).ready(function(){
                             $('#tool').click(function(){
                                 $('[data-toggle=tooltip]').tooltip('toggle')});

                         });*/
                            $(document).ready(function(){
                                $('.custom-tooltip3').tooltip({
                                    title: "<ul style='list-style-type: none ; text-align: left;padding-left: 2px; color: #0C0C0C'>  <li>• Use your real name to build trust.</li><br> <li>• Don't use symbols or numbers in your name.</li></ul> ", html: true, placement: "right", trigger: "click"});

                            });
                        </script>
                        <div class="clearfix"></div>
                        <label><span style="color: black">Your Mobile No</span> <span>*</span></label>
                        <input type="text" class="phone custom-tooltip4" placeholder="+92">

                        <script>
                            /* $(document).ready(function(){
                             $('#tool').click(function(){
                                 $('[data-toggle=tooltip]').tooltip('toggle')});

                         });*/
                            $(document).ready(function(){
                                $('.custom-tooltip4').tooltip({title: "<ul style='list-style-type: none ; text-align: left;padding-left: 2px; color: #0C0C0C'>  <li>• Enter your MOBILE number.</li><br> <li>• Don't add +92 or 0 before the number.</li><br> <li>• A verification code will be sent to this MOBILE number.</li><br>  <li>• Verification is mandatory to post the Ad.</li><br></ul> ", html: true, placement: "right", trigger: "click"});

                            });
                        </script>
                        <div class="clearfix"></div>
                        <label>Your Email Address<span>*</span></label>
                        <input type="text" class="email" placeholder="">
                        <div class="clearfix"></div>--}}
                        <p class="post-terms">By clicking <strong>post Button</strong> you accept our <a href="terms.html" target="_blank">Terms of Use </a> and <a href="privacy.html" target="_blank">Privacy Policy</a></p>
                        <input type="submit" value="Post">
                        <div class="clearfix"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- // Submit Ad -->


<!-- FOOTER -->
<footer id="footer" class="section section-grey">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="app">
                    <h3>DOWNLOAD APP</h3>
                    <img src="./img/app.jpg" style="width: 180px">
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Information</h3>
                    <ul class="list-links">
                        <li><a href="#">Location Map</a></li>
                        <li><a href="#">Popular Seraches</a></li>
                        <li><a href="#">term of use</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <div class="clearfix visible-sm visible-xs"></div>

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">HELP</h3>
                    <ul class="list-links">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Sitemap</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer subscribe -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Contact Us</h3>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-earphone"></span></li>
                        <li>+0 561 111 235</li>
                    </ul>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-envelope"></span></li>
                        <li><a href="mailto:info@example.com">mail@example.com</a></li>
                    </ul>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-briefcase"></span></li>
                        <li><strong>Business Packages</strong></li>
                        <li><small>(featured ads, advertising)</small></li><br>
                        <li><a href="#">click here</a></li>
                    </ul>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- /footer subscribe -->
        </div>
        <!-- /row -->
        <!-- /row -->
    </div>
    <!-- /container -->
</footer>
<!--footer section start-->
<footer>
    <div class="agileits-footer-bottom text-center">
        <div class="container">
            <div class="w3-footer-logo">
                <h1><a href="index.blade.php"><span>E-</span>Lend</a></h1>
            </div>
            <div class="w3-footer-social-icons">
                <ul>
                    <li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a></li>
                    <li><a class="flickr" href="#"><i class="fa fa-flickr" aria-hidden="true"></i><span>Flickr</span></a></li>
                    <li><a class="googleplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i><span>Google+</span></a></li>
                    <li><a class="dribbble" href="#"><i class="fa fa-dribbble" aria-hidden="true"></i><span>Dribbble</span></a></li>
                </ul>
            </div>
            <div class="col-md-8 col-md-offset-2 text-center">
                <!-- footer copyright -->
                <div class="footer-copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | We <i class="fa fa-heart-o" style="color: red" aria-hidden="true"></i> to make you happy</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </div>
                <!-- /footer copyright -->
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</footer>
<!--footer section end-->





<!-- jQuery Plugins -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<!-- Navigation-Js-->
<script type="text/javascript" src="jss/main.js"></script>
<script type="text/javascript" src="jss/classie.js"></script>
<!-- //Navigation-Js-->
<!-- js -->
<script type="text/javascript" src="jss/jquery.min.js"></script>
<!-- js -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="jss/bootstrap.js"></script>
<script src="jss/bootstrap-select.js"></script>
<script>
    $(document).ready(function () {
        var mySelect = $('#first-disabled2');

        $('#special').on('click', function () {
            mySelect.find('option:selected').prop('disabled', true);
            mySelect.selectpicker('refresh');
        });

        $('#special2').on('click', function () {
            mySelect.find('option:disabled').prop('disabled', false);
            mySelect.selectpicker('refresh');
        });

        $('#basic2').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });
    });
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
</script>
<!-- Slider-JavaScript -->
<script src="jss/responsiveslides.min.js"></script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            pager: false,
            nav: true,
            speed: 500,
            maxwidth: 800,
            namespace: "large-btns"
        });

    });
</script>
<!-- //Slider-JavaScript -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="jss/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- //here ends scrolling icon -->
</body>
<!-- Navigation-JavaScript -->
<script src="jss/classie.js"></script>
<script src="jss/main.js"></script>
<!-- //Navigation-JavaScript -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="jss/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
</html>