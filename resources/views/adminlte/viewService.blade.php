@extends('adminlte::page')
@section('title', 'Product')

@section('content')

    <style>

        input[type=date]::-webkit-inner-spin-button,
        input[type=date]::-webkit-outer-spin-button {
            -webkit-appearance: none;
        }


        #map {
            height: 300px;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }

    </style>
    <div class="main-container" style="margin: 10px">
        <div class="row">
            <div class="col-md-12" style="margin-left: -15px; margin-right: -15px; margin-bottom: 15px">
                <img  style="width: 100%; height: 200px; object-fit: cover;" src="{{$service->picture}}">
            </div>
        </div>
        <div class="row">
                <div class="col-md-6" style="border: 1px solid #DADADA;background-color: white;float: left">
                <div class="vendor-pro-section">
                    <h2 class="vendor-pro-heading" style="margin-top:7px ">{{$service->title}}</h2>
                </div>

                    <div class="col-md-12">
                        <div class="vendor-pro-dtl" id="about-content" style="height: auto; float:left;overflow: hidden;">
                            <p></p><p>{{$service->about}}</p>
                            <p></p> <br>
                        </div>
                        <br>
                    </div>
                <div class="col-md-12 col-xs-12" style="margin-top: 20px; margin-bottom: 20px; border:1px solid #DADADA; padding:7px 5px;background-color: white;">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8" style="padding-right: 0;">
                            <p style="font-size: 20px;">Contact Info</p>
                            <p>
                                <a href="#" class="fa fa-phone" data="77,Shadibox_Contact" style="transform: rotate(100deg)"> <a>&nbsp&nbsp{{$service->contact}}</a> </a> <br>
                                <address href="#" class="fa fa-map-marker" >&nbsp&nbsp{{$service->location_name}}</address>
                            </p>
                        </div>
                    </div>
                    <?php
                    echo "<script>
                                // Initialize and add the map
                                function initMap() {
                                    // The location of Uluru
                                    var uluru = {lat:".$service->lat." , lng:". $service->lng."};
                                    // The map, centered at Uluru
                                    var map = new google.maps.Map(
                                        document.getElementById('map'), {zoom: 15, center: uluru});
                                    // The marker, positioned at Uluru
                                    var marker = new google.maps.Marker({position: uluru, map: map});
                                }
                            </script>";
                    ?>
                    <div id="map"></div>

                </div>


            </div>
                <div class="row">

                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">



                                <div class="box-body no-padding" style="background-color: white; border: 1px solid #DADADA">
                                    <h2 class="vendor-enquery-heading" style="margin: 7px">Book {{$service->title}}</h2>
                                    <form action="{{url('/servicebooking')}}" method="POST"  enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{$service->id}}">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="Event_Type">Event Type</label>
                                                <input list="Event" type="text" class="form-control" name="event" id="Event_Type" placeholder="Select Event *" required>
                                                <datalist id="Event">
                                                    <option value="Mehandi">
                                                    <option value="Nikkah">
                                                    <option value="Mangni">
                                                    <option value="Barat">
                                                    <option value="Waleema">
                                                    <option value="Anniversary">
                                                </datalist>
                                            </div>
                                            <div class="form-group">
                                                <label for="Date">Select Date</label>
                                                <input type="date" id="Function_Date"  name="Function_Date" class="form-control fa fa-calendar" data-date-start-date="+1d" placeholder="Function Date *" required onemptied="this.setCustomValidity('Select Function Date')">
                                            </div>
                                            <div class="form-group">
                                                <label for="No._of_guests">No. Of Guests</label>
                                                <input type="number" class="form-control" id="no_of_guests" min="0" name="guest" placeholder="No. of guests *" required onemptied="this.setCustomValidity('Enter Total Guests')">
                                            </div>

                                            <div class="form-group">
                                                <label for="Function_time">Function Time</label>
                                                <input list="Time" type="text" class="form-control" name="time" id="Event_Type" placeholder="Select Time *" required onemptied="this.setCustomValidity('Enter Function type')">
                                                <datalist id="Time">
                                                    <option value="Lunch Time">
                                                    <option value="Dinner Time">
                                                </datalist>
                                            </div>
                                            <div class="form-group">
                                                <label for="message">Message</label>
                                                <textarea class="form-control" id="Message" rows="4" placeholder="Message" name="Message" onemptied="this.setCustomValidity('Enter Message')"></textarea>
                                            </div>

                                        <!-- /.box-body -->
                                            <button type="submit" class="btn btn-primary">Submit</button>

                                        </div>
                                    </form>
                                </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn7IlopznAGZ4LYnBmtwAHOSdLSkk5TU&callback=initMap">
    </script>
@stop
