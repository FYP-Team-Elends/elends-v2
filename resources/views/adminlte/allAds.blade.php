@extends('adminlte::page')
@section('title', 'All Ads')

@section('content')
    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }

        .corner {
            width: 0;
            height: 0;
            border-top: 80px solid #ffcc00;
            border-bottom: 80px solid transparent;
            border-left: 80px solid transparent;
            position:absolute;
            right:0;
        }

        .corner span {
            position:absolute;
            top: -65px;
            width: 70px;
            left: -65px;
            text-align: center;
            font-size: 16px;
            font-family: arial;
            transform: rotate(45deg);
            display:block;
        }
    </style>

    @if(session('msg'))
        <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
            <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">{{session('msg')}}</span>
        </div>
    @endisset

    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">All Ads</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="adds-wrapper noSideBar">

                        @if(sizeof($myads)>0)
                        @foreach($myads as $ad)
                            <div class="item-list make-grid">


                                <div class="col-sm-2 no-padding photobox">

                                    <div class="add-image">
                                        <a href="{{url('viewproduct/'.$ad->id)}}">
                                            <div class="corner"><span>Rs. {{$ad->rent}}</span></div>
                                            <img class="thumbnail no-margin center-cropped" src="{{asset($ad->item->pictures[0]->path)}}" alt="img">
                                        </a>
                                    </div>

                                </div>

                                <div class="col-sm-7 add-desc-box">
                                    <div class="add-details">
                                        <h5 class="add-title">
                                            <a href="#">{{$ad->title}}</a>
                                        </h5>
                                        <span class="info-link">
                                            Category : {{$ad->item->category->category}}
                                        </span>
                                        <span class="info-row">
                                            <span class="date">Posted on {{ \Carbon\Carbon::parse($ad->created_at)->format('d M, Y')}} </span><br>
                                        </span>
                                        <span class="btn btn-danger btn-block" value="DELETE AD" data-toggle="modal" data-target="#modal-default" ad-id="{{$ad->id}}">DELETE AD</span>
                                    </div>
                                </div>
                            </div>

                        @endforeach
                        @else
                            <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px; text-align: center">No Ad Yet</h4>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-default" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Delete Ad</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure you want to delete this ad?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>

                    <form method="GET" action="" id="deleteForm">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}

                        <div class="form-group">
                            <input type="submit" class="btn btn-danger" value="Yes!Delete it">
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>

    <script>
        $('#modal-default').on('show.bs.modal', function (e) {
            var opener=e.relatedTarget;
            var id=$(opener).attr('ad-id');
            $('#deleteForm').action = "/deleteAd/"+id;
        });
    </script>
@stop
