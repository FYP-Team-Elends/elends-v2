@extends('adminlte::page')
@section('title', 'Messages')

@section('content')
    <style>
        .no-style-a {
            font-size: unset !important;
            font-family: unset !important;
            font-weight: unset !important;
            font-style: unset !important;
            text-transform: unset !important;
            margin-top: unset !important;
            margin-bottom: unset !important;
            font-family: unset !important;
            line-height: unset !important;
            color: unset !important;
            margin: unset !important;
            display: unset !important;
            -webkit-margin-before: unset !important;
            -webkit-margin-after: unset !important;
            -webkit-margin-start: unset !important;
            -webkit-margin-end: unset !important;
        }
        .container{max-width:100%; margin:auto;}
        img{ max-width:100%;}
        .inbox_people {
            background: #f8f8f8 none repeat scroll 0 0;
            float: left;
            overflow: hidden;
            width: 30%; border-right:1px solid #c4c4c4;
        }
        .inbox_msg {
            border: 1px solid #c4c4c4;
            clear: both;
            overflow: hidden;
            max-height: 100%;
        }
        .top_spac{ margin: 20px 0 0;}


        .recent_heading {float: left; width:40%;}
        .srch_bar {
            display: inline-block;
            text-align: right;
            width: 60%;
        }
        .headind_srch{ padding:10px 29px 10px 20px; overflow:hidden; border-bottom:1px solid #c4c4c4;}

        .recent_heading h4 {
            color: #05728f;
            font-size: 21px;
            margin: auto;
        }
        .srch_bar input{ border:1px solid #cdcdcd; border-width:0 0 1px 0; width:80%; padding:2px 0 4px 6px; background:none;}
        .srch_bar .input-group-addon button {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: medium none;
            padding: 0;
            color: #707070;
            font-size: 18px;
        }
        .srch_bar .input-group-addon { margin: 0 0 0 -27px;}

        .chat_ib h5{ font-size:15px; color:#464646; margin:0 0 8px 0;}
        .chat_ib h5 span{ font-size:13px; float:right;}
        .chat_ib p{ font-size:14px; color:#989898; margin:auto}
        .chat_img {
            float: left;
            width: 11%;
        }
        .chat_ib {
            float: left;
            padding: 0 0 0 15px;
            width: 88%;
        }

        .chat_people{ overflow:hidden; clear:both;}
        .chat_list {
            border-bottom: 1px solid #c4c4c4;
            margin: 0;
            padding: 18px 16px 10px;
        }
        .inbox_chat { height: 500px; overflow-y: scroll;}

        .active_chat{ background:#ebebeb;}

        .incoming_msg_img {
            display: inline-block;
            width: 6%;
        }
        .received_msg {
            display: inline-block;
            padding: 0 0 0 10px;
            vertical-align: top;
            width: 92%;
        }
        .received_withd_msg p {
            background: #ebebeb none repeat scroll 0 0;
            border-radius: 3px;
            color: #646464;
            font-size: 14px;
            margin: 0;
            padding: 5px 10px 5px 12px;
            width: 100%;
        }
        .time_date {
            color: #747474;
            display: block;
            font-size: 12px;
            margin: 8px 0 0;
        }
        .received_withd_msg { width: 57%;}
        .mesgs {
            float: left;
            padding: 30px 15px 0 25px;
            width: 70%;
        }

        .sent_msg p {
            background: #05728f none repeat scroll 0 0;
            border-radius: 3px;
            font-size: 14px;
            margin: 0; color:#fff;
            padding: 5px 10px 5px 12px;
            width:100%;
        }
        .outgoing_msg{ overflow:hidden; margin:26px 0 26px;}
        .sent_msg {
            float: right;
            width: 46%;
        }
        .input_msg_write input {
            background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
            border: medium none;
            color: #4c4c4c;
            font-size: 15px;
            min-height: 48px;
            width: 100%;
        }

        .type_msg {border-top: 1px solid #c4c4c4;position: relative;}
        .msg_send_btn {
            background: #05728f none repeat scroll 0 0;
            border: medium none;
            border-radius: 50%;
            color: #fff;
            cursor: pointer;
            font-size: 17px;
            height: 33px;
            position: absolute;
            right: 0;
            top: 11px;
            width: 33px;
        }
        .messaging { padding: 0 0 50px 0;}
        .msg_history {
            height: 470px;
            overflow-y: auto;
        }
    </style>

    <div class="messaging">
        <div class="inbox_msg">
            <div class="inbox_people">
                <div class="headind_srch">
                    <div class="recent_heading">
                        <h4>All Messaages</h4>
                    </div>
                </div>
                <script>
                    function selectDeselect(elem) {
                        var el = document.getElementById('txtt');
                        if(el){
                            el.remove();
                        }
                        var a = document.getElementsByTagName('li')
                        for (i = 0; i < a.length; i++) {
                            a[i].classList.remove('active_chat');
                        }
                        elem.classList.add('active_chat');
                    }

                </script>
                <div class="inbox_chat">
                    <ul id="chat">
                        @foreach($chats as $chat)
                            @php
                                $servicebooking = null;
                                $booking = null;
                                if($chat->service_booking_id)
                                {
                                  $servicebooking = \ELends\ServiceBooking::findorfail($chat->service_booking_id);
                                }else
                                {
                                  $booking = \ELends\Booking::findorfail($chat->booking_id);
                                }

                            @endphp
                            <li class="contact {{$id == $chat->id ? 'active_chat' : ''}}"  id="{{$chat->id}}" style="list-style: none" onclick="selectDeselect(this)">
                                <div class="chat_list" style="all: revert" id="cchat" href="#">
                                    <a href="#" class="no-style-a">
                                        <div class="chat_people">
                                            <div class="chat_img">
                                                @if($booking)
                                                    <img src="{{$chat->user->first_name === Auth::user()->first_name ? $booking->ad->user->picture :$chat->user->picture }}" alt="">
                                                @else
                                                    <img src="{{$chat->user->first_name === Auth::user()->first_name ? $servicebooking->user->picture :$chat->user->picture }}" alt="">
                                                @endif

                                            </div>
                                            <div class="chat_ib">
                                                @if($booking)
                                                    <h5 class="preview">{{$booking->ad->title}}
                                                        <span class="chat_date">{{$chat->updated_at}}</span></h5>
                                                @else
                                                    <h5><strong>{{$servicebooking->service->title}}</strong>
                                                        <span class="chat_date">{{$chat->updated_at}}</span></h5>
                                                @endif

                                                    @if($chat->user->first_name === Auth::user()->first_name)
                                                        @if($servicebooking)
                                                            `<p class="preview"><strong>{{$servicebooking->service->user->first_name.' '.$servicebooking->service->user->last_name}}</strong></p>
                                                        @else
                                                            <p class="preview"><strong>{{$booking->ad->user->first_name.' '.$booking->ad->user->last_name}}</strong></p>
                                                        @endif
                                                    @else
                                                        <p class="preview"><strong>{{$chat->user->first_name.' '.$chat->user->last_name}}</strong></p>
                                                    @endif
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="mesgs">
                <div class="msg_history">
                    <h4 style="text-align: center" id="txtt">Please select a chat</h4>
                    <ul>

                    </ul>
                </div>
                <div class="type_msg">
                    <div class="input_msg_write">
                        <form id="msg_send_form" >
                            @csrf
                            <input type="text" id="message" name="message" class="write_msg" placeholder="Type a message" />
                            <a href="#" class="no-style-a msg_send_btn">
                                <i class="fa fa-plane" style="margin-left: 9px; margin-top: 8px; color: #fff" aria-hidden="true"></i>
                            </a>
                        </form>
                    </div>
                </div>
            </div>
        </div>


    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>

    <script>
        $('input').on('keydown', function(event) {
            var x = event.which;
            if (x === 13) {
                event.preventDefault();
            }
        });
        $(document).ready(function() {
            var id = null;
            var isdown = true;
            var stop = null;
            /*setTimeout(function(){
                getmessages(id);
            }, 1000); */
            @if($id)
                id = {{$id}}
            $('#txtt').html('');
            getmessages(id);
            @endif
            $("#chat li").click(function() {
                id = $(this).attr('id');
                isdown = true;
                if (stop != null)
                {
                    clearInterval(stop);
                }
                stop = setInterval(function(){
                    getmessages(id);
                }, 1000);


            });
            function getmessages(id) {
                $.ajax({
                    url: '/chat/getmessages/'+id,
                    method: 'GET',
                    success: function( data ){
                        $(".msg_history ul").html(data);
                        if (isdown) {
                            $(".msg_history").animate({scrollTop: $('.msg_history').prop("scrollHeight")}, "fast");
                            isdown = false;
                        }
                    },
                });
            }
            $(".msg_send_btn").click(function () {
                var form = document.getElementById('msg_send_form');
                var formData = $(form).serialize();
                $.ajax({
                    method : 'POST',
                    data: formData,
                    url: '/chat/sendmessage/'+id,
                    success: function (data) {
                        $("#message").val('');
                        isdown = true;
                        getmessages(id);

                    }
                });
            });
        });
    </script>


    <script>
        $('#modal-default').on('show.bs.modal', function (e) {
            var opener=e.relatedTarget;
            var id=$(opener).attr('ad-id');
            $('#deleteForm').action = "/deleteAd/"+id;
        });
    </script>
@stop
