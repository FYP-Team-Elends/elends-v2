@extends('adminlte::page')
@section('title', 'Wishlist')
@section('content_header')
    <h1>Wishlist</h1>
@stop
@section('content')
    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
    </style>
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">My Wishlist</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="adds-wrapper noSideBar">

                        @if(sizeof($favads)>0)
                        @foreach($favads as $ad)
                            @if($ad->ad->ad_type == 'item')
                                <div class="item-list make-grid">
                                    <div class="col-sm-2 no-padding photobox">
                                        <div class="add-image">
                                            <a href="{{url('viewproduct/'.$ad->ad->item->id)}}">

                                                <img class="thumbnail no-margin center-cropped" src="{{asset(\ELends\ItemPictures::where('item_id',$ad->ad->item->id)->pluck('path')->first())}}" alt="img">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-sm-7 add-desc-box">
                                        <div class="add-details">
                                            <h5 class="add-title">
                                                <a href="#">{{$ad->ad->title}} </a>
                                            </h5>
                                            <span class="info-link">
                                            Category : {{$ad->ad->item->category->category}}
                                        </span>
                                            <span class="info-row">
                                            <span class="date"> {{$ad->ad->created_at}} </span><br>
                                        </span>
                                            <h4 class="item-price">
                                                {{$ad->ad->rent}} PKR / Day
                                            </h4>
                                            <a href="/AdminLte/favad/remove/{{$ad->id}}" class="btn btn-danger btn-block showphone" style="margin-top: 10px">
                                                Remove from Wishlist
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            @else($ad->ad_type == 'service')
                                <div class="item-list make-grid">
                                    <div class="col-sm-2 no-padding photobox">
                                        <div class="add-image">
                                            <a href="{{url('viewproduct/'.$ad->ad->service->id)}}">
                                                <img class="thumbnail no-margin center-cropped" src="{{asset($ad->ad->service->image)}}" alt="img">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="col-sm-7 add-desc-box">
                                        <div class="add-details">
                                            <h5 class="add-title">
                                                <a href="#">{{$ad->ad->title}} </a>
                                            </h5>
                                            <span class="info-link">
                                           Ad Category : {{\ELends\ServiceCategory::where('id',$ad->ad->service->service_category_id)->pluck('name')->first()}}
                                            </span>
                                            <span class="info-row">
                                            <span class="date"> {{$ad->ad->created_at}} </span><br>
                                        </span>
                                            <a href="/AdminLte/favad/remove/{{$ad->id}}" class="btn btn-danger btn-block showphone" style="margin-top: 10px">
                                                Remove from Wishlist
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            @endif
                        @endforeach
                        @else
                            <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px; text-align: center">No ads</h4>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop
