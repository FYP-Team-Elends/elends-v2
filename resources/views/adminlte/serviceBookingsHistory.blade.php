@extends('adminlte::page')
@section('title', 'Current Bookings')

@section('content')
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Service Bookings History</h3>
                </div>
                <div class="box-body no-padding">

                    @if(sizeof($mybookings)>0)
                        @foreach($mybookings as $mybooking)

                            <div class="col-lg-12 col-sm-12 col-md-12">
                                <div class="col-sm-2 col-md-2 col-lg-2 no-padding photobox" style="width: 160px; height: 100%; text-align: center;  margin-top: 10px">
                                    <img style="width: 160px; height: 160px" class="thumbnail no-margin" src="{{"".$mybooking->service->user->picture}}" alt="img">
                                </div>
                                <div class="col-sm-7 col-md-7 col-lg-7">
                                    <h3 class="add-title" style="margin-top: 10px">
                                        {{$mybooking->service->user->first_name.' '.$mybooking->service->user->last_name}}
                                    </h3>
                                    <a href="{{'/viewservice/'.$mybooking->service_id}}" style="color: #bf5329">{{$mybooking->service->title}}</a><br>
                                    <div>
                                        <i class="fa fa-circle text-success"></i>
                                        <span>COMPLETED</span>
                                    </div>
                                    <h5 style="margin-bottom: 0px !important; padding-bottom: 0px !important;">Message:</h5>
                                    <div class="rounded" style="background: #dadada; padding: 8px; width: 400px; height: auto">
                                        <span>{{$mybooking->message}}</span>
                                    </div>
                                    <div class="col" style="margin-top: 5px; margin-bottom: 5px">
                                        <a href="{{url('ServiceProvider/chat/'.$mybooking->id)}}" class="btn btn-default make-favorite">
                                            <i class="fas fa-envelope"></i><span> Send Message </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-lg-3 col-md-3">
                                    <font size="3" style="float: right">{{ \Carbon\Carbon::parse($mybooking->created_at)->format('d M, Y')}}</font>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div style="margin: 20px; width: 270px ; margin-left: auto; margin-right: auto; display: block">
                            <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                            <h4 style="margin:15px; text-align: center">No Bookings</h4>
                        </div>
                    @endif


                </div>
            </div>

        </div>
    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop
