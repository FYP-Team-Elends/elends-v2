@extends('adminlte::page')
@section('title', 'All Items')

@section('content_header')
    <h1>All Items</h1>
@stop
@section('content')
    <style>
        input[type=date]::-webkit-inner-spin-button,
        input[type=date]::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }


        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
    </style>
    <script src="{{ asset('vendor/bower_components/jquery/dist/jquery.js') }}"></script>
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">All Items</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="adds-wrapper noSideBar">
                        @if(sizeof($items)>0)
                        @foreach($items as $item)

                            <div class="item-list make-grid">
                                <div class="col-sm-2 no-padding photobox">
                                    <div class="add-image">
                                        <a href="{{url('viewitem/'.$item->id)}}">
                                            <img class="thumbnail no-margin center-cropped" src="{{ asset($item->pictures[0]->path)}}" alt="img">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-sm-7 add-desc-box">
                                    <div class="add-details">
                                        <h5 class="add-title">
                                            <a href="#">{{$item->name}} </a>
                                        </h5>
                                        <span class="info-link">
                                            Category : {{$item->category->category}}
                                        </span>
                                        <span class="info-row">
                                            <span class="date"> Added on {{ \Carbon\Carbon::parse($item->created_at)->format('d M, Y')}} </span><br>
                                        </span>
                                        <button class="btn btn-success btn-block" data-toggle="modal" channel-id="{{$item->id}}" data-target="#postModal"
                                      style="margin-top: 10px">
                                            POST AD
                                        </button>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                        @else
                            <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px">You haven't post anything</h4>
                            </div>
                        @endif
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- The Modal -->
    <div class="modal fade" id="postModal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h3 class="modal-title">Submit Ad</h3>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="box-body no-padding">
                        <form role="form" action="{{url('/submitPost')}}" id="postAdForm" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">ID</label>
                                    <input type="text" class="form-control" name="id" placeholder="" readonly required>
                                </div>
                                <div class="form-group">
                                    <label>Ad Title</label>
                                    <input type="text" class="form-control" id="ad_title" name="title" placeholder="" required onemptied="this.setCustomValidity('Enter Title')">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Ad Description</label>
                                    <textarea id="ad_description" name="description" style="height: 130px" class="form-control" required onemptied="this.setCustomValidity('Enter Description')"></textarea>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputFile">Rent Per Day</label>
                                    <input  class="form-control" type="number" min="0" id="date" name="rent" required onemptied="this.setCustomValidity('Enter Rent')">
                                </div>

                                <div class="form-group">
                                    @php
                                        $events = \ELends\Event::all();
                                    @endphp
                                    <label for="exampleInputFile">Event Type</label>
                                        <select class="form-control" name="Events">
                                            <option value="All">Events (All)</option>
                                            @foreach($events as $event)
                                                <option value="{{$event->id}}">{{$event->name}}</option>
                                            @endforeach
                                        </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputFile">Expiry Date</label>
                                    <input  class="form-control" type="date" id="date" name="expirydate" required>
                                </div>
                            </div>

                            <!-- /.box-body -->

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" style="float: right" >Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>


            </div>
        </div>
    </div>

@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
    <script>
        $('#postModal').on('show.bs.modal', function (e) {
            var opener=e.relatedTarget;
            var channelid=$(opener).attr('channel-id');
            $('#postAdForm').find('[name="id"]').val(channelid);
        });

    </script>

@stop
