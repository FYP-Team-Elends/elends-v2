@extends('adminlte::page')
@section('title', 'View Hall')

@section('content_header')
    <h1>All Ads</h1>
@stop

@section('content')

    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
    </style>
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">

                    <h3 class="box-title">Ads</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="adds-wrapper noSideBar">
                            <div class="item-list make-grid">
                                <div class="col-sm-2 no-padding photobox">
                                    <div class="add-image">
                                        <a href="abc">
                                            <img class="thumbnail no-margin center-cropped" src="https://cdn0.weddingwire.co.uk/emp/fotos/4/4/4/3/t20_helen-elliot-photography_4_114443.jpg" alt="img">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-sm-7 add-desc-box">
                                    <div class="add-details">
                                        <h5 class="add-title">
                                            <a href="#">Wedding Hall </a>
                                        </h5>
                                        <span class="info-link">
                                            Lahore
                                        </span>
                                        <span class="info-row">
                                            <span class="date"> Town Ship </span><br>
                                        </span>
                                        <a href="/Favorites/add/id" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                            View Hall
                                        </a>
                                    </div>
                                </div>

                            </div>
                        <div class="item-list make-grid">
                            <div class="col-sm-2 no-padding photobox">
                                <div class="add-image">
                                    <a href="abc">
                                        <img class="thumbnail no-margin center-cropped" src="https://cdn0.weddingwire.co.uk/emp/fotos/4/4/4/3/t20_helen-elliot-photography_4_114443.jpg" alt="img">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-7 add-desc-box">
                                <div class="add-details">
                                    <h5 class="add-title">
                                        <a href="#">Wedding Hall </a>
                                    </h5>
                                    <span class="info-link">
                                            Lahore
                                        </span>
                                    <span class="info-row">
                                            <span class="date"> Town Ship </span><br>
                                        </span>
                                    <a href="/Favorites/add/id" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                        View Hall
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="item-list make-grid">
                            <div class="col-sm-2 no-padding photobox">
                                <div class="add-image">
                                    <a href="abc">
                                        <img class="thumbnail no-margin center-cropped" src="https://cdn0.weddingwire.co.uk/emp/fotos/4/4/4/3/t20_helen-elliot-photography_4_114443.jpg" alt="img">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-7 add-desc-box">
                                <div class="add-details">
                                    <h5 class="add-title">
                                        <a href="#">Wedding Hall </a>
                                    </h5>
                                    <span class="info-link">
                                            Lahore
                                        </span>
                                    <span class="info-row">
                                            <span class="date"> Town Ship </span><br>
                                        </span>
                                    <a href="/Favorites/add/id" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                        View Hall
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="item-list make-grid">
                            <div class="col-sm-2 no-padding photobox">
                                <div class="add-image">
                                    <a href="abc">
                                        <img class="thumbnail no-margin center-cropped" src="https://cdn0.weddingwire.co.uk/emp/fotos/4/4/4/3/t20_helen-elliot-photography_4_114443.jpg" alt="img">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-7 add-desc-box">
                                <div class="add-details">
                                    <h5 class="add-title">
                                        <a href="#">Wedding Hall </a>
                                    </h5>
                                    <span class="info-link">
                                            Lahore
                                        </span>
                                    <span class="info-row">
                                            <span class="date"> Town Ship </span><br>
                                        </span>
                                    <a href="/Favorites/add/id" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                        View Hall
                                    </a>
                                </div>
                            </div>

                        </div>
                        <div class="item-list make-grid">
                            <div class="col-sm-2 no-padding photobox">
                                <div class="add-image">
                                    <a href="abc">
                                        <img class="thumbnail no-margin center-cropped" src="https://cdn0.weddingwire.co.uk/emp/fotos/4/4/4/3/t20_helen-elliot-photography_4_114443.jpg" alt="img">
                                    </a>
                                </div>
                            </div>

                            <div class="col-sm-7 add-desc-box">
                                <div class="add-details">
                                    <h5 class="add-title">
                                        <a href="#">Wedding Hall </a>
                                    </h5>
                                    <span class="info-link">
                                            Lahore
                                        </span>
                                    <span class="info-row">
                                            <span class="date"> Town Ship </span><br>
                                        </span>
                                    <a href="/Favorites/add/id" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                        View Hall
                                    </a>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    @stop



@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>

    <script>
        $('#modal-default').on('show.bs.modal', function (e) {
            var opener=e.relatedTarget;
            var id=$(opener).attr('ad-id');
            $('#deleteForm').action = "/deleteAd/"+id;
        });
    </script>
@stop