@extends('adminlte::page')
@section('title', 'Product')

@section('content')

    <style>
        .center-cropped {
            object-fit: none;/* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 400px;
            width: 800px;
        }

        .pac-container {
            z-index: 10000 !important;
        }
    </style>

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <div class="main-container">

        <div class="col-md-12">
            @if(!Auth::User()->picture || !Auth::User()->city)
                <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
                    <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Your Profile is incomplete, Please complete it</span>
                    <a href="{{url('AdminLte/myProfie')}}">click to view profile</a>
                </div>
            @endif
                <br>
        </div>

        <div class="h-spacer"></div>
        <div class="container">
            <div class="row">


                    <div class="col-sm-9 page-content col-thin-right">
                        <div class="inner inner-box ads-details-wrapper">
                            <h2 class="enable-long-words">
                                <strong>
                                    <a href="#">
                                        {{$item->title}}
                                    </a>
                                </strong>
                            </h2>
                            <span class="info-row">
							<span class="category">{{$item->category->category}}</span> -&nbsp;
							<span class="item-location"><i class="fa fa-map-marker"></i> {{$item->user->city}} </span>
						</span>

                            <div class="ads-image">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        <?php $i = 0;?>
                                        @foreach($item->pictures as $picture)
                                            @if($i==0)
                                                    <div class="item active" id="{{'cros-'.$picture->id}}">
                                                        <img style="width:100%; height: 500px;" src="{{"../".$picture->path}}"  alt="Los Angeles">
                                                    </div>
                                            @else
                                                    <div class="item" id="{{'cros-'.$picture->id}}">
                                                        <img style="width:100%; ; height: 500px" src="{{"../".$picture->path}}" alt="Los Angeles">
                                                    </div>
                                            @endif
                                        <?php $i++;?>
                                    @endforeach
                                        </div>

                                        <!-- Left and right controls -->
                                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                            </div>
                            <!--ads-image-->

                            <div class="ads-details">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab-details" data-toggle="tab"><h4>Item Details</h4></a>
                                    </li>

                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-details">
                                        <div class="row" style="padding: 10px;">
                                            <div class="ads-details-info col-md-12 col-sm-12 col-xs-12 enable-long-words from-wysiwyg">

                                                <!-- Location -->
                                                <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                                    <div>
                                                        <span><i class="fa fa-map-marker"></i> Location: </span>
                                                        <span>
                                                        <a href="{{'/AdminLte/browseItem?Events=All&lat'.$item->user->lat."&lng=".$item->user->lng}}">
                                                        {{$item->user->city}}
														</a>
													</span>
                                                    </div>
                                                </div>
                                                <!-- Price / Salary -->
                                                <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                                    <div>
														<span>
															Size
														</span>
                                                        <span>
                                                        {{$item->size}}
                                                    </span>
                                                    </div>
                                                </div>
                                                <div style="clear: both;"></div>
                                                <hr>

                                                <!-- Description -->
                                                <div class="detail-line-content">
                                                    <p>{{$item->description}}</p>
                                                </div>

                                                <!-- Custom Fields -->

                                                <!-- Tags -->
                                                <div style="clear: both;"></div>

                                                <!-- Actions -->
                                            </div>

                                            <br>&nbsp;<br>
                                        </div>
                                    </div>


                                </div>
                                <!-- /.tab content -->

                            </div>
                        </div>
                        <!--/.ads-details-wrapper-->
                    </div>
                    <div class="col-sm-3 page-sidebar-right">
                        <aside>
                            @if($item->user->id == Auth::user()->id)
                                <div class="panel sidebar-panel panel-contact-seller">
                                    <div class="panel-heading">Manage</div>
                                    <div class="panel-content user-info">
                                        <div class="panel-body text-center">
                                            <div class="seller-info">
                                                <h3 class="no-margin">{{$item->user->first_name}}</h3>
                                                <p>
                                                    Location:&nbsp;
                                                    <strong>
                                                        <a href="{{'/AdminLte/browseItem?Events=All&lat'.$item->user->lat."&lng=".$item->user->lng}}">
                                                            {{$item->user->city}}
                                                        </a>
                                                    </strong>
                                                </p>
                                            </div>
                                            <div class="user-ads-action">
                                                <a class="btn btn-default btn-block" href="{{url('/editItem/'.$item->id)}}">
                                                    <i class="far fa-pencil-square"></i> Update Detail
                                                </a>
                                                <br>
                                                <a class="btn btn-default btn-block" data-toggle="modal" data-target="#updateimage">
                                                    <i class="far fa-pencil-square"></i> Update Images
                                                </a>
                                                <br>
                                                <a class="btn btn-default btn-block btn-danger" href="{{url('/deleteItem/'.$item->id)}}">
                                                    <i class="far fa-pencil-square"></i> Delete
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="modal fade" id="updateimage">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h3 class="modal-title">Images Section</h3>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="col-md-12" id="message"></div>
                                                <div class="box-body no-padding">
                                                    <div class="col-md-12">
                                                    <ul id="images">
                                                    @foreach($item->pictures as $picture)
                                                        <div class="col-md-2" id="{{'img-'.$picture->id}}">
                                                            <a href="#" type="button" class="close" id="{{$picture->id}}">&times;</a>
                                                            <img src="{{asset($picture->path)}}" alt="Not loaded" style="width:100%; height: 100%;">
                                                        </div>
                                                    @endforeach
                                                    </ul>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <br><br>
                                                    <form role="form" action="{{url('/AddPictures/'.$item->id)}}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <div class="box-body">
                                                            <div class="form-group">
                                                                <label for="exampleInputFile">Add Image/s</label>
                                                                <input name="photos[]" type="file" multiple value="Select Files">
                                                                <p class="help-block">Only JPG or PNG Types are allowed</p>
                                                            </div>
                                                        </div>

                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-success" style="float: right" >Update</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                            @endif



                        </aside>
                    </div>
                <!--/.page-content-->
            </div>

        </div>

        <div class="h-spacer"></div>

    </div>

@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>

    <script>
        $(document).ready(function() {
            $("#images a").click(function () {
                    id = $(this).attr('id');
                    $.ajax({
                        method : 'Get',
                        url: '/deletePicture/'+id,
                        success: function (data){
                            if (data['success'])
                            {
                                //$('#cros-'+id).remove();
                                $('#img-'+id).remove();
                            }
                            $("#message").text( data['message']);
                        },

                    });
            });
        });
    </script>
@stop
