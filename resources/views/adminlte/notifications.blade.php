@extends('adminlte::page')
@section('title', 'Notifications')

@section('content_header')
    <h1>Notifications</h1>
@stop
@section('content')
    <div class="box box-primary" style="position: relative; left: 0px; top: 0px;">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="ion ion-clipboard"></i>
            <h3 class="box-title">All Notifications</h3>
            <a type="button" class="btn btn-success" style="float: right" href="{{url('/clearNotifications')}}">Clear All</a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
            <ul class="todo-list ui-sortable">
                @if(sizeof($notifications)>0)
                    @foreach($notifications as $notification)
                        <li>
                            <a href="{{url('/readNotification?notification_id='.$notification->id)}}">
                                <i class="fa fa-lg fa-users text-aqua mr-20"></i> <span style="font-size: 140%">{{$notification->title}}</span>
                            </a>
                            <span style="float: right">{{ \Carbon\Carbon::parse($notification->created_at)->format('d M').' at '
                    .\Carbon\Carbon::parse($notification->created_at)->format('h:i a')}}</span>
                        </li>
                    @endforeach

                @else
                    <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                        <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                        <h4 style="margin:15px">You have got no notifications</h4>
                    </div>
                @endif

            </ul>
        </div>
    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
@stop
