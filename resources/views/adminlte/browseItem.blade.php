@extends('adminlte::page')
@section('title', 'Search Ads')
@section('content_header')
    <h1>Browse Item</h1>
@stop
@section('content')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn7IlopznAGZ4LYnBmtwAHOSdLSkk5TU&sensor=false&libraries=places&language=en" async defer></script>

    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
        .corner {
            width: 0;
            height: 0;
            border-top: 80px solid #ffcc00;
            border-bottom: 80px solid transparent;
            border-left: 80px solid transparent;
            position:absolute;
            right:0;
        }

        .corner span {
            position:absolute;
            top: -65px;
            width: 70px;
            left: -65px;
            text-align: center;
            font-size: 16px;
            font-family: arial;
            transform: rotate(45deg);
            display:block;
        }
    </style>

    <aside class="control-sidebar control-sidebar-dark">
        <a href="#" class="glyphicon glyphicon-remove"  data-toggle="control-sidebar" style="margin: 10px; float: right"></a>
        <form method="get" action="{{url('/AdminLte/browseItem')}}">
            <div style="margin: 15px; margin-top: 25px">
                <p style="margin-bottom: 3px; color: white"><b>Query</b></p>
                <input class="form-control" style="height:30px; border-radius: 3px" type="text" name="query" placeholder="Enter text" required
                       value="{{app('request')->input('query')}}">
            </div>

            @php
                $events = \ELends\Event::all();
            @endphp

            <div style="margin: 15px">
                <p style="margin-bottom: 3px; color: white"><b>Events</b></p>
                @if(app('request')->input('Events')=="All")
                    <input type="radio" name="Events" value="All" checked>All<br>
                @elseif(app('request')->input('Events')!="All")
                    <input type="radio" name="Events" value="All">All<br>
                @else
                    <input type="radio" name="Events" value="All" checked>All<br>
                @endif

                @foreach($events as $event)
                    @if(app('request')->input('Events')==$event->id)
                        <input type="radio" name="Events" value="{{$event->id}}" checked> {{$event->name}}<br>
                    @else
                        <input type="radio" name="Events" value="{{$event->id}}"> {{$event->name}}<br>
                    @endif
                @endforeach
            </div>

            <div style="margin: 15px">
                <p style="margin-bottom: 3px; color: white"><b>Price</b></p>
                <input class="form-control" style="height:30px; border-radius: 3px" name="rent" type="number" min="0" placeholder="Enter price" required>
            </div>

            <div style="margin: 15px">
                <p style="margin-bottom: 3px; color: white"><b>Location</b></p>
                <input id="loc" class="form-control" style="height:30px; border-radius: 3px" type="text" placeholder="Enter Location" required>

                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">

                <script>
                    var input = document.getElementById('loc');
                    var autocomplete = new google.maps.places.Autocomplete(input);
                    autocomplete.addListener('place_changed', function() {
                        var place = autocomplete.getPlace();
                        console.log("Lat :" + place.geometry.location.lat());
                        console.log("Lng :" + place.geometry.location.lng());
                        $("#lat").val(place.geometry.location.lat());
                        $("#lng").val(place.geometry.location.lng());
                    });
                </script>
            </div>

            <div style="margin: 5px">
                <button type="submit" style="margin-top:3px; float:right; margin-right:10px; color: #0C0C0C; border-radius: 3px"><b>Filter</b></button>
            </div>
        </form>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
      immediately after the control sidebar -->
    <div class="control-sidebar-bg">
    </div>

    <div class="row" style="margin-top: 40px">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">

                @if(!empty($result))
                    <div class="box-header with-border">
                        <h3 class="box-title">Results </h3>
                        <a href="#" class="btn btn-info btn-lg" style="float: right; margin-right: 10px" data-toggle="control-sidebar">
                            <span class="glyphicon glyphicon-filter"></span> Filter
                        </a>
                        <hr>
                        <subheading>Showing results for {{"'".$result['query']."'"}}</subheading><hr><div class="clearfix"></div>
                    </div>
                @else
                    <div class="box-header with-border">
                        <h3 class="box-title">Results </h3>
                        <a href="#" class="btn btn-info btn-lg" style="float: right; margin-right: 10px" data-toggle="control-sidebar">
                            <span class="glyphicon glyphicon-filter"></span> Filter
                        </a>
                    </div>

            @endif

                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="adds-wrapper noSideBar">

                        <?php
                        $empty = false;
                        ?>

                        @if(empty($result))
                            <?php
                            $empty = true;
                            ?>
                            <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px">You haven't search anything</h4>
                            </div>
                        @endif

                        @if(!empty($result))
                            <?php
                            $ads = $result['ads'];
                            ?>
                        @endif

                        @if(!empty($result)&&sizeof($ads)>0)
                            @foreach($ads as $ad)
                                    <div class="item-list make-grid">
                                        <div class="col-sm-2 no-padding photobox">
                                            <div class="add-image">
                                                <a href="{{url('viewproduct/'.$ad->id)}}">
                                                    <div class="corner"><span>Rs. {{$ad->rent}}</span></div>
                                                    <img class="thumbnail no-margin center-cropped" src="{{asset($ad->item->pictures[0]->path)}}" alt="img">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="col-sm-7 add-desc-box">
                                            <div class="add-details">
                                                <h5 class="add-title">
                                                    <a class="giveMeEllipsis" href="#">{{$ad->title}} </a>
                                                </h5>
                                                <span class="info-link">
                                            {{$ad->item->category->category}}
                                        </span>
                                                <span class="info-row">
                                            <span class="date"> {{ \Carbon\Carbon::parse($ad->created_at)->format('d M')}} </span><br>
                                        </span>
                                                <a href="/Favorites/add/{{$ad->id}}" class="btn btn-default btn-block showphone" style="margin-top: 10px">
                                                    Add to Favorites
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                            @endforeach
                        @elseif(!$empty)
                                <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                    <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                    <h4 style="margin:15px; text-align: center">Ads not available</h4>
                                </div>
                        @endif

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>


@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop
