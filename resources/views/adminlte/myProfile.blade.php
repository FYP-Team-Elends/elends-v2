@extends('adminlte::page')
@section('title', 'My Profile')
@section('content_header')
    <h1>Profile</h1>
@stop
@section('content')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn7IlopznAGZ4LYnBmtwAHOSdLSkk5TU&sensor=false&libraries=places&language=en" async defer></script>
    <div class="row">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="{{asset(Auth::user()->picture)}}" alt="User profile picture" style="height: 100px">

                    <h3 class="profile-username text-center">{{$user->first_name.' '.$user->last_name}}</h3>


                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b>Total Ads</b> <a class="pull-right">{{$postedadcount}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Total Items</b> <a class="pull-right">{{$itemcount}}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Bookings</b> <a class="pull-right">{{$bookingadscount}}</a>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#UpdateProfile" data-toggle="tab">General Information</a></li>
                    <li><a href="#UpdateDetail" data-toggle="tab">Contact Information</a></li>
                    <li><a href="#UpdatePassword" data-toggle="tab">Update Password</a></li>
                </ul>
                <div class="tab-content">
                    <div class="active tab-pane" id="UpdateProfile">
                        <form role="form" action="{{url('/update_info')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">First Name</label>
                                    <input name="first_name" pattern="^[a-zA-Z ]*$" type="text" class="form-control" title="Enter Text Only" id="exampleInputEmail1" placeholder="Enter first name" required value="{{$user->first_name}}" onemptied="this.setCustomValidity('Enter First Name')">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last Name</label>
                                    <input name="last_name" type="text" pattern="^[a-zA-Z ]*$" class="form-control" title="Enter Text Only" id="exampleInputEmail1" placeholder="Enter last name" required value="{{$user->last_name}}" onemptied="this.setCustomValidity('Enter Last Name')">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Email</label>
                                    <input name="email" type="email" pattern="^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$" readonly class="form-control" id="exampleInputPassword1" value="{{$user->email}}">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="UpdateDetail">
                        <form role="form" action="{{url('/update_detail')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Address</label>
                                    <input name="Address" id="Address" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Address" value="{{$user->city}}" onemptied="this.setCustomValidity('Enter Address')">
                                    <input type="hidden" name="lat" id="lat">
                                    <input type="hidden" name="lng" id="lng">
                                    <script>
                                        var input = document.getElementById('Address');
                                        var autocomplete = new google.maps.places.Autocomplete(input);
                                        autocomplete.addListener('place_changed', function() {
                                            var place = autocomplete.getPlace();
                                            console.log("Lat :" + place.geometry.location.lat());
                                            console.log("Lng :" + place.geometry.location.lng());
                                            $("#lat").val(place.geometry.location.lat());
                                            $("#lng").val(place.geometry.location.lng());
                                        });
                                    </script>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Phone No.</label>
                                    <input name="phone" type="tel" pattern="^([0][3])([0,1,2,3,4,6,7,8,9])([0-9][0-9][0-9][0-9][0-9][0-9][0-9])" class="form-control" title="03XXXXXXXXX"id="exampleInputEmail1" placeholder="Enter Phone number" value="{{$user->phone}}" onemptied="this.setCustomValidity('Enter Valid Phone No.')">
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Picture</label>
                                    <input name="picture" type="file" class="form-control" id="exampleInputEmail1" placeholder="Enter last name" value="{{$user->picture}}">
                                </div>
                            </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="UpdatePassword">

                        <form role="form" action="{{url('/update_password')}}" method="POST" enctype="multipart/form-data">
                            @if(Session::has('Current_Password_Success'))
                                <p class="alert alert-success">{{ Session::get('Current_Password_Success') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif
                            @if(Session::has('Current_Password_Error'))
                                <p class="alert alert-danger">{{ Session::get('Current_Password_Error') }} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></p>
                            @endif

                        @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="current_password">Current Password</label>
                                    <input name="current_password" type="password" class="form-control" id="current" placeholder="Enter Current Password">
                                </div>
                                <div class="form-group">
                                    <label for="password">New Password</label>
                                    <input type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" name="password" title="Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit." class="form-control"
                                           placeholder="{{ trans('adminlte::adminlte.password') }}">
                                     </span>

                                    @if ($errors->has('password'))
                                        <span class="alert-error">
                                             <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="password_confirmation">Repeat Password</label>
                                    <input type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" title="Same As Above" name="password_confirmation" class="form-control"
                                           placeholder="{{ trans('adminlte::adminlte.retype_password') }}">
                                </div>
                        </div>
                            <!-- /.box-body -->

                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Update Password</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.tab-content -->
            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col -->
    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop
