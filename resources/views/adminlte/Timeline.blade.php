@extends('adminlte::page')
@section('title', 'Timeline')

@section('content')
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <!-- MAP & BOX PANE -->

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Timeline</h3>
                </div>
                <div class="box-body">
                    <ul class="timeline">

                        <!-- timeline time label -->
                        <li class="time-label">
        <span class="bg-red">
                       {{ \Carbon\Carbon::parse($statuses[0]->created_at)->format('d M. Y')}}
        </span>
                        </li>
                        <!-- /.timeline-label -->
                        <!-- timeline item -->
                        @foreach($statuses as $status)
                            @if($status->status==\ELends\TimelineStatus::$REQUEST)
                                @if(\Illuminate\Support\Facades\Auth::id()==$status->booking->user_id)
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">You sent request</a> </h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>

                                            @if($status->booking->timeline_status == \ELends\TimelineStatus::getIntStatus($status->status))
                                                <div class="timeline-footer">
                                                    <a class="btn btn-danger btn-xs" style="margin: 3px" data-toggle="modal" data-target="#cancelRequest">Cancel Request</a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header no-border"><a href="#">{{$status->booking->user->first_name.' '.$status->booking->user->last_name}}</a> sent you booking request</h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            @if($status->booking->timeline_status == \ELends\TimelineStatus::getIntStatus($status->status))
                                                <div class="timeline-footer">
                                                    <a class="btn btn-success btn-xs" style="margin: 3px" data-toggle="modal" data-target="#acceptRequest">Accept</a>
                                                    <a class="btn btn-danger btn-xs" style="margin: 3px" data-toggle="modal" data-target="#rejectRequest">Reject</a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @endif
                                <div class="modal fade" id="acceptRequest">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h3 class="modal-title">Accept Booking Request</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="box-body no-padding">
                                                    <form role="form" action="{{url('timeline/updateTimeline/')}}" method="POST">
                                                        @csrf

                                                        <div class="form-group">
                                                            <label> Message</label>
                                                            <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                                                        </div>
                                                        <input type="hidden" name="status" value="{{\ELends\TimelineStatus::$ACCEPTED}}">
                                                        <input type="hidden" name="bookingId" value="{{$status->booking_id}}">
                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-success" style="float: right">Accept Request</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="rejectRequest">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h3 class="modal-title">Accept Booking Request</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="box-body no-padding">
                                                    <form role="form" action="{{url('timeline/updateTimeline/')}}" method="POST">
                                                        @csrf

                                                        <div class="form-group">
                                                            <label> Message</label>
                                                            <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                                                        </div>                                            <input type="hidden" name="status" value="{{\ELends\TimelineStatus::$REJECTED}}">
                                                        <input type="hidden" name="bookingId" value="{{$status->booking_id}}">
                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-danger" style="float: right">Reject Request</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal fade" id="cancelRequest">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h3 class="modal-title">Cancel Booking Request</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="box-body no-padding">
                                                    <form role="form" action="{{url('timeline/updateTimeline/')}}" method="POST">
                                                        @csrf

                                                        <div class="form-group">
                                                            <label> Message</label>
                                                            <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                                                        </div>
                                                        <input type="hidden" name="status" value="{{\ELends\TimelineStatus::$REQUEST_CANCELLED}}">
                                                        <input type="hidden" name="bookingId" value="{{$status->booking_id}}">
                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-danger" style="float: right">Cancel Request</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif($status->status==\ELends\TimelineStatus::$REQUEST_CANCELLED)
                                @if(\Illuminate\Support\Facades\Auth::id()==$status->booking->user_id)
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">You cancelled booking request</a> </h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header no-border"><a href="#">{{$status->booking->user->first_name.' '.$status->booking->user->last_name}}</a> cancelled booking request</h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                        </div>
                                    </li>
                                @endif

                            @elseif($status->status==\ELends\TimelineStatus::$ACCEPTED)
                                @if(\Illuminate\Support\Facades\Auth::id()==$status->booking->user_id)
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Your request has been accepted</a> </h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            {{--<div class="timeline-footer">--}}
                                            {{--<a class="btn btn-primary btn-xs fa fa-envelope" style="margin: 3px"> Send Message</a>--}}
                                            {{--<a class="btn btn-danger btn-xs fa fa-times" style="margin: 3px">Cancel Request</a>--}}
                                            {{--</div>--}}
                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">You accepted booking request</a> </h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            @if($status->booking->timeline_status == \ELends\TimelineStatus::getIntStatus($status->status))
                                                <div class="timeline-footer">
                                                    <a class="btn btn-primary btn-xs" style="margin: 3px" data-toggle="modal" data-target="#markPosted"> Mark posted</a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>

                                @endif
                                <div class="modal fade" id="markPosted">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h3 class="modal-title">Mark product as posted</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="box-body no-padding">
                                                    <form role="form" action="{{url('timeline/updateTimeline/')}}" method="POST">
                                                        @csrf

                                                        <div class="form-group">
                                                            <label> Message</label>
                                                            <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                                                        </div>
                                                        <input type="hidden" name="status" value="{{\ELends\TimelineStatus::$DELIVERED}}">
                                                        <input type="hidden" name="bookingId" value="{{$status->booking_id}}">
                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-danger" style="float: right">Mark Posted</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif($status->status==\ELends\TimelineStatus::$REJECTED)
                                @if(\Illuminate\Support\Facades\Auth::id()==$status->booking->user_id)
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Your booking request has been rejected</a> </h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            {{--<div class="timeline-footer">--}}
                                            {{--<a class="btn btn-primary btn-xs fa fa-envelope" style="margin: 3px"> Send Message</a>--}}
                                            {{--<a class="btn btn-danger btn-xs fa fa-times" style="margin: 3px">Cancel Request</a>--}}
                                            {{--</div>--}}
                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <!-- timeline icon -->
                                        <i class="fa fa-envelope bg-blue"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{$status->created_at}}</span>

                                            <h3 class="timeline-header"><a href="#">You rejected booking request</a> </h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            {{--<div class="timeline-footer">--}}
                                            {{--<a class="btn btn-primary btn-xs fa fa-envelope" style="margin: 3px"> Send Message</a>--}}
                                            {{--<a class="btn btn-danger btn-xs fa fa-times" style="margin: 3px">Cancel Request</a>--}}
                                            {{--</div>--}}
                                        </div>
                                    </li>
                                @endif
                            @elseif($status->status==\ELends\TimelineStatus::$DELIVERED)
                                @if(\Illuminate\Support\Facades\Auth::id()==$status->booking->user_id)
                                    <li>
                                        <i class="fa fa-comments bg-yellow"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Item posted for delivery</a></h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            @if($status->booking->timeline_status == \ELends\TimelineStatus::getIntStatus($status->status))
                                                <div class="timeline-footer">
                                                    <a class="btn btn-primary btn-xs" style="margin: 3px" data-toggle="modal" data-target="#markReceived">Mark Received</a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <i class="fa fa-comments bg-yellow"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Item posted for delivery</a></h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                        </div>
                                    </li>
                                @endif
                                <div class="modal fade" id="markReceived">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h3 class="modal-title">Mark product as received</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="box-body no-padding">
                                                    <form role="form" action="{{url('timeline/updateTimeline/')}}" method="POST">
                                                        @csrf

                                                        <div class="form-group">
                                                            <label> Message</label>
                                                            <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                                                        </div>
                                                        <input type="hidden" name="status" value="{{\ELends\TimelineStatus::$RECEIVED}}">
                                                        <input type="hidden" name="bookingId" value="{{$status->booking_id}}">
                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-success" style="float: right">Mark Received</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif($status->status==\ELends\TimelineStatus::$RECEIVED)
                                @if(\Illuminate\Support\Facades\Auth::id()==$status->booking->user_id)
                                    <li>
                                        <i class="fa fa-comments bg-yellow"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Item received</a></h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            @if($status->booking->timeline_status == \ELends\TimelineStatus::getIntStatus($status->status))
                                                <div class="timeline-footer">
                                                    <a class="btn btn-primary btn-xs" style="margin: 3px" data-toggle="modal" data-target="#markReturned">Mark Returned</a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <i class="fa fa-comments bg-yellow"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Item received</a></h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                        </div>
                                    </li>
                                @endif
                                <div class="modal fade" id="markReturned">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h3 class="modal-title">Mark product as returned</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="box-body no-padding">
                                                    <form role="form" action="{{url('timeline/updateTimeline/')}}" method="POST">
                                                        @csrf

                                                        <div class="form-group">
                                                            <label> Message</label>
                                                            <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                                                        </div>
                                                        <input type="hidden" name="status" value="{{\ELends\TimelineStatus::$RETURNED}}">
                                                        <input type="hidden" name="bookingId" value="{{$status->booking_id}}">
                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-success" style="float: right">Mark Returned</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif($status->status==\ELends\TimelineStatus::$RETURNED)
                                @if(\Illuminate\Support\Facades\Auth::id()==$status->booking->user_id)
                                    <li>
                                        <i class="fa fa-comments bg-yellow"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Item returned</a></h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                        </div>
                                    </li>
                                @else
                                    <li>
                                        <i class="fa fa-comments bg-yellow"></i>

                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                            <h3 class="timeline-header"><a href="#">Item returned</a></h3>

                                            <div class="timeline-body">
                                                {{$status->message}}
                                            </div>
                                            @if($status->booking->timeline_status == \ELends\TimelineStatus::getIntStatus($status->status))
                                                <div class="timeline-footer">
                                                    <a class="btn btn-primary btn-xs" style="margin: 3px" data-toggle="modal" data-target="#markCompleted">Mark Completed</a>
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                @endif
                                <div class="modal fade" id="markCompleted">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">

                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h3 class="modal-title">Mark order complete</h3>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <!-- Modal body -->
                                            <div class="modal-body">
                                                <div class="box-body no-padding">
                                                    <form role="form" action="{{url('timeline/updateTimeline/')}}" method="POST">
                                                        @csrf

                                                        <div class="form-group">
                                                            <label> Message</label>
                                                            <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                                                        </div>
                                                        <input type="hidden" name="status" value="{{\ELends\TimelineStatus::$COMPLETED}}">
                                                        <input type="hidden" name="bookingId" value="{{$status->booking_id}}">
                                                        <!-- /.box-body -->

                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-success" style="float: right">Mark Completed</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @elseif($status->status==\ELends\TimelineStatus::$COMPLETED)
                                <li>
                                    <i class="fa fa-comments bg-yellow"></i>

                                    <div class="timeline-item">
                                        <span class="time"><i class="fa fa-clock-o"></i> {{ \Carbon\Carbon::parse($status->created_at)->format('h:i a')}}</span>

                                        <h3 class="timeline-header"><a href="#">Order Completed</a></h3>

                                        <div class="timeline-body">
                                            {{$status->message}}
                                        </div>
                                    </div>
                                </li>
                            @endif
                        @endforeach

                        <li class="time-label">
        <span class="bg-red">
                       {{ \Carbon\Carbon::parse($statuses[sizeof($statuses)-1]->created_at)->format('d M. Y')}}
        </span>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
    </div>

@stop