@extends('adminlte::page')
@section('title', 'Post Item')

@section('content_header')
    <h1>Post Item</h1>
@stop
@section('content')

    <link href="{{asset('js/styles.imageuploader.css')}}" rel="stylesheet"></link>

    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">All Items</h3>
                </div>
                <div class="box-body no-padding">
                    <form role="form" action="{{url('/postItem')}}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Item Name</label>
                                <input type="text" class="form-control" name="name" id="exampleInputEmail1" placeholder="Enter Name" required onemptied="this.setCustomValidity('Enter Item Name')">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Select Category</label>
                                <select class="form-control" name="category" required onemptied="this.setCustomValidity('Select Category')">
                                    <option value=null>Select Category</option>
                                    @foreach($category as $item)
                                        <option value="{{$item->id}}">{{$item->category}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Item Description</label>
                                <textarea class="form-control" name="description" style="height: 250px" required onemptied="this.setCustomValidity('Enter Description')"></textarea>
                            </div>

                            <div class="form-group">

                                <label for="exampleInputEmail1">Size</label>
                                <input type="text" list="sizes" class="form-control" name="size" id="exampleInputEmail1" placeholder="Enter Size" required onemptied="this.setCustomValidity('Enter Sizes')">

                                <datalist id="sizes">
                                    <option value="Small">
                                    <option value="Medium">
                                    <option value="Large">
                                    <option value="Extra Large">
                                </datalist>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Item Color</label>
                                <input type="text" list="colors" class="form-control" name="color" id="exampleInputEmail1" placeholder="Enter Color" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Add Pictures</label>
                                <input name="photos[]" type="file" multiple value="Select Files" required>
                            </div>

                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop
