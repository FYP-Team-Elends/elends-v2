@extends('adminlte::page')
@section('title', 'Services')

@section('content_header')
    <h1>Services</h1>
@stop
@section('content')

    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
    </style>
    <div class="row">

        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red">
                   <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                        width="50" height="50"
                        viewBox="0 0 252 252"
                        style="fill:#ffffff;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,252v-252h252v252z" fill="none"></path><path d="M-7.56,259.56v-267.12h267.12v267.12z" fill="#dd4b39"></path><g fill="#ffffff"><g id="surface1"><path d="M105.58897,53.75736c-3.15884,0.32398 -6.68218,0.76947 -10.69149,1.94391v28.18666c17.61667,-18.66961 44.58838,2.1464 62.20504,-21.70697c-25.3923,5.42675 -29.4016,-10.61049 -51.51355,-8.4236zM177.83754,65.09682v26.89072c14.49832,-13.48586 37.33923,12.43291 51.83754,-5.18375c-23.85336,-1.05294 -24.86581,-23.77237 -51.83754,-21.70697zM31.3965,71.25253c-2.67287,0.32398 -5.71023,1.09345 -9.07157,2.26789v26.89072c14.49832,-18.66961 37.33923,2.0654 51.83754,-20.73501c-20.85651,5.42675 -24.17735,-10.61049 -42.76597,-8.4236zM126,95.22739c-68.44174,0 -124.41009,18.66961 -124.41009,41.47003v114.04258h93.30757v-102.70312c0,0 10.36751,-0.97195 31.10252,-0.97195c20.73501,0 31.10252,0.97195 31.10252,0.97195v102.70312h93.30757v-114.04258c0,-22.80042 -55.96833,-41.47003 -124.41009,-41.47003zM177.83754,148.03688l20.73501,2.26789v26.89072l-20.73501,-0.97195zM74.16246,149.00884v28.18666l-20.73501,0.97195v-26.89072zM32.69243,156.46048v23.65088l-10.36751,1.29594v-22.03095c0,0 4.13081,-0.85046 10.36751,-2.91586zM219.30757,156.46048c6.23671,1.05296 10.36751,2.91586 10.36751,2.91586v22.03095l-10.36751,-1.29594zM74.16246,197.93051v32.07448h-20.73501v-31.10252zM177.83754,197.93051l20.73501,0.97195v31.10252h-20.73501zM32.69243,200.84637v29.15861h-10.36751v-27.86268zM219.30757,200.84637l10.36751,1.29594v27.86268h-10.36751z"></path></g></g></g></svg>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">Venue</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green">

                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                         width="50" height="50"
                         viewBox="0 0 252 252"
                         style="fill:#ffffff;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,252v-252h252v252z" fill="none"></path><path d="M-7.56,259.56v-267.12h267.12v267.12z" fill="#27ae60"></path><g fill="#ffffff"><g id="surface1"><path d="M-13.86,-1.6275v244.755h279.72v-209.79h-174.825l-20.76047,-34.965zM9.45,56.6475h233.1v163.17h-233.1zM102.69,79.9575v11.655h-11.655l-34.965,11.655v81.585h139.86v-93.24h-46.62v-11.655zM62.99016,85.05656v10.19813l17.4825,-6.19172v-4.00641zM126,103.2675c19.30359,0 34.965,15.66141 34.965,34.965c0,19.30359 -15.66141,34.965 -34.965,34.965c-19.30359,0 -34.965,-15.66141 -34.965,-34.965c0,-19.30359 15.66141,-34.965 34.965,-34.965zM126,114.9225c-12.88424,0 -23.31,10.42576 -23.31,23.31c0,12.88424 10.42576,23.31 23.31,23.31c12.88424,0 23.31,-10.42576 23.31,-23.31c0,-12.88424 -10.42576,-23.31 -23.31,-23.31z"></path></g></g></g></svg>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">Photographers</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                         width="50" height="50"
                         viewBox="0 0 252 252"
                         style="fill:#ffffff;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,252v-252h252v252z" fill="none"></path><path d="M0,252v-252h252v252z" fill="#00c0ef"></path><g fill="#ffffff"><g id="surface1"><path d="M94.94471,32.76v20.70353h20.70353v10.35176h20.70353v-10.35176h20.70353v-20.70353zM126,74.16706c-56.93471,0 -103.51765,46.58294 -103.51765,103.51765h207.03529c0,-56.93471 -46.58294,-103.51765 -103.51765,-103.51765zM1.77882,188.03647v20.70353h248.44235v-20.70353z"></path></g></g></g></svg>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">Caterers</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow">
                    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"
                         width="50" height="50"
                         viewBox="0 0 252 252"
                         style="fill:#ffffff;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal"><path d="M0,252v-252h252v252z" fill="none"></path><path d="M0,252v-252h252v252z" fill="#f39c12"></path><g fill="#ffffff"><g id="surface1"><path d="M162.33984,6.09328c-28.65926,0 -37.30125,2.5725 -37.30125,2.5725c-6.02929,2.04996 -14.34972,3.49699 -17.68594,9.96844c-1.00488,1.92938 -4.2607,9.04394 -7.7175,18.32906c4.70285,0.12059 8.76258,0.40196 12.21938,0.64313c2.53231,-6.31066 4.82344,-9.52629 5.46656,-11.25469c0.8843,-2.04996 1.68821,-8.36062 45.01875,-8.36062c43.33055,0 44.25504,7.31555 44.37563,7.39594c1.32644,3.81856 6.71262,11.85762 10.93312,28.2975c0,0 -18.24867,5.78812 -55.30875,5.78812c-2.21074,0 -4.05972,0.04019 -6.10969,0c1.7284,4.22051 4.26071,10.73215 7.07438,18.32906c7.95868,0 15.67618,5.42637 18.32906,13.18406l2.25094,6.43125c1.40684,4.05972 1.32644,8.44102 -0.32156,12.21938c12.62133,0.80391 17.80653,2.21074 18.97219,2.5725l1.92937,0.32156c6.51165,1.64801 17.32418,4.46168 21.86625,14.79187c0.68332,1.52743 2.13035,4.74305 3.85875,9.00375h2.25094c3.53719,0 6.43125,-2.89406 6.43125,-6.43125v-47.91281c0,-1.16566 -0.40195,-5.50676 -0.96469,-6.43125c0,0 -1.20585,-2.61269 -3.21563,-8.36063h7.39594c3.53719,0 5.38618,-4.18031 4.18031,-7.7175l-2.25094,-6.43125c-1.20585,-3.53719 -5.145,-6.10969 -8.68219,-6.10969h-7.39594c-4.66266,-13.22426 -9.36551,-25.00148 -10.93312,-28.61906c-2.93426,-6.63222 -12.54094,-8.19984 -17.68594,-9.64687c0,0 -8.32043,-2.5725 -36.97969,-2.5725zM90.30984,47.25328c-28.65926,0 -37.30125,2.5725 -37.30125,2.5725c-6.02929,2.04996 -14.34972,3.49699 -17.68594,9.96844c-1.32644,2.5725 -6.19007,14.67129 -10.93312,28.2975h-7.39594c-3.53719,0 -7.47632,2.5725 -8.68219,6.10969l-2.25094,6.43125c-1.20585,3.53719 0.64312,7.7175 4.18031,7.7175h7.39594c-1.96957,5.74793 -3.21562,8.36062 -3.21562,8.36062c-0.56273,0.92449 -0.96469,5.26559 -0.96469,6.43125v47.91281c0,3.53719 2.89406,6.43125 6.43125,6.43125h17.68594c3.53719,0 6.43125,-2.89406 6.43125,-6.43125v-9.00375h27.01125c2.93426,-7.23516 10.3704,-12.21938 18.0075,-12.21938c3.77836,-10.41059 7.47633,-19.89668 9.00375,-22.83094c4.74305,-9.20472 14.30953,-11.93801 20.58,-13.82719c1.04508,-0.32156 2.25094,-0.64313 3.21563,-0.96469c1.96957,-0.60293 11.77722,-3.21563 40.51687,-3.21563c0.24118,0 0.40196,0 0.64312,0c-0.08039,-0.20097 -0.24117,-0.40195 -0.32156,-0.64312h7.39594c3.53719,0 5.38618,-4.18031 4.18031,-7.7175l-2.25094,-6.43125c-1.20585,-3.53719 -5.145,-6.10969 -8.68219,-6.10969h-7.39594c-4.66266,-13.22426 -9.36551,-25.00148 -10.93312,-28.61906c-2.93426,-6.63222 -12.54094,-8.19984 -17.68594,-9.64687c0,0 -8.32043,-2.5725 -36.97969,-2.5725zM90.30984,59.15109c43.33055,0 44.25504,7.31555 44.37562,7.39594c1.32644,3.81856 6.71262,11.85762 10.93312,28.2975c0,0 -18.24867,5.78812 -55.30875,5.78812c-37.06007,0 -55.63031,-5.78812 -55.63031,-5.78812c4.46168,-17.80653 9.52629,-24.51914 10.61156,-27.33281c0.8843,-2.04996 1.68821,-8.36063 45.01875,-8.36063zM216.68391,78.76641c3.53719,0 7.07438,0.28137 7.07438,6.43125v6.43125c0,5.26559 -7.83809,6.43125 -15.435,6.43125h-8.36062c-6.39106,0 -8.36062,-1.76859 -8.36062,-5.46656c0,-3.97934 5.70774,-5.94891 10.61156,-8.36062c4.50187,-2.21074 11.01352,-5.46656 14.47031,-5.46656zM162.33984,119.28328c-28.65926,0 -37.30125,2.5725 -37.30125,2.5725c-6.02929,2.04996 -14.34972,3.49699 -17.68594,9.96844c-1.32644,2.5725 -6.19007,14.67129 -10.93313,28.2975h-7.39594c-3.53719,0 -7.47632,2.5725 -8.68219,6.10969l-2.25094,6.43125c-1.20585,3.53719 0.64312,7.7175 4.18031,7.7175h7.39594c-1.96957,5.74793 -3.21563,8.36062 -3.21563,8.36062c-0.56273,0.92449 -0.96469,5.26559 -0.96469,6.43125v47.91281c0,3.53719 2.89406,6.43125 6.43125,6.43125h17.68594c3.53719,0 6.43125,-2.89406 6.43125,-6.43125v-9.00375h92.28844v9.00375c0,3.53719 2.89406,6.43125 6.43125,6.43125h17.68594c3.53719,0 6.43125,-2.89406 6.43125,-6.43125v-47.91281c0,-1.16566 -0.40195,-5.50676 -0.96469,-6.43125c0,0 -1.20585,-2.61269 -3.21563,-8.36062h7.39594c3.53719,0 5.38618,-4.18031 4.18031,-7.7175l-2.25094,-6.43125c-1.20585,-3.53719 -5.145,-6.10969 -8.68219,-6.10969h-7.39594c-4.66266,-13.22426 -9.36551,-25.00148 -10.93312,-28.61906c-2.93426,-6.63222 -12.54094,-8.19984 -17.68594,-9.64687c0,0 -8.32043,-2.5725 -36.97969,-2.5725zM33.07172,119.92641c0.80391,-0.12059 1.68821,0 2.5725,0c3.4568,0 10.29,3.25582 14.79187,5.46656c4.90383,2.41172 10.29,4.34109 10.29,8.36063c0,3.69797 -1.96957,5.46656 -8.36062,5.46656h-8.36062c-7.59691,0 -15.11344,-1.12547 -15.11344,-6.43125v-6.43125c0,-4.62246 1.76859,-6.06949 4.18031,-6.43125zM162.33984,131.18109c43.33055,0 44.25504,7.31555 44.37563,7.39594c1.32644,3.81856 6.71262,11.85762 10.93312,28.2975c0,0 -18.24867,5.78813 -55.30875,5.78813c-37.06007,0 -55.63031,-5.78813 -55.63031,-5.78813c4.50187,-17.80653 9.52629,-24.51914 10.61156,-27.33281c0.8843,-2.04996 1.68821,-8.36062 45.01875,-8.36062zM105.10172,191.95641c0.80391,-0.12059 1.68821,0 2.5725,0c3.4568,0 10.29,3.25582 14.79187,5.46656c4.90383,2.41172 10.29,4.34109 10.29,8.36062c0,3.69797 -1.96957,5.46656 -8.36063,5.46656h-8.36062c-7.59691,0 -15.11344,-1.12547 -15.11344,-6.43125v-6.43125c0,-4.62246 1.76859,-6.06949 4.18031,-6.43125zM216.68391,191.95641c3.53719,0 7.07438,0.24118 7.07438,6.43125v6.43125c0,5.26559 -7.83809,6.43125 -15.435,6.43125h-8.36062c-6.39106,0 -8.36062,-1.76859 -8.36062,-5.46656c0,-4.01953 5.70774,-5.94891 10.61156,-8.36062c4.50187,-2.21074 11.01352,-5.46656 14.47031,-5.46656z"></path></g></g></g></svg>
                </span>

                <div class="info-box-content">
                    <span class="info-box-text">Car Rentals</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    </div>
    {{--<div class="col-sm-2 add-desc-box">
        <a class="btn btn-danger btn-block showphone" style="margin-top: 10px; float: left;">
            Book Marriage Hall
        </a>
        </div>

    <div class="col-sm-2 add-desc-box">
        <a class="btn btn-danger btn-block showphone" style="margin-top: 10px; float: left;">
            Catering
        </a>
        </div>

    <div class="col-sm-2 add-desc-box">
        <a class="btn btn-danger btn-block showphone" style="margin-top: 10px; float: left;">
            Transport
        </a>
        </div>

    <div class="col-sm-2 add-desc-box">
        <a class="btn btn-danger btn-block showphone" style="margin-top: 10px; float: left;">
            Movie Maker
        </a>
        </div>

    <div class="col-sm-2 add-desc-box">
        <a class="btn btn-danger btn-block showphone" style="margin-top: 10px; float: left;">
            Photographer
        </a>
    </div>

    <div class="col-sm-2 add-desc-box">
        <a class="btn btn-danger btn-block showphone" style="margin-top: 10px; float: left;">
            Lightening
        </a>
        </div>--}}



    <div class="row">
        <!-- Left col -->
        <div class="col-md-12" style="margin-top: 20px">
            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title" style="float: left;">Ads</h3>
                    <div style="float: right">
                        <form>
                            <input type="text" placeholder="Search location.." name="search">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="adds-wrapper noSideBar">

                        <?php
                        $empty = false;
                        ?>

                        @if(empty($services['ads']))
                            <?php
                            $empty = true;
                            ?>
                            <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px">You haven't search anything</h4>
                            </div>
                        @endif

                        @if(!empty($services))
                            <?php
                            $ads = $services['ads'];
                            ?>
                        @endif

                        @foreach($ads as $ad)
{{--                            {{dd($ad->service->category)}}--}}
                            <div class="item-list make-grid">
                                <div class="col-sm-2 no-padding photobox">
                                    <div class="add-image">
                                        <a href="{{url('viewproduct/'.$ad->id)}}">
                                            <img class="thumbnail no-margin center-cropped" src="{{asset($ad->service->image)}}" alt="img">
                                        </a>
                                    </div>
                                </div>

                                <div class="col-sm-7 add-desc-box">
                                    <div class="add-details">
                                        <h5 class="add-title">
                                            <a href="{{url('viewproduct/'.$ad->id)}}">{{$ad->title}} </a>
                                        </h5>
                                        <span class="info-link">
                                           Ad Category : {{\ELends\ServiceCategory::where('id',$ad->service->service_category_id)->pluck('name')->first()}}
                                        </span>
                                        <span class="info-row">
                                            <span class="date"> {{$ad->created_at}} </span><br>
                                        </span>
                                        <a href="/Favorites/add/{{$ad->id}}" class="btn btn-success btn-block showphone" style="margin-top: 10px">
                                            Add to Favorites
                                        </a>
                                    </div>
                                </div>

                            </div>
                        @endforeach

                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>



@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop
