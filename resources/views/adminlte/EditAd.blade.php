@extends('adminlte::page')
@section('title', 'Edit Ad')

@section('content_header')
    <h1>Edit Here</h1>
@stop
@section('content')

    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-body no-padding">
                    <form role="form" action="{{url('/updateAd')}}" method="get">
                        <div class="box-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" required value="{{$ad->id}}" >
                                <label for="exampleInputEmail1">Edit Title</label>
                                <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Enter Title" required value="{{$ad->title}}" onemptied="this.setCustomValidity('Enter Title')">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Edit Rent Per Day</label>
                                <input type="number" class="form-control" min="0" name="rent" id="exampleInputEmail1" placeholder="Enter Rent"  value="{{$ad->rent}}" required onemptied="this.setCustomValidity('Enter Rent')">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Edit Description</label>
                                <textarea class="form-control" name="description" required>{{$ad->description}}
                                </textarea>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Change Picture</label>
                                <input type="file" name="picture" id="exampleInputFile" required>
                                <p class="help-block">Only JPG or PNG Types are allowed</p>
                            </div>
                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop