@extends('adminlte::page')
@section('title', 'Services')

@section('content_header')
    <h1>Services</h1>
@stop
@section('content')
    <style type="text/css" href="{{asset('advancesearchstyle.css')}}"></style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn7IlopznAGZ4LYnBmtwAHOSdLSkk5TU&sensor=false&libraries=places&language=en" async defer></script>

    <style>
    .refine-search {
        padding: 33px 30px 21px 30px;
        background-color: #FFF;
        border: 1px solid #DDD;
    }

    .refine-search-heading {
        line-height: 1.44;
        font-weight: 700;
        text-transform: uppercase;
        margin-bottom: 23px;
    }
    h4 {
        font-size: 18px;
    }
    .row {
        margin-right: -15px;
        margin-left: -15px;
    }
    .form-group {
        margin-bottom: 15px;
    }
    .container {
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto;
    }
    .tags_ul {
        padding: 5px;
    }
    #img-tag li {
        float: left;
        margin: 3px;
        display: inline-flex;
        cursor: pointer;
        background-color: transparent;
        padding: 4px 5px;
        font-size: 13px;
        font-weight: 300;
        border: 1px solid #c1c1c1;
    }
    /* :: 10.0 Advanced Search Area */
    .form-control {
        background-color: transparent;
        width: 100%;
        height: 38px;
        border-radius: 0;
        font-size: 14px;
        color: #000000;
        margin-bottom: 30px;
        font-weight: 500;
        padding: 0 15px;
        border: 1px solid #e1dddd; }
    .form-control:hover, .form-control:focus {
        background-color: transparent;
        box-shadow: none;
        border: 1px solid #e1dddd; }

    .south-search-area .advanced-search-form {
        position: relative;
        z-index: 1;
        padding: 30px 50px;
        border: 1px solid #e1dddd;
        box-shadow: 0 5px 30px rgba(0, 0, 0, 0.15); }
    @media only screen and (max-width: 767px) {
        .south-search-area .advanced-search-form {
            padding: 30px 20px; } }
    .south-search-area .advanced-search-form .search-title p {
        margin-bottom: 0;
        color: #ffffff;
        text-transform: uppercase;
        line-height: 45px;
        font-weight: 600; }
</style>

    <style>
        .center-cropped {
            object-fit: fill; /* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 230px;
            width: 230px;
        }
    </style>

<div class="south-search-area box">
                <div class="advanced-search-form">
                    <!-- Search Title -->
                    <h4><b>REFINE YOUR SEARCH</b></h4>
                    <form  id="advanceSearch">
                        <div class="row">

                            <div class="col-12 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <select name="AllCategories" class="form-control" id="AllCategories">

                                        <option selected="selected" value="All">All Categories</option>

                                        <option value="Venues">Venues</option>

                                        <option value="Photographers">Photographers</option>

                                        <option value="Caterers">Caterers</option>

                                        <option value="Car Rentals">Car Rentals</option>

                                        <option value="Decorators">Decorators</option>

                                        <option value="Transport">Transport</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Enter Location Here.."  id="loc" name="loc" required="true" >
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="lat" id="lat">
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="lng" id="lng">
                                </div>

                                <script>
                                    var input = document.getElementById('loc');
                                    var autocomplete = new google.maps.places.Autocomplete(input);

                                    autocomplete.addListener('place_changed', function() {
                                        var place = autocomplete.getPlace();
                                        console.log("Lat :" + place.geometry.location.lat());
                                        console.log("Lng :" + place.geometry.location.lng());
                                        $("#lat").val(place.geometry.location.lat());
                                        $("#lng").val(place.geometry.location.lng());
                                    });
                                </script>
                            </div>

                            <div class="col-12 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <select name="Distance" class="form-control" id="Distance">
                                        <option value="0-1000">Distance(All)</option>
                                        <option value="0-10">0-10 KM</option>
                                        <option value="10-25">10-25 KM</option>
                                        <option value="25-40">25-40 KM</option>
                                        <option value="40-55">40-55 KM</option>
                                        <option value="55-70">55-70 KM</option>
                                        <option value="70-1000">70+ KM</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-12 col-md-4 col-lg-3">
                                <div class="form-group">
                                    <select class="form-control" name="Price" id="Price">
                                        <option value="0-1000000">Price(All)</option>
                                        <option value="0-600">0-600</option>
                                        <option value="600-900">600-900</option>
                                        <option value="900-1300">900-1300</option>
                                        <option value="1300-1000000">1300+</option>
                                    </select>
                                </div>
                            </div>
                            <!-- Submit -->
                                <div class="form-group mb-0">
                                    <a id="btnsearch" type="submit" class="btn btn-default" style="float: right;">Search</a>
                                </div>
                            </div>
                    </form>
                </div>
</div>
<!-- ##### Advance Search Area End ##### -->
<br>

<div class="row">
    <!-- Left col -->
    <div class="col-md-12">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="row " style="margin-left: auto">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                            <h4 id="heading">Featured Services </h4>

                    </div>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="adds-wrapper noSideBar" id="datahtml">
                    <?php
                    $empty = false;
                    ?>
                    @if(empty($services_result))
                        <?php
                        $empty = true;
                        ?>
                        <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                            <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                            <h4 style="margin:15px">Search Not Available</h4>
                        </div>
                    @endif

                    @if(!empty($services_result))
                        <?php
                        $services = $services_result;
                        ?>
                    @endif

                    @if(!empty($services_result)&&sizeof($services)>0)
                    @foreach($services as $service)
                        <div class="item-list make-grid">

                            <div class="col-md-4 no-padding photobox">
                                <div class="add-image">
                                    <a href="{{url('/viewservice/'.$service->id)}}">
                                        <img class="thumbnail no-margin center-cropped" src="{{asset($service->picture)}}" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-7 add-desc-box">
                                <div class="add-details">
                                    <h5 class="add-title">
                                        <a href="#">{{$service->title}} </a>
                                    </h5>
                                    <span class="info-link">
                                            <i class="fas fa-map-marker-alt">{{$service->location_name}}</i>
                                        </span>
                                    <span class="info-row">
                                            <span class="date">  </span><br>
                                        </span>
                                    <div class="btn-block"
                                            style="margin-top: 10px; color:  #00a65a">
                                        <span  style="float: left">Venues</span>
                                        <span  style="float: right">{{$service->price_per_person}}/<small>Per Person</small></span>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @endforeach
                        @elseif(!$empty)
                            <div style="margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block">
                                <img src="{{asset("/images/Logo.png")}}" height="150px" width="150px" style="margin-left: auto; margin-right: auto;display: block; opacity: 0.5">
                                <h4 style="margin:15px; text-align: center">No services</h4>
                            </div>
                        @endif
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
    <script>
        $('#btnsearch').click(function () {
            if($('#loc').val()==""){
                alert("Please enter location");
            }else{
                var form = document.getElementById('advanceSearch');
                var formData = $(form).serialize();
                $.ajax({
                    method : 'GET',
                    data: formData,
                    url: '/AdvancedSearch',
                    success: function (data) {
                        $('#heading').html('Services');
                        $('#datahtml').html(data);
                    }
                });

            }
        });
    </script>
@stop
