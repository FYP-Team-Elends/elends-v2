@extends('adminlte::page')
@section('title', 'Edit Item')

@section('content_header')
    <h1>Edit Here</h1>
@stop
@section('content')
    <style>

        .custom_style {
            border: 1px solid #dddddd;
            border-radius: 4px;
            padding: 5px;
            margin: 5px;
            width: 100px;
            height: 100px;
            float: left;
        }

        img:hover {
            box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
        }
    </style>

    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">

            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-body no-padding">
                    <form role="form" method="get">
                        <div class="box-body">
                            <div class="form-group">
                                <input type="hidden" class="form-control" name="id" required>
                                <label for="exampleInputEmail1">Edit Title</label>
                                <input type="text" class="form-control" name="title" id="exampleInputEmail1" placeholder="Enter Title" required onemptied="this.setCustomValidity('Enter Title')">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Edit Rent Per Day</label>
                                <input type="number" class="form-control" min="0" name="rent" id="exampleInputEmail1" placeholder="Enter Rent"  required onemptied="this.setCustomValidity('Enter Rent')">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Edit Description</label>
                                <textarea class="form-control" name="description" required onemptied="this.setCustomValidity('Enter Description')">

                                </textarea>
                            </div>

                            <div >
                                    <div class="custom_style">
                                        <input  type="button" id = "x" style="float: right" value="x">
                                        <img src="p2.jpg" alt="image">
                                    </div>
                                <div class="custom_style">
                                    <input  type="button" id = "x" style="float: right" value="x">
                                    <p>Add Image Here</p>
                                </div>
                                <div class="custom_style">
                                    <input  type="button" id = "x" style="float: right" value="x">
                                    <p>Add Image Here</p>
                                </div>
                                <div class="custom_style">
                                    <input  type="button" id = "x" style="float: right" value="x">
                                    <p>Add Image Here</p>
                                </div>


                            </div>

                        </div>

                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
@stop

@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
@stop