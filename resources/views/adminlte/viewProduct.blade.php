@extends('adminlte::page')
@section('title', 'Product')

@section('content')
    <style>
        .center-cropped {
            object-fit: none;/* Do not scale the image */
            object-position: center; /* Center the image within the element */
            height: 400px;
            width: 800px;
        }
        #map {
            height: 300px;  /* The height is 400 pixels */
            width: 100%;  /* The width is the width of the web page */
        }
    </style>
    <div id="sendRequest" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Send Booking Request</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body no-padding">
                        <form role="form" action="{{url('bookItem/')}}" method="POST">
                            @csrf

                            <div class="form-group">
                                <input name="item_id" value="{{$ad->item->id}}" type="hidden">
                                <input name="ad_id" value="{{$ad->id}}" type="hidden">
                                <label> Message</label>
                                <textarea class="form-control" placeholder="Enter message here..." name="msg"></textarea>
                            </div>
                            <!-- /.box-body -->

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-success" style="float: right">Send Request</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 20px">Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="main-container">
        <div class="col-md-12" id="message">
            @if(!Auth::User()->picture || !Auth::User()->city)
                <div style="padding: 10px; background: rgb(243, 156, 18); z-index: 999999; font-size: 16px; font-weight: 600;">
                    <span style="color: rgba(255, 255, 255, 0.9); display: inline-block; margin-right: 10px; text-decoration: none;">Your Profile is incomplete, Please complete it</span>
                    <a href="{{url('AdminLte/myProfie')}}">click to view profile</a>
                </div>
            @endif
            <br>
        </div>
        <div class="row">
            <div class="col-sm-9 page-content col-thin-right">
                <div class="inner inner-box ads-details-wrapper">
                    <h2 class="enable-long-words">
                        <strong>
                            <a href="#">
                                {{$ad->title}}
                            </a>
                        </strong>
                    </h2>
                    <span class="info-row">
							<span class="category">{{$ad->item->category->category}}</span> -&nbsp;
							<span class="item-location"><i class="fa fa-map-marker"></i> {{$ad->item->user->city}} </span>
						</span>

                    <div class="ads-image">
                        <h1 class="pricetag">
                            Rs. {{$ad->rent}}/Day
                        </h1>

                        <div id="myCarousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <?php $i = 0;?>
                                @foreach($ad->item->pictures as $picture)
                                    @if($i==0)
                                        <div class="item active">
                                            <img style="width:100%; height: 500px;" src="{{"../".$picture->path}}" alt="Los Angeles">
                                        </div>
                                    @else
                                        <div class="item">
                                            <img style="width:100%; ; height: 500px" src="{{"../".$picture->path}}" alt="Los Angeles">
                                        </div>
                                    @endif
                                    <?php $i++;?>
                                @endforeach
                            </div>

                            <!-- Left and right controls -->
                            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                    <!--ads-image-->

                    <div class="ads-details" style="margin-top: 20px; padding: 20px">
                        <div class="row" style="margin-left: 10px; margin-right: 10px">
                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                <div>
                                    <span>Color: </span>
                                </div>
                            </div>
                            <!-- Price / Salary -->
                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                <div>
                                    <p>{{$ad->item->color}}</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-left: 10px; margin-right: 10px">
                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                <div>
                                    <span>Size: </span>
                                </div>
                            </div>
                            <!-- Price / Salary -->
                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                <div>
                                    <p>{{$ad->item->size}}</p>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" style="margin-left: 10px; margin-right: 10px">
                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                <div>
                                    <span>Location: </span>
                                </div>
                            </div>

                            <div class="detail-line-lite col-md-6 col-sm-6 col-xs-6">
                                <div>
                                    <span><a href="{{'/AdminLte/browseItem?Events=All&lat'.$ad->item->user->lat."&lng=".$ad->item->user->lng}}">{{$ad->item->user->city}}</a></span>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <!-- Price / Salary -->
                    </div>

                    <div class="ads-details">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab-details" data-toggle="tab"><h4>Description</h4></a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-details">
                                <div class="row" style="padding: 10px;">
                                    <div class="ads-details-info col-md-12 col-sm-12 col-xs-12 enable-long-words from-wysiwyg">


                                        <!-- Description -->
                                        <div class="detail-line-content">
                                            <p>{{$ad->description}}</p>
                                        </div>

                                        <!-- Custom Fields -->

                                        <!-- Tags -->
                                        <div style="clear: both;"></div>

                                        <!-- Actions -->
                                    </div>

                                    <br>&nbsp;<br>
                                </div>
                            </div>


                        </div>
                        <!-- /.tab content -->

                    </div>

                </div>
                <!--/.ads-details-wrapper-->
            </div>
            <div class="col-sm-3 page-sidebar-right">
                <aside>
                    @if($ad->item->user->id == \Illuminate\Support\Facades\Auth::id())
                        <div class="panel sidebar-panel panel-contact-seller">
                            <div class="panel-heading">Manage</div>
                            <div class="panel-content user-info">
                                <div class="panel-body text-center">
                                    <div class="seller-info">
                                        <h3 class="no-margin">{{$ad->item->user->first_name}}</h3>
                                        <p>
                                            Location:&nbsp;
                                            <strong>
                                                <a href="{{'/AdminLte/browseItem?Events=All&lat'.$ad->item->user->lat."&lng=".$ad->item->user->lng}}">
                                                    {{$ad->item->user->city}}
                                                </a>
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="user-ads-action">
                                        <a class="btn btn-default btn-block" href="{{url('/editAd/'.$ad->id)}}">
                                            <i class="far fa-pencil-square"></i> Edit Ad
                                        </a>

                                        <form method="GET" action="{{url('/deleteAd/'.$ad->id)}}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            = <input name="submit" class="btn btn-danger btn-block" value="Delete Ad" type="submit">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="panel sidebar-panel panel-contact-seller">
                            <div class="panel-heading">Contact Advertiser</div>
                            <div class="panel-content user-info">
                                <div class="panel-body text-center">
                                    <div class="seller-info">
                                        <h3 class="no-margin">{{$ad->item->user->first_name}}</h3>
                                        <p>
                                            Location:&nbsp;
                                            <strong>
                                                <a href="{{'/AdminLte/browseItem?Events=All&lat'.$ad->item->user->lat."&lng=".$ad->item->user->lng}}">
                                                    {{$ad->item->user->city}}
                                                </a>
                                            </strong>
                                        </p>
                                    </div>
                                    <div class="user-ads-action">
                                        <a class="btn btn-default btn-block">
                                            <i class="far fa-envelope"></i> Send a message
                                        </a>
                                        <a class="btn btn-default btn-block">
                                            <i class="far fa-heart"></i> Add to wishlist
                                        </a>
                                        <a class="btn btn-success btn-block showphone">
                                            <i class="fas fa-phone"></i>
                                            <img src="data:image/png;charset=utf-8;base64,iVBORw0KGgoAAAANSUhEUgAAAGsAAAAMCAYAAABlTkPuAAAACXBIWXMAAA7EAAAOxAGVKw4bAAADnElEQVRYw62YT2hVRxTGv3uRUESkBFtKkSASSnh0JV1IyKKEICJBgrgIUkSCSCrBZiEipZsQXIgUkSIiUroQkVJEQVSkC3EhXYikIiJFixRrqUjwTytJquHn5nswDPPunVfvwOVlzvlmvnPPnHvOmUgZA3gPOAo8AV4BPwOfBPpR4CawAPwO7InW7wJuA0vAQ2C6A08v8FFCfoL0+MH6+wnd3ab4M31QywFM2dYFYA4Y+z+YusP6EZgHdgNbgVvesPR8GfgWGPEvwLjXfua1B4BNwGHrdyZ4ZoHHQBnJ1wODwTNspxwMDutshGk1xZ/hg1oOYMJ++saYk54PdYOpO6iPvWBvIOu3MdsccWPRmjngWjBfHelvApciWY+j9lCGTV8Ar9tfgZ12pAL/Tvx1PsjhAK4DVyLMH8CJXEwcweeBs5Gt/cb92hYURfFA0ktJraIoFouiuBCt+VPSmgD/MtKXkv6NZOOSeiUdz4ihLyVdLori75yAa4C/0geZHKWkxQizKOm/XEwZRFWP52Uwl6Tn/v0wONRS0htJH3TwT0vSg1SEA/skDUg6Gqm/knShKIq/ar6qlqRBSSfV5XgH/mwfVHCckzTq2lYCWyWtlXQqG2NharwODLjvHN0P9Dl/LwMzCYcMe/32DvKFRAMyZN1QUB9Od3D4MeBRWFds329OYwsu/uua5M/xQRWH9aesf2zMcNcYYIOfa8DV9jzSP/ImS8CMa8ZEomO6B9xIGPE+sBHYZyMOBLqfgLlgfjF1WN7/GTAbyb92cR8Cxm3rrSb5c3xQw7EFeOoGZcod49PIz7WYuprV1q0ABmzQqI3uS0TFsziqO7Tir/z3Wr/0ZDv9utCeCVJxe91OR3Nfzf4Ttm+gYf5aH3TgWA28CJsXYKVb/du5mOzDCjBrnBLORfJJv/RIRu046BftBXZQPXqCdTeAqxn7j3jtcJP8dT6o4Gin2c0RZsbylTmYbgv0mFvJO+HlEfjcB7WnQ+panwiKJ0Hq2BA9d4BLUYr4NGyVo/1WJdLisr+aRvirfJDB0bLtkxHme+Af9w21mNxD2hjcqo+EdwpfWOeBK9GldNBp45Dz7t7o0jxdwfdLomYcdgOxIpJvsXy/L5L7/R+G09Y3xV/lg1oONz3zAWY20aDUYnIvxrvji19QR5LdpA+rx6QPXZjvxdGT6awyjt5AN+1ucMlRP9s+1Ab5q3xQywGsAr5zl7cE3AWmusG8BQTbzo62PAR1AAAAAElFTkSuQmCC">
                                        </a>

                                        <a class="btn btn-success btn-block" data-toggle="modal" data-target="#sendRequest">Book Ad</a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel sidebar-panel panel-contact-seller">
                            <div class="panel-heading">Location on map</div>
                            <div class="panel-content user-info">
                                <div class="panel-body text-center">
                                    <?php
                                    echo "<script>
                                // Initialize and add the map
                                function initMap() {
                                    // The location of Uluru
                                    var uluru = {lat:".$ad->user->lat." , lng:". $ad->user->lng."};
                                    // The map, centered at Uluru
                                    var map = new google.maps.Map(
                                        document.getElementById('map'), {zoom: 15, center: uluru});
                                    // The marker, positioned at Uluru
                                    var marker = new google.maps.Marker({position: uluru, map: map});
                                }
                            </script>";
                                    ?>
                                    <div id="map"></div>
                                </div>
                            </div>
                        </div>

                    @endif
                </aside>
            </div>
        </div>
    </div>
@stop
@section('adminlte_js')
    <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script src="{{asset('vendor/bower_components/chart.js/Chart.js')}}"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnn7IlopznAGZ4LYnBmtwAHOSdLSkk5TU&callback=initMap">
    </script>
@stop
