<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Resale Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <title>Home</title>
    <link href="{{asset('csss/style.css')}}" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="{{asset('csss/bootstrap-select.css')}}"><!-- bootstrap-select-CSS -->
    <link href="{{asset('csss/style.css')}}" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="{{asset('csss/flexslider.css')}}" type="text/css" media="screen" /><!-- flexslider-CSS -->
    <link rel="stylesheet" href="{{asset('csss/font-awesome.min.css')}}" /><!-- fontawesome-CSS -->
    <link rel="stylesheet" href="{{asset('css/app1.css')}}" />

    <link rel="stylesheet" href="{{asset('csss/menu_sideslide.css')}}" type="text/css" media="all"><!-- Navigation-CSS -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">


    <!-- meta tags -->

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->

    <!--fonts-->
    <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--//fonts-->

    <!-- js -->
    <script type="text/javascript" src="jss/jquery.min.js"></script>
    <!-- js -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="jss/bootstrap.js"></script>
    <script src="jss/bootstrap-select.js"></script>


    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}" />
    <link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}" />
    <style>
        .ad{
            position: relative;
            margin: 0;
            font-size: 17px;
            font-weight: normal;
            line-height: 15px;
            padding: 13px 21px;
            -webkit-border-radius: 3px;
            border-radius: 3px;
            background: #F8694A;
            color: #fff;
            font-weight: bold;
            font-weight: 700;
        }
    </style>
</head>
<body>

<!-- HEADER -->
<header>
    <!-- header -->
    <div id="header" style="background: #F5F5F5; box-shadow: 0px 0px 8px 2px #c9c9c9;
      background-position: center center;
      background-repeat: no-repeat;">
        <div class="container">
            <div class="pull-left">
                <!-- Logo -->
                <div class="header-logo" style="float: left;">
                    <a class="logo" href="#">
                        <img src="{{asset('img/logo.png')}}" alt="">
                    </a>
                </div>
                <!-- /Logo -->

                <!-- Search -->
                <div class="header-search">
                    <form>
                        <input class="input search-input" type="text" placeholder="Enter your keyword">
                        <select class="input search-categories">
                            <option value="0">Select Event</option>
                            <option value="1">Mehandi</option>
                            <option value="1">Barat</option>
                            <option value="1">Nikha</option>
                            <option value="1">Valima</option>
                            <option value="1">Party</option>
                            <option value="1">Birthday</option>
                        </select>
                        <button class="search-btn"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <!-- /Search -->
            </div>
            <div class="top-right header-btns">
                    <!-- Account -->
                @if (Route::has('login'))
                    <div class="links">
                        @auth
                            <a href="{{ url('/home') }}">Home</a>
                <li class="header-cart dropdown default-dropdown">
                        @else
                            <a href="{{ route('login') }}">Login</a>
                </li>
                <!-- Account -->
                <li class="header-account dropdown default-dropdown">
                    <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">
                            <i class="fa fa-user-o"></i>
                        </div>
                        <strong class="text-uppercase">My Account <i class="fa fa-caret-down"></i></strong>
                    </div>
                        <ul class="custom-menu">
                            <li><a href="#"><i class="fa fa-user-o"></i> My Account</a></li>
                            <li><a href="{{url('fav_ads')}}"><i class="fa fa-heart-o"></i> My Wishlist</a></li>
                            <li><a href="{{url('my_ads')}}"><i class="fa fa-exchange"></i> My Ads</a></li>
                            </li>
                        </ul>
                    @endauth
                    @endif
                </li>
                <!-- /Account -->

                <!-- Cart -->

                <!-- Cart -->

                <div class=" position-ref full-height">
                    @if (Route::has('login'))
                        <div class="links">
                            @auth
                                <a href="{{ url('/home') }}">Home</a>
                            @else
                                <a href="{{ route('login') }}">Login</a>
                                <button class="btn btn-box dropdown-toggle links" type="button" data-toggle="dropdown" >Register as
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('register') }}">User</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('/register/ServiceProvider')}}">Service Provider</a></li>
                                </ul>

                            @endauth
                        </div>
                    @endif

                </div>
            </div>
                       {{-- <small>Welcome, {{Auth::user()->first_name}}</small>
                        @else
                        <a href="{{ route('login') }}" class="text-uppercase">Login</a> / <a href="{{ route('register') }}" class="text-uppercase">Join</a>
                        @endif--}}
                    <!-- /Account -->
            </div>
        </div>
        <!-- header -->
    </div>
    <!-- Containr -->
</header>

<!-- HEADER -->
@yield('content')
<footer>
    <div class="agileits-footer-bottom text-center">
        <div class="container_footer">
            <div class="w3-footer-logo">
                <h1><a href="#"><span>E-</span>Lends</a></h1>
            </div>
            <div class="w3-footer-social-icons">
                <ul>
                    <li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a></li>
                    <li><a class="flickr" href="#"><i class="fa fa-flickr" aria-hidden="true"></i><span>Flickr</span></a></li>
                    <li><a class="googleplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i><span>Google+</span></a></li>
                    <li><a class="dribbble" href="#"><i class="fa fa-dribbble" aria-hidden="true"></i><span>Dribbble</span></a></li>
                </ul>
            </div>
            <div class="col-md-8 col-md-offset-2 text-center">
                <!-- footer copyright -->
                <div class="footer-copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | We <i class="fa fa-heart-o" style="color: red" aria-hidden="true"></i> to make you happy</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </div>
                <!-- /footer copyright -->
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</footer>
<!--footer section end-->

<!-- jQuery Plugins -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/nouislider.min.js"></script>
<script src="js/jquery.zoom.min.js"></script>
<script src="js/main.js"></script>

<!-- Navigation-Js-->
<script type="text/javascript" src="jss/main.js"></script>
<script type="text/javascript" src="jss/classie.js"></script>
<!-- //Navigation-Js-->
<!-- js -->
<script type="text/javascript" src="jss/jquery.min.js"></script>
<!-- js -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="jss/bootstrap.js"></script>
<script src="jss/bootstrap-select.js"></script>
<script>
    $(document).ready(function () {
        var mySelect = $('#first-disabled2');

        $('#special').on('click', function () {
            mySelect.find('option:selected').prop('disabled', true);
            mySelect.selectpicker('refresh');
        });

        $('#special2').on('click', function () {
            mySelect.find('option:disabled').prop('disabled', false);
            mySelect.selectpicker('refresh');
        });

        $('#basic2').selectpicker({
            liveSearch: true,
            maxOptions: 1
        });
    });
</script>
<!-- Slider-JavaScript -->
<script src="jss/responsiveslides.min.js"></script>
<script>
    $(function () {
        $("#slider").responsiveSlides({
            auto: true,
            pager: false,
            nav: true,
            speed: 500,
            maxwidth: 800,
            namespace: "large-btns"
        });

    });
</script>
<!-- //Slider-JavaScript -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="jss/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
<!-- //here ends scrolling icon -->
</body>
<!-- Navigation-JavaScript -->
<script src="jss/classie.js"></script>
<script src="jss/main.js"></script>
<!-- //Navigation-JavaScript -->
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="jss/easing.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- start-smoth-scrolling -->
</html>
