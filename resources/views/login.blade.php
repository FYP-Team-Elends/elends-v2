<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <!-- Login.css -->
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="cssL/util.css">
    <link rel="stylesheet" type="text/css" href="cssL/main.css">
    <!--===============================================================================================-->
    <!-- login.css -->


    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/bootstrap-select.css"><!-- bootstrap-select-CSS -->
    <link href="csss/style.css" rel="stylesheet" type="text/css" media="all" /><!-- style.css -->
    <link rel="stylesheet" href="csss/font-awesome.min.css" /><!-- fontawesome-CSS -->
    <link rel="stylesheet" href="csss/menu_sideslide.css" type="text/css" media="all"><!-- Navigation-CSS -->
    <!--fonts-->
    <link href='//fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <!--//fonts-->
    <!-- js -->
    <!-- js -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="jss/bootstrap.js"></script>
    <script src="jss/bootstrap-select.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

    <!-- Bootstrap -->
    <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css" />

    <!-- Slick -->
    <link type="text/css" rel="stylesheet" href="css/slick.css" />
    <link type="text/css" rel="stylesheet" href="css/slick-theme.css" />

    <!-- nouislider -->
    <link type="text/css" rel="stylesheet" href="css/nouislider.min.css" />

    <!-- Font Awesome Icon -->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!-- Custom stlylesheet -->
    <link type="text/css" rel="stylesheet" href="css/style.css" />
</head>
<body>
<!-- header -->
<div id="header">
    <!-- top Header -->
    
    <!-- /top Header -->
    <div class="container">
        <div class="pull-left">
            <!-- Logo -->
            <div class="header-logo" style="float: left;">
                <a class="logo" href="#">
                    <img src="./img/logo.png" alt="">
                </a>
            </div>
            <!-- /Logo -->

            <!-- Search -->
            <div class="header-search">
                <form>
                    <input class="input search-input" type="text" placeholder="Enter your keyword">
                    <select class="input search-categories">
                        <option value="0">Select Event</option>
                        <option value="1">Mehandi</option>
                        <option value="1">Barat</option>
                        <option value="1">Nikha</option>
                        <option value="1">Valima</option>
                        <option value="1">Party</option>
                        <option value="1">Birthday</option>
                        <option value="1">Bridal Shower</option>
                    </select>
                    <button class="search-btn"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <!-- /Search -->

        </div>
    </div>
</div>

<!-- header -->
<!-- container -->

<div class="container">

        <div class="agileinfo_signin">
            <h3>Login</h3>
            <form action="#" method="post">
                <input type="email" name="Your Email" placeholder="Your Email" required="">
                <input type="password" name="Password" placeholder="Password" required="">
                <input type="submit" value="Login">
                <div class="forgot-grid">
                    <label class="checkbox"><input type="checkbox" name="checkbox">Remember me</label>
                    <div class="forgot">
                        <a href="#" data-toggle="modal" data-target="#myModal2">Forgot Password?</a>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal2" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h3 class="w3ls-password">Get Password</h3>
                                        <p class="get-pw">Enter your email address below and we'll send you an email with instructions.</p>
                                        <form action="#" method="post">
                                            <input type="text" class="user" name="email" placeholder="Email" required="">
                                            <input type="submit" value="Submit">
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
<<<<<<< HEAD
                <div class="text-center w-full p-t-42 p-b-22">
						<span class="txt1">
							Or login with
						</span>
                </div>
             <div class="login with row">
                <a href="{{url('login/facebook')}}" class="btn-face m-b-10 col-md-5 col-lg-5 fa fa-facebook-official">
                    Facebook
                </a>
                 <p class="col-md-2 col-lg-2 m-t-5"> OR </p>
                <a href="#" class="btn-google m-b-10 col-md-5 col-lg-5">
                    <img src="images/icons/icon-google.png" alt="GOOGLE">
                    Google
                </a>
             </div>
=======
                
>>>>>>> a37a0d9dcfa083ffc58788aca6e85a82164b8db8
            </form>
            </form>
            <h6> Not a Member Yet? <a href="signup.blade.php">Sign Up Now</a> </h6>
        </div>
</div>
<!-- //sign in form -->
<!-- /login-->

<!-- FOOTER -->
<footer id="footer" class="section section-grey">
    <!-- container -->
    <div class="container">
        <!-- row -->
        <div class="row">
            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="app">
                    <h3>DOWNLOAD APP</h3>
                    <img src="./img/app.jpg" style="width: 180px">
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Information</h3>
                    <ul class="list-links">
                        <li><a href="#">Location Map</a></li>
                        <li><a href="#">Popular Seraches</a></li>
                        <li><a href="#">term of use</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <div class="clearfix visible-sm visible-xs"></div>

            <!-- footer widget -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">HELP</h3>
                    <ul class="list-links">
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">Sitemap</a></li>
                    </ul>
                </div>
            </div>
            <!-- /footer widget -->

            <!-- footer subscribe -->
            <div class="col-md-3 col-sm-6 col-xs-6">
                <div class="footer">
                    <h3 class="footer-header">Contact Us</h3>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-earphone"></span></li>
                        <li>+0 561 111 235</li>
                    </ul>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-envelope"></span></li>
                        <li><a href="mailto:info@example.com">mail@example.com</a></li>
                    </ul>
                    <div class="clearfix"> </div>
                    <ul class="location">
                        <li><span class="glyphicon glyphicon-briefcase"></span></li>
                        <li><strong>Business Packages</strong></li>
                        <li><small>(featured ads, advertising)</small></li><br>
                        <li><a href="#">click here</a></li>
                    </ul>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- /footer subscribe -->
        </div>
        <!-- /row -->
        <!-- /row -->
    </div>
    <!-- /container -->
</footer>
<!--footer section start-->
<footer>
    <div class="agileits-footer-bottom text-center">
        <div class="container">
            <div class="w3-footer-logo">
                <h1><a href="index.blade.php"><span>E-</span>Lend</a></h1>
            </div>
            <div class="w3-footer-social-icons">
                <ul>
                    <li><a class="facebook" href="#"><i class="fa fa-facebook" aria-hidden="true"></i><span>Facebook</span></a></li>
                    <li><a class="twitter" href="#"><i class="fa fa-twitter" aria-hidden="true"></i><span>Twitter</span></a></li>
                    <li><a class="flickr" href="#"><i class="fa fa-flickr" aria-hidden="true"></i><span>Flickr</span></a></li>
                    <li><a class="googleplus" href="#"><i class="fa fa-google-plus" aria-hidden="true"></i><span>Google+</span></a></li>
                    <li><a class="dribbble" href="#"><i class="fa fa-dribbble" aria-hidden="true"></i><span>Dribbble</span></a></li>
                </ul>
            </div>
            <div class="col-md-8 col-md-offset-2 text-center">
                <!-- footer copyright -->
                <div class="footer-copyright">
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | We <i class="fa fa-heart-o" style="color: red" aria-hidden="true"></i> to make you happy</a>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                </div>
                <!-- /footer copyright -->
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</footer>
<!--footer section end-->

<!-- jQuery Plugins -->
<!-- start-smoth-scrolling -->
</html>