<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Home

Route::get('/','WelcomeController@welcome');

Route::get('/index',function (){return view('index'); });

//Dashboard


Route::get('/DashBoard','DashboardController@index');

//Profile

Route::get('/AdminLte/myProfie','Profile\MyProfileController@index');

Route::post('/update_info','Profile\MyProfileController@update_info');

Route::post('/update_detail','Profile\MyProfileController@update_detail');

Route::post('/update_password','Profile\MyProfileController@update_password');

//Notification

Route::get('/notifications','NotificationController@getAllNotifications');

Route::get('/readNotification', 'NotificationController@redirectToAction');

Route::get('/clearNotifications', 'NotificationController@clearNotifications');

//My Items

Route::get('/AdminLte/myItems/viewItems','ItemController@show_all');

Route::post('/postItem','ItemController@postItem');

Route::get('/AdminLte/myItems/postItem','ItemController@index');

Route::get('/viewitem/{id}','ItemController@show');

Route::get('/editItem/{id}','ItemController@edit');

// Update Items

Route::post('/UpdateItem/{id}','ItemController@update');

// Delete Items

Route::get('/deleteItem/{id}','ItemController@destroy');


//ItemsPictures

Route::post('/AddPictures/{id}','ItemController@AddImages');


//Delete Item Images

Route::get('/deletePicture/{id}','ItemController@deleteImage');

Route::get('/AdminLte/myItems/postItem','ItemController@index');

//Search

Route::get('/AdminLte/browseItem', 'AdController@searchAds');


//My Ads

Route::get('/AdminLte/myAds/viewAds','AdController@show_all');

Route::get('/AdminLte/viewAd/{id}','AdController@show');

Route::post('/submitPost','AdController@submitAd');

Route::get('/deleteAd/{id}','AdController@deleteAd');





//My Wishlist

Route::get('/AdminLte/myWishlist','FavoriteAdController@index');

Route::get('/AdminLte/favad/remove/{id}','FavoriteAdController@removeFavAd');

//Add to Favorites

Route::get('Favorites/add/{id}','FavoriteAdController@addFavorites');


//My Bookings



Route::get('/AdminLte/currentBookings','BookingController@myBookings');

Route::get('/AdminLte/bookingsHistory','BookingController@bookingsHistory');

Route::get('/AdminLte/CurrentOrders','BookingController@currentOrders');

Route::post('myOrders/BookingRequests/Confirm/{id}','BookingController@ConfirmBooking');

Route::post('myOrders/BookingRequests/Cancel/{id}','BookingController@CancelBooking');

Route::get('/AdminLte/myBooking/Cancel/{id}','BookingController@CancelmyBooking');





//My Orders




Route::get('/AdminLte/ordersHistory','BookingController@ordersHistory');

Route::get('/AdminLte/myOrders/CancelOrder/{id}','BookingController@CancelOrder');

Route::get('/AdminLte/myOrders/requestsReceived',function ()
{
    return View('adminlte.requestsReceived');
});


Route::get('/AdminLte/feeds','AdController@feeds');

Route::get('/AdminLte/service','ServiceController@serviceview');

Route::get('/AdminLte/Service/Bookings','ServiceController@bookings');

Route::get('/AdminLte/Service/History','ServiceController@history');

Route::get('/AdminLte/Service/Requests','ServiceController@requests');


//Services Search

Route::get('/AdvancedSearch','ServiceController@AdvancedSearch');




//Home Route

Route::get('/home', 'HomeController@index')->name('home');

//Product

Route::get('/c',function (){
    return view('contactus');
});
Route::get('/viewAd/{id}', 'AdController@viewAd')->name('home');

//viewProduct

Route::get('/viewproduct/{id}',"AdController@viewAd");


//Hall


Route::get('/viewservice/{id}', 'ServiceController@showservice');

Route::post('/servicebooking','ServiceController@servicebooking');

//Route::post('hallbooking','');

Route::get('/HallAd',function (){
    return view('adminlte.HallAds');
});

Route::post('/bookItem','AdController@sendItemBookingRequest');



//chat related to ad

Route::get('/chat/{id?}','ChatController@index');

Route::get('/chat/getmessages/{id}','ChatController@getmessages');

Route::post('/chat/sendmessage/{id}','ChatController@sendmessage');


Route::get('/Chat/chatThread/{id}','ChatController@chat');

//Edit Ad
Route::get('/editAd/{id}', 'AdController@EditAd');

Route::get('/editItem',function (){
    return view('adminlte.EditItem');
});
//update Ad
Route::get('/updateAd', 'AdController@updateAd');



// ServiceProvider Routes

Route::get('/sp','ServiceProvider\SPServices@index');


Route::group(['prefix' => 'ServiceProvider', 'middleware' => ['role:ServiceProvider']], function() {

    Route::get('/services','ServiceProvider\SPServicesController@dashboard');

    Route::get('/myservices','ServiceProvider\SPServicesController@Services');

    Route::get('/viewService/{id}', 'ServiceProvider\SPServicesController@ShowService');

    Route::post('/delete/{id}', 'ServiceProvider\SPServicesController@delete');

    Route::get('/myservicesrequests','ServiceProvider\SPServicesController@ServicesRequests');

    Route::get('/myserviceshistory','ServiceProvider\SPServicesController@Services');

    Route::get('/mycompletedservices','ServiceProvider\SPServicesController@CompletedServices');

    Route::get('/mybookedservices','ServiceProvider\SPServicesController@BookedServices');

    Route::get('/availability','ServiceProvider\SPServicesController@changeAvailability');

    Route::get('/rejectedservices','ServiceProvider\SPServicesController@RejectedServices');

    Route::post('/deleteService/{id}','ServiceProvider\SPServicesController@deleteService');

    Route::post('/addService','ServiceProvider\SPServicesController@addService');

    Route::post('/updateService','ServiceProvider\SPServicesController@updateService');

    Route::post('/BookingRequests','ServiceProvider\SPServicesController@response');

    Route::get('/chats/{id?}','ServiceProvider\SPServicesController@getchats');

    Route::get('/chat/{id}','ServiceProvider\SPServicesController@chat');

});

// Admin Routes

Route::group(['prefix' => 'Admin', 'middleware' => ['role:Admin']], function() {

    Route::get('/DashBoard','Admin\AdminController@index');

    Route::get('/create','Admin\AdminController@create');

    Route::get('/accountRequest/{id}','Admin\AdminController@request');

    Route::get('/request','Admin\AdminController@request');

    Route::get('/users', 'Admin\AdminController@AllUser');

    Route::post('/manageRoles', 'Admin\AdminController@ManageRoles');

    Route::post('/AcceptRequest', 'Admin\AdminController@AcceptRequest');

    Route::post('/RejectRequest', 'Admin\AdminController@RejectRequest');

    Route::post('/approveService', 'Admin\AdminController@approveService');

    Route::get('/serviceRequests', 'Admin\AdminController@serviceRequests');

    Route::get('/report','Admin\AdminController@Reports');

    Route::get('/allitems','Admin\AdminController@TotalItems');

    Route::get('/allads','Admin\AdminController@TotalAds');

    Route::get('/allservices','Admin\AdminController@TotalServices');
});

//Timeline

Route::get('/timeline/{id}', 'TimelineStatusController@getTimeline');
Route::post('/timeline/updateTimeline', 'TimelineStatusController@updateTimeline');


//LoginRoutes

Route::get('login/{service}', 'Auth\LoginController@redirectToProvider');

Route::get('login/{service}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/register/{ServiceProvider}','ServiceProvider\SPServicesController@RegisterAs');

Auth::routes(['verify' => true]);


