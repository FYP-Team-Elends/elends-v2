<?php

namespace ELends\Providers;

use ELends\Event;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Events\Dispatcher;
use JeroenNoten\LaravelAdminLte\Events\BuildingMenu;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services
     *
     * @return void
     */
    public function boot(Dispatcher $events)
    {
        Schema::defaultStringLength(191);



        view()->composer('*', function($view)
        {
            $user = Auth::user();
            session()->put('user',$user);
        });



        $events->listen(BuildingMenu::class, function (BuildingMenu $event) {
            $user = session()->get('user');
            $status = true;
            if ($user->hasRole('Admin')) {
                $status = false;
                /*'Service Provider'*/
                $event->menu->add(
                    [
                        'text' => 'Home',
                        'url' => 'Admin/DashBoard',
                        'icon' => 'home',
                    ],
                    [
                        'text' => 'Service Approval Requests',
                        'url' => '/Admin/serviceRequests',
                        'icon' => 'check',
                    ],
                    [
                        'text' => 'Manage User Roles',
                        'url' => '/Admin/users',
                        'icon' => 'user',
                    ],
                    [
                    'text' => 'All Items',
                    'url' => '/Admin/allitems',
                    'icon' => 'clone',
                    ],

                    [
                    'text' => 'All Ads',
                    'url' => '/Admin/allads',
                    'icon' => 'ad',
                    ],

                    [
                        'text' => 'All Services',
                        'url' => '/Admin/allservices',
                        'icon' => 'car',
                    ]);
            }
            if ($user->hasRole('ServiceProvider')) {
                $status = false;
                /*'Service Provider'*/
                $event->menu->add(
                    [
                        'text' => "Dashboard",
                        'url' => 'ServiceProvider/services',
                        'icon' => 'home'

                    ],
                    [
                        'text' => 'My Profile',
                        'url' => '/AdminLte/myProfie',
                        'icon' => 'user',
                    ],
                    [
                        'text' => 'Messages',
                        'url' => 'ServiceProvider/chats',
                        'icon' => 'envelope',
                    ],
                    [
                        'text' => 'My Services',
                        'url' => 'ServiceProvider/myservices',
                        'icon' => 'car',
                    ],
                    [
                        'text' => 'Booking Requests',
                        'url' => 'ServiceProvider/myservicesrequests',
                        'icon' => 'book',
                    ],
                    [
                        'text' => 'Current Bookings',
                        'url' => 'ServiceProvider/mybookedservices',
                        'icon' => 'book',
                    ],
                    [
                        'text' => 'Bookings History',
                        'url' => 'ServiceProvider/mycompletedservices',
                        'icon' => 'history',
                    ]
//                    ,
//                    [
//                        'text' => 'Rejected Services',
//                        'url' => 'ServiceProvider/rejectedservices',
//                        'icon' => 'history',
//                    ]
                );
                }
                if ($user->hasRole('ServiceUser') || $status)
                {
                $event->menu->add(
                    [
                        'text' => 'Dashboard',
                        'url' => '/DashBoard',
                        'icon' => 'home',
                    ],
                    [
                        'text' => 'My Profile',
                        'url' => '/AdminLte/myProfie',
                        'icon' => 'user',
                    ],
                    [
                        'text' => 'Messages',
                        'url' => '/chat',
                        'icon' => 'envelope',
                    ],
                    /*'Orders',*/
                    [
                        'text' => 'My Orders',
                        'icon' => 'folder',
                        'submenu' => [
                            [
                                'text' => 'Current Orders',
                                'url' => 'AdminLte/CurrentOrders',
                                'icon' => 'circle',
                            ],
                            [
                                'text' => 'Orders History',
                                'url' => 'AdminLte/ordersHistory',
                                'icon' => 'circle',
                            ],
                        ],
                    ],
                    /*'Bookings',*/
                    [
                        'text' => 'My Bookings',
                        'icon' => 'check',
                        'submenu' => [
                            [
                                'text' => 'Current Bookings',
                                'url' => 'AdminLte/currentBookings',
                                'icon' => 'circle',
                            ],
                            [
                                'text' => 'Bookings History',
                                'url' => 'AdminLte/bookingsHistory',
                                'icon' => 'circle',
                            ],
                        ],
                    ],
                    /*'Items',*/
                    [
                        'text' => 'My Items',
                        'icon' => 'list',
                        'submenu' => [
                            [
                                'text' => 'View Items',
                                'url' => '/AdminLte/myItems/viewItems',
                                'icon' => 'circle',

                            ],
                            [
                                'text' => 'Post Item',
                                'url' => '/AdminLte/myItems/postItem',
                                'icon' => 'circle',
                            ],
                        ],
                    ],
//                    [
//                        'text' => 'Feeds',
//                        'url' => '/AdminLte/feeds',
//                        'icon' => 'feed',
//                    ],
                    /*'Ads',*/
                    [
                        'text' => 'My Ads',
                        'icon' => 'ad',
                        'url' => 'AdminLte/myAds/viewAds',
                    ],
                    /*'Wishlist',*/
                    [
                        'text' => 'My Wishlist',
                        'url' => 'AdminLte/myWishlist',
                        'icon' => 'grin-hearts',
                    ],

                    [
                    'text' => 'Services',
                        'icon' => 'car',
                        'submenu' => [
                            [
                                'text' => 'Search Services',
                                'url' => 'AdminLte/service',
                                'icon' => 'circle',
                            ],
                            [
                                'text' => 'Current Bookings',
                                'url' => '/AdminLte/Service/Bookings',
                                'icon' => 'circle',

                            ],
                            [
                                'text' => 'Sent Requests',
                                'url' => '/AdminLte/Service/Requests',
                                'icon' => 'circle',
                                ],
                            [
                                'text' => 'Bookings History',
                                'url' => '/AdminLte/Service/History',
                                'icon' => 'circle',
                            ],
                        ],
                    ]
                );
            }
        });

    }

    /**
     * Register any application services
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
