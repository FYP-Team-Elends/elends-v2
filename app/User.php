<?php

namespace ELends;

use ELends\SocialProvider;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use EntrustUserTrait; // add this trait to your user model

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function favads()
    {
        return $this->hasMany('ELends\FavoriteAd');
    }

    public function socialProviders(){
        return $this->hasMany(SocialProvider::class);
    }
    public function Registeras(){
        return $this.$this->hasOne('Elends\Registeras');
    }

}
