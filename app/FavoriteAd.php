<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class FavoriteAd extends Model
{
	public function user()
    {
    	return $this->belongsTo('ELends\User');
    }
    public function ad()
    {
    	return $this->belongsTo('ELends\Ad');
    }
}
