<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class TimelineStatus extends Model
{
    public static $REQUEST = "REQUEST", $REQUEST_CANCELLED = "REQUEST_CANCELLED", $ACCEPTED="ACCEPTED", $REJECTED = "REJECTED", $DELIVERED = "DELIVERED",
    $RECEIVED = "RECEIVED", $RETURNED="RETURNED", $COMPLETED = "COMPLETED";

    public function booking(){
        return $this->belongsTo('ELends\Booking');
    }

    public static function getIntStatus($i){
        if($i===TimelineStatus::$REQUEST){
            return 0;
        }else if($i===TimelineStatus::$REQUEST_CANCELLED){
            return 1;
        }else if($i===TimelineStatus::$ACCEPTED){
            return 2;
        }else if($i===TimelineStatus::$REJECTED){
            return 3;
        }else if($i===TimelineStatus::$DELIVERED){
            return 4;
        }else if($i===TimelineStatus::$RECEIVED){
            return 5;
        }else if($i===TimelineStatus::$RETURNED){
            return 6;
        }else if($i===TimelineStatus::$COMPLETED){
            return 7;
        }
    }

    public static function getStrStatus($i){
        if($i===0){
            return TimelineStatus::$REQUEST;
        }if($i===1){
            return TimelineStatus::$REQUEST_CANCELLED;
        }else if($i===2){
            return TimelineStatus::$ACCEPTED;
        }else if($i===3){
            return TimelineStatus::$REJECTED;
        }else if($i===4){
            return TimelineStatus::$DELIVERED;
        }else if($i===5){
            return TimelineStatus::$RECEIVED;
        }else if($i===6){
            return TimelineStatus::$RETURNED;
        }else if($i===7){
            return TimelineStatus::$COMPLETED;
        }
    }

}
