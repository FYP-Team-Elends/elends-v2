<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;
use ELends\Category;
use ELends\SubCategory;

class Item extends Model
{

    public function category(){

     return $this->belongsTo('ELends\Category');
    }



    public function ad()
    {
        return $this->hasOne('ELends\Ad', 'item_id', 'id');
    }


    public function user()
    {
        return $this->belongsTo('ELends\User');
    }

    public function pictures(){
        return $this->hasMany('ELends\ItemPictures');
    }

}
