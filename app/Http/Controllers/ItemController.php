<?php

namespace ELends\Http\Controllers;

use ELends\Category;
use ELends\Item;
use ELends\ItemPictures;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;



class ItemController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $category=Category::all();
        return view('adminlte.postItem')->with(compact('category'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request);
    }
    public function show_all()
    {
        $items = Item::where('user_id',Auth::user()->id)->get();
        $items = $items->reverse();
        return view('adminlte.allItems')->with(compact('items'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $item = Item::findorfail($id);
        if (Auth::user()->id == $item->user->id) {
            return view('Adminlte.viewItem')->with(compact('item'));
        }else{
            return abort('404');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Item::findorfail($id);

        if (Auth::user()->id == $item->user->id){
            $category=Category::all();
            //dd($item);
            return view('adminlte.updateItem')->with(compact('item','category'));
        }else{
            return abort(403, 'Oops, You are not authorized to Edit it');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\Item  $item
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'category'=> 'required',
            'description'=> 'required',
        ]);

        $item = Item::findorfail($id);

        if (Auth::user()->id == $item->user->id)
        {
            $item->name = $request->name;
            $item->description = $request->description;
            $item->color = $request->color;
            $item->size = $request->size;
            $item->category_id = $request->category;
            $item->save();

            return redirect()->to('viewitem/'.$item->id);
        }
        else
        {
            return abort(403, 'Oops, You are not authorized to Edit it');
        }
    }

    public function AddImages(Request $request , $id)
    {
        $item = Item::findorfail($id);
        $files = $request->file('photos');

        if ($item && $item->user->id == Auth::user()->id)
        {
            if($files!=null){
                foreach($files as $file){
                    $name = time() .'-'. $file->getClientOriginalName();
                    $file->move('Items-images/items/',$name);
                    $itemPicture = new ItemPictures();
                    $itemPicture->item_id = $item->id;
                    $itemPicture->path = 'Items-images/items/'.$name;
                    $itemPicture->save();
                }
            }
            return redirect()->to('/viewitem/'.$id);
        }else
        {
            return abort(403, 'Oops, You are not authorized to Update it');
        }
    }
    public function deleteImage($id)
    {
        $image = ItemPictures::findorfail($id);
        if ($image->item->user->id == Auth::user()->id) {
            $images = ItemPictures::where('item_id',$image->item->id)->count();
                if ($images > 1){
                    $destinationpath = public_path($image->path);
                    $desimage = File::delete($destinationpath);
                    if ($desimage)
                    {
                        $image->delete();
                        $response = [
                            'success' => true,
                            'message' => 'Image Deleted Successfully'
                        ];
                    } else{
                        $response = [
                            'success' => false,
                            'message' => 'Image Error on deleting from directory'
                        ];
                    }
            } else {
                    $response = [
                        'success' => false,
                        'message' => 'Oops, You cannot delete this image Item must have one image'
                    ];
            }
        }else {
            $response = [
                'success' => false,
                'message' => 'Oops, You are not authorized to Delete it'
            ];
        }
        return response()->json($response);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findorfail($id);

        if (Auth::user()->id == $item->user->id) {
            $item->delete();
            return redirect()->to('AdminLte/myItems/viewItems');
        }else{
            return abort(403, 'Oops, You are not authorized to delete it');
        }
    }

    public function postItem(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'category'=> 'required',
            'description'=> 'required',
            'photos' => 'required',
        ]);

        $item = new Item();
        $item->name = $request->name;
        $item->description = $request->description;
        $item->color = $request->color;
        $item->size = $request->size;

        $item->user_id = Auth::user()->id;
        $item->category_id = $request->category;
        $item->save();

        $files = $request->file('photos');

        foreach($files as $file){
            $name = time() .'-'. $file->getClientOriginalName();
            $file->move('Items-images/items/',$name);
            $itemPicture = new ItemPictures();
            $itemPicture->item_id = $item->id;
            $itemPicture->path = 'Items-images/items/'.$name;
            $itemPicture->save();
        }

        return redirect('AdminLte/myItems/viewItems')->with('success','Item Added Successfully!');
    }
}
