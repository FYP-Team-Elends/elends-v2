<?php

namespace ELends\Http\Controllers;

use ELends\Notification;
use ELends\Service;
use ELends\ServiceCategory;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use ELends\Ad;
use ELends\RecentAd;
use ELends\ServiceBooking;
use Illuminate\Support\Facades\Auth;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\Service  $service
     * @return \Illuminate\Http\Response
     */

    public function servicebooking(Request $request)
    {
        //
        $this->validate($request,[
            'event' => 'required',
            'Function_Date'=> 'required|date',
            'guest'=> 'required|numeric',
            'time' => 'required',
            'Message' => 'required|string',
        ]);

        $booking = new ServiceBooking();
        $booking->service_id = $request->id;
        $booking->user_id = Auth::user()->id;
        $booking->guests = $request->guest;
        $booking->Function_type = $request->event;
        $booking->request_status ="requested";
        $booking->message = $request->Message;
        $booking->save();

        $notification = new Notification();
        $notification ->title = $booking->user->first_name." sent you a booking request";
        $notification ->action = "/ServiceProvider/myservicesrequests";
        $notification ->user_id = $booking->service->user_id;
        $notification ->isSeen = false;
        $notification ->isDeleted = false;
        $notification ->save();

        return redirect('AdminLte/Service/Requests')->with('success','Booking Request send successfully!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //

    }
    public function showservice($id){
        $service = Service::find($id);
        return view('adminlte.viewService')->with(compact('service'));
        //return view('adminlte.viewService')->with(compact('service '));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }

    public function serviceview(){
        $services_result = Service::where('isFeatured', true)->get();
        return view('adminlte.allServices')->with(compact('services_result'));
    }

    public function AdvancedSearch(Request $request)
    {
        $services = null;
        $datahtml = '';
        $AllCategories = $request->get('AllCategories');
        $lat = $request->get('lat');
        $lng = $request->get('lng');


       $Distance = explode('-',$request->get('Distance'));

        $minDist = $Distance[0];
        $maxDist = $Distance[1];

        $Price = explode('-',$request->get('Price'));

        if ($AllCategories == "All")
        {
            $services = Service::whereBetween('price_per_person',[$Price[0],$Price[1]])->where('isActive', 1)->where('isApproved', 1)->get();
        }
        else
        {
            $servicesCategories = ServiceCategory::where('name', $AllCategories)->pluck('id');
            $services = Service::whereIn('service_category_id',$servicesCategories)->whereBetween('price_per_person',[$Price[0],$Price[1]])
                ->where('isActive', 1)->where('isApproved', 1)->get();
        }

        $b = false;

         if ($services->count()>0) {
             foreach ($services as $service) {
                 $calculatedDist = ServiceController::computeDistance($lat, $lng, $service->lat, $service->lng);
                 if($calculatedDist>=$minDist&&$calculatedDist<=$maxDist){

                     $b = true;
                     $datahtml = $datahtml."<div class=\"item-list make-grid\">

                             <div class=\"col-md-4 no-padding photobox\">
                                 <div class=\"add-image\">
                                     <a href=\"../viewservice/$service->id\">
                                         <img class=\"thumbnail no-margin center-cropped\" src=".asset($service->picture)." alt=\"\">
                                     </a>
                                 </div>
                             </div>
                             <div class=\"col-md-7 add-desc-box\">
                                 <div class=\"add-details\">
                                     <h5 class=\"add-title\">
                                         <a href=\"../viewservice/$service->id\">$service->title</a>
                                     </h5>
                                     <span class=\"info-link\">
                                             <i class=\"\">$service->location_name</i>
                                         </span>
                                     <span class=\"info-row\">
                                             <span class=\"date\">".round($calculatedDist). " KM</span><br>
                                         </span>
                                     <div class=\"btn-block\"
                                             style=\"margin-top: 10px; color:  #00a65a\">
                                         <span  style=\"float: left\">".$service->service_category->name."</span>
                                         <span  style=\"float: right\">$service->price_per_person/<small>Per Person</small></span>
                                     </div>
                                 </div>
                             </div>

                         </div>";

                 }
             }
         }
         else
         {
             $b=true;
             $datahtml = $datahtml."<div style=\"margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block\">
                                    <img src=\"http://localhost:8000/images/Logo.png\" height=\"150px\" width=\"150px\" style=\"margin-left: auto; margin-right: auto;display: block; opacity: 0.5\">
                                    <h4 style=\"margin:15px; text-align: center\">No services found</h4>
                                </div>";

         }

         if(!$b){
             $datahtml = $datahtml."<div style=\"margin: 20px; width: 250px ; margin-left: auto; margin-right: auto; display: block\">
                                    <img src=\"http://localhost:8000/images/Logo.png\" height=\"150px\" width=\"150px\" style=\"margin-left: auto; margin-right: auto;display: block; opacity: 0.5\">
                                    <h4 style=\"margin:15px; text-align: center\">No services found</h4>
                                </div>";
         }

        return $datahtml;
    }

    public static function computeDistance($lat1, $lng1, $lat2, $lng2, $radius = 6378137)
    {
        static $x = M_PI / 180;
        $lat1 *= $x; $lng1 *= $x;
        $lat2 *= $x; $lng2 *= $x;
        $distance = 2 * asin(sqrt(pow(sin(($lat1 - $lat2) / 2), 2) + cos($lat1) * cos($lat2) * pow(sin(($lng1 - $lng2) / 2), 2)));
        return ($distance * $radius)/1000;
    }

    public function bookings(){
        $mybookings = ServiceBooking::where('user_id',Auth::id())->where('request_status','accepted')->get();
        $title = "Current Bookings";
        return view('adminlte.currentServiceBookings')->with(compact('mybookings'))->with(compact('title'));
    }

    public function history(){
        $mybookings = ServiceBooking::where('user_id',Auth::id())->where('request_status','completed')->orWhere('request_status','rejected')->get();
        $title = "Current Bookings";
        return view('adminlte.serviceBookingsHistory')->with(compact('mybookings'))->with(compact('title'));
    }

    public function requests(){
        $mybookings = ServiceBooking::where('user_id',Auth::id())->where('request_status','requested')->get();
        $title = "Current Bookings";
        return view('adminlte.serviceBookingRequests')->with(compact('mybookings'))->with(compact('title'));
    }

}
