<?php

namespace ELends\Http\Controllers;

use ELends\Booking;
use ELends\Notification;
use ELends\TimelineStatus;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Illuminate\View\View;

class TimelineStatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\TimelineStatus  $timelineStatus
     * @return \Illuminate\Http\Response
     */
    public function show(TimelineStatus $timelineStatus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\TimelineStatus  $timelineStatus
     * @return \Illuminate\Http\Response
     */
    public function edit(TimelineStatus $timelineStatus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\TimelineStatus  $timelineStatus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TimelineStatus $timelineStatus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\TimelineStatus  $timelineStatus
     * @return \Illuminate\Http\Response
     */
    public function destroy(TimelineStatus $timelineStatus)
    {
        //
    }

    public function getTimeline($id){
        $statuses = TimelineStatus::where('booking_id',$id)->get();
        return view('adminlte.Timeline')->with(compact('statuses'));
    }

    public function updateTimeline(Request $request){
        $statusStr = $request->status;
        $bookingId = $request->bookingId;
        $message = $request->msg;

        $statusInt = TimelineStatus::getIntStatus($statusStr);

        $timeline = new TimelineStatus();
        $timeline->booking_id = $bookingId;
        $timeline->status = $statusStr;
        $timeline->message = $message;
        $timeline->save();

        $booking = Booking::find($bookingId);
        $booking->timeline_status = $statusInt;
        $booking->save();

        if($statusStr === TimelineStatus::$REQUEST){
            $notification = new Notification();
            $notification ->title = "You have received a booking request.";
            $notification ->action = "/AdminLte/CurrentOrders";
            $notification ->user_id = $booking->ad->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === TimelineStatus::$REQUEST_CANCELLED){
            $notification = new Notification();
            $notification ->title = $booking->user->first_name." cancelled booking request";
            $notification ->action = "/AdminLte/CurrentOrders";
            $notification ->user_id = $booking->ad->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === TimelineStatus::$ACCEPTED){
            $notification = new Notification();
            $notification ->title = $booking->ad->user->first_name." accepted your booking request";
            $notification ->action = "/AdminLte/currentBookings";
            $notification ->user_id = $booking->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === TimelineStatus::$REJECTED){
            $notification = new Notification();
            $notification ->title = $booking->ad->user->first_name." rejected your booking request";
            $notification ->action = "/AdminLte/currentBookings";
            $notification ->user_id = $booking->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === TimelineStatus::$DELIVERED){
            $notification = new Notification();
            $notification ->title = $booking->ad->user->first_name." have posted the product";
            $notification ->action = "/AdminLte/currentBookings";
            $notification ->user_id = $booking->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === TimelineStatus::$RECEIVED){
            $notification = new Notification();
            $notification ->title = $booking->user->first_name." have received the product";
            $notification ->action = "/AdminLte/CurrentOrders";
            $notification ->user_id = $booking->ad->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === TimelineStatus::$RETURNED){
            $notification = new Notification();
            $notification ->title = $booking->user->first_name." have returned the product";
            $notification ->action = "/AdminLte/CurrentOrders";
            $notification ->user_id = $booking->ad->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === TimelineStatus::$COMPLETED){
            $notification = new Notification();
            $notification ->title = $booking->ad->user->first_name." have marked the order completed";
            $notification ->action = "/AdminLte/bookingsHistory";
            $notification ->user_id = $booking->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }

        return redirect()->back();
    }
}
