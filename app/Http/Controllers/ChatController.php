<?php

namespace ELends\Http\Controllers;

use ELends\Ad;
use ELends\Booking;
use ELends\Chat;
use ELends\Message;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Auth;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $id = null )
    {
        $ads = Ad::where('user_id',Auth::User()->id)->pluck('id');
        $bookings = Booking::whereIn('ad_id',$ads)->pluck('id');
        $chats = Chat::whereIn('booking_id',$bookings)->orWhere('user_id',Auth::User()->id)->latest()->get();
        return view('adminlte.chat')->with(compact('chats','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getmessages($id)
    {
        $messages = Message::where('chat_id',$id)->get();
        $messageshtml = "";

        foreach ($messages as $message)
        {
            if ($message->user_id == Auth::user()->id)
            {
                $messageshtml = $messageshtml."<div class=\"outgoing_msg\">
                        <div class=\"sent_msg\">
                            <p>".$message->message."</p>
                            <span class=\"time_date\">".$message->created_at."</span> </div>
                    </div>";
            }
            else
            {
                $messageshtml = $messageshtml."<div class=\"incoming_msg\">             
                        <div class=\"received_msg\">
                            <div class=\"received_withd_msg\">
                                <p>".$message->message."</p>
                                <span class=\"time_date\">".$message->created_at."</span></div>
                        </div>
                    </div>";
            }
        }
        return response()->json($messageshtml);
    }

    public function sendmessage($id,Request $request){
        $message = new Message();
        $message->message = $request->message;
        $message->chat_id = $id;
        $message->user_id = Auth::User()->id;
        $message->save();
        return "true";
    }

    public function chat($id)
    {
        $booking = Booking::findorfail($id);
        $chat = Chat::where('booking_id',$booking->id)->Where('user_id',$booking->user_id)->first();
       // dd($ad,$chat);
        if (!$chat)
        {
            $chat = new Chat();
            $chat->booking_id = $id;
            $chat->user_id = $booking->user_id;
            $chat->save();
        }
        return redirect()->to('/chat/'.$chat->id);
    }
}
