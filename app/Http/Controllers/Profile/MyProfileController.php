<?php

namespace ELends\Http\Controllers\Profile;

use ELends\Ad;
use ELends\Booking;
use ELends\Item;
use ELends\User;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class MyProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = Item::where('user_id',Auth::user()->id)->get();
        $item_id = $item->pluck('id');
        $itemcount = $item->count();

        $postedads = Ad::whereIn('item_id',$item_id)->get();
        $postedadcount = $postedads->count();

        $bookingads =Booking::where('user_id',Auth::user()->id)->get();
        $bookingadscount = $bookingads->count();




        $user = User::find(Auth::user()->id);
          return view('adminlte.myProfile')->with(compact('user','itemcount','postedadcount','bookingadscount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_info(Request $request)
    {

        $user = User::find(Auth::user()->id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->save();

        return redirect('/AdminLte/myProfie');



    }

    public function update_detail(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->city = $request->Address;

        if($request->has('lat')){
            $user->lat = $request->lat;
            $user->lng = $request->lng;
        }
        $user->phone = $request->phone;
        if ($request->hasFile('picture'))
        {
            $photo = $request->picture;
            $path = '/storage/Profile_images/';
            $name = Auth::User()->email.'-'.$photo->getClientOriginalName();
            $photo->move(public_path().$path , $name);
            $user->picture = $path.$name;
        }
        $user->save();
        return redirect('/AdminLte/myProfie');
    }

    public function update_password(Request $request)
    {
        $this->validate($request,[
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = User::find(Auth::user()->id);

        if ( !Hash::check($request->current_password, $user->password)) {
            $request->session()->flash('Current_Password_Error', 'Your Current Password not matched with our record');
            return back();
        } else {

            $user->password = Hash::make($request->password);
            $user->save();
            $request->session()->flash('Current_Password_Success', 'Your Current Password Updated Successfully');
            return redirect('/AdminLte/myProfie');

        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
