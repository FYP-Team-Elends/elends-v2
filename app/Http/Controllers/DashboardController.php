<?php

namespace ELends\Http\Controllers;

use ELends\Booking;
use ELends\Charts\LineChart;
use ELends\Interest;
use ELends\Item;
use ELends\RecentAd;
use ELends\User;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use ELends\AD;
use Auth;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        //$this->middleware('verified');
    }

    public function index()
    {
        $item = Item::where('user_id',Auth::user()->id)->get();
        $item_id = $item->pluck('id');
        $itemcount = $item->count();

        $postedads = Ad::whereIn('item_id',$item_id)->get();
        $postedadcount = $postedads->count();

        $bookingads =Booking::where('user_id',Auth::user()->id)->where('request_status','request-accepted')->get();
        $bookingadscount = $bookingads->count();

        $reqbookingads =Booking::whereIn('item_id',$item_id)->where('request_status','request-sent')->get();
        $reqbookingadscount = $reqbookingads->count();

        $interestedCategories = Interest::where('user_id',Auth::user()->id)->get();
        $recentads = RecentAd::where('user_id',Auth::user()->id)->get();

        $intAds = array();

        foreach($interestedCategories as $cat){

//          $ads = Ad::where('item_id', Item::where('category_id', $cat)->pluck('id'))->get();

            $items = Item::where('category_id', $cat->category_id)->pluck("id");

            $ads = Ad::take(5)->whereIn('item_id', $items)->get();

            foreach($ads as $ad){
                $intAds[] =$ad;
            }
        }

        $adIds = Ad::where('user_id', \Illuminate\Support\Facades\Auth::id())->pluck('id');
        $bookingCount = Booking::whereIn('ad_id', $adIds)->where('request_status', 'request-sent')->get();

        $labels = array();
        $line = array();


        foreach ($bookingCount as $cnt){
            if(!in_array($cnt->created_at->toDateString(), $labels)){
                $count = Booking::whereDate('created_at', $cnt->created_at)->count();
                $labels[] = $cnt->created_at->toDateString();
                $line[] = $count;
            }
        }

        $chart = new LineChart();

        $chart->labels($labels);
        $chart->dataset('Requests Received', 'line', $line)->color('green');

        return view('home')->with(compact('itemcount','postedadcount','bookingadscount','reqbookingadscount','intAds','recentads','chart','chart1'));
    }
}
