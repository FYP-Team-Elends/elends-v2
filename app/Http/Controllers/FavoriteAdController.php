<?php

namespace ELends\Http\Controllers;

use ELends\FavoriteAd;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Auth;
use function Sodium\add;

class FavoriteAdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $favads = FavoriteAd::where('user_id',Auth::user()->id)->get();
        $favads = $favads->reverse();
        return view('adminlte.wishlist')->with(compact('favads'));
    }

    public function removeFavAd($id)
    {

        $favads = FavoriteAd::findorfail($id);

        $favads->delete();

        return redirect()->to(url('/AdminLte/myWishlist'));
    }

     public function addFavorites($id)
     {

         $favad = FavoriteAd::where('ad_id', $id)->get();
         if (count($favad) == 0)
         {
             $addfavad = new FavoriteAd();
             $addfavad->user_id = Auth::user()->id;
             $addfavad->ad_id = $id;
             $addfavad->save();
         }

         return redirect()->to('/AdminLte/myWishlist');

     }
}
