<?php

namespace ELends\Http\Controllers;

use ELends\HallSchedule;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;

class HallScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\HallSchedule  $hallSchedule
     * @return \Illuminate\Http\Response
     */
    public function show(HallSchedule $hallSchedule)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\HallSchedule  $hallSchedule
     * @return \Illuminate\Http\Response
     */
    public function edit(HallSchedule $hallSchedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\HallSchedule  $hallSchedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HallSchedule $hallSchedule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\HallSchedule  $hallSchedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(HallSchedule $hallSchedule)
    {
        //
    }
}
