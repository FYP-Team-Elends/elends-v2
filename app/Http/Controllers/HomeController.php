<?php

namespace ELends\Http\Controllers;

use ELends\Category;
use ELends\Interest;
use ELends\RecentAd;
use Illuminate\Http\Request;
use ELends\AD;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the ELendslication dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->hasRole('Admin')) {
            return redirect('/Admin/DashBoard');
        }
        else if (Auth::user()->hasRole('ServiceProvider')) {
            return redirect('ServiceProvider/services');
        } else {
            return redirect('/DashBoard');
        }
    }
}
