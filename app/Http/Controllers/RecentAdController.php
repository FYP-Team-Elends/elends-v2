<?php

namespace ELends\Http\Controllers;

use ELends\RecentAd;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;

class RecentAdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\RecentAd  $recentAd
     * @return \Illuminate\Http\Response
     */
    public function show(RecentAd $recentAd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\RecentAd  $recentAd
     * @return \Illuminate\Http\Response
     */
    public function edit(RecentAd $recentAd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\RecentAd  $recentAd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RecentAd $recentAd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\RecentAd  $recentAd
     * @return \Illuminate\Http\Response
     */
    public function destroy(RecentAd $recentAd)
    {
        //
    }
}
