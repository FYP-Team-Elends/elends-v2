<?php

namespace ELends\Http\Controllers;

use ELends\Notification;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }

    public function getAllNotifications(){
        $notifications = Notification::where([['user_id', Auth::id()],['isDeleted', 0]])->get();
        $notifications = $notifications->reverse();
        return view('adminlte.notifications')->with(compact('notifications'));
    }

    public function redirectToAction(Request $request){
        $notification = Notification::find($request->notification_id);
        $notification ->isSeen = true;
        $notification ->save();
        return redirect($notification->action);
    }

    public function clearNotifications(){
        Notification::where('user_id', Auth::id())->update(['isDeleted'=>1]);
        return redirect()->back();
    }
}
