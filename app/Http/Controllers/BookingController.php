<?php

namespace ELends\Http\Controllers;

use ELends\Ad;
use ELends\Booking;
use ELends\Item;
use ELends\TimelineStatus;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function ordersHistory()
    {
        $items = Item::where('user_id',Auth::user()->id)->pluck('id');
        $orders = Booking::whereIn('item_id',$items)->where('timeline_status',TimelineStatus::getIntStatus(TimelineStatus::$COMPLETED))->get();
        $orders = $orders->reverse();
        return View('adminlte.ordersHistory')->with(compact('orders'));
    }

    public function CancelOrder($id)
    {
        $order = Booking::findorfail($id);
        $order->request_status = 'booking-cancelled';
        $order->save();

        return redirect()->to(url('/AdminLte/myOrders/viewOrders'));
    }


    public function CancelmyBooking($id)
    {
        $order = Booking::findorfail($id);
        $order->request_status = 'booking-cancelled';
        $order->save();

        return redirect()->to(url('/AdminLte/myBookings'));
    }


    public function myBookings()
    {
        $bookings = Booking::where('user_id',Auth::user()->id)->where('timeline_status','<',TimelineStatus::getIntStatus(TimelineStatus::$COMPLETED))->latest()->get();
        return View('adminlte.currentBookings')->with(compact('bookings'));
    }

    public function bookingsHistory(){
        $bookings = Booking::where('user_id',Auth::user()->id)->where('timeline_status','=',TimelineStatus::getIntStatus(TimelineStatus::$COMPLETED))->latest()->get();
        return View('adminlte.bookingsHistory')->with(compact('bookings'));

    }

    public function CurrentOrders()
    {
        $item = Item::where('user_id',Auth::user()->id)->pluck('id');
        $mybookings = Booking::whereIn('item_id',$item)->where('timeline_status','<',TimelineStatus::getIntStatus(TimelineStatus::$COMPLETED))->latest()->get();
        return View('adminlte.currentOrders')->with(compact('mybookings'));
    }

    public function ConfirmBooking($id)
    {

        $booked = Booking::findorfail($id);
        $booked->request_status = 'request-accepted';
        $booked->save();
        return redirect()->to(url('/AdminLte/myOrders/BookingRequests'));
    }

    public function CancelBooking($id)
    {
        $booked = Booking::findorfail($id);
        $booked->request_status = 'request-rejected';
        $booked->save();
        return redirect()->to(url('/AdminLte/myOrders/BookingRequests'));
    }

}
