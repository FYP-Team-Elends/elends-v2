<?php

namespace ELends\Http\Controllers;

use ELends\MarrigeHall;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;

class MarrigeHallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\MarrigeHall  $marrigeHall
     * @return \Illuminate\Http\Response
     */
    public function show(MarrigeHall $marrigeHall)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\MarrigeHall  $marrigeHall
     * @return \Illuminate\Http\Response
     */
    public function edit(MarrigeHall $marrigeHall)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\MarrigeHall  $marrigeHall
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MarrigeHall $marrigeHall)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\MarrigeHall  $marrigeHall
     * @return \Illuminate\Http\Response
     */
    public function destroy(MarrigeHall $marrigeHall)
    {
        //
    }
}
