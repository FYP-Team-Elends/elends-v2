<?php

namespace ELends\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function welcome()
    {
        $catagories = \ELends\Category::all();
        $ads = \ELends\Ad::latest()->get();
        return view('welcome')->with(compact('catagories','ads'));
    }
}
