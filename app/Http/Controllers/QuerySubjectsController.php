<?php

namespace ELends\Http\Controllers;

use ELends\QuerySubjects;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;

class QuerySubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\QuerySubjects  $querySubjects
     * @return \Illuminate\Http\Response
     */
    public function show(QuerySubjects $querySubjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\QuerySubjects  $querySubjects
     * @return \Illuminate\Http\Response
     */
    public function edit(QuerySubjects $querySubjects)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\QuerySubjects  $querySubjects
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuerySubjects $querySubjects)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\QuerySubjects  $querySubjects
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuerySubjects $querySubjects)
    {
        //
    }
}
