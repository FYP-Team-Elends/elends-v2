<?php

namespace ELends\Http\Controllers\ServiceProvider;


use ELends\Charts\LineChart;
use ELends\Chat;
use ELends\Notification;
use ELends\Service;
use ELends\ServiceBooking;
use ELends\ServiceCategory;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class SPServicesController extends Controller
{
    public function RegisterAs($ServiceProvider)
    {
        session(['RegisterRequest' => $ServiceProvider]);
        return redirect()->route('register');
    }

    public function Services()
    {
        $services = Service::where('user_id',Auth::user()->id)->get();
        $categories = ServiceCategory::all();
        return view('ServiceProvider.services')->with(compact('services','categories'));
    }
    public function ServicesRequests()
    {
        $services = Service::where('user_id',Auth::user()->id)->pluck('id');
        $servicebookings = ServiceBooking::whereIn('service_id',$services)->where('request_status','requested')->get();
        $title = "Booking Requests";
        return view('ServiceProvider.serviceRequest')->with(compact('servicebookings'))->with(compact('title'));
    }
    public function BookedServices()
    {
        $services = Service::where('user_id',Auth::user()->id)->pluck('id');
        $mybookings = ServiceBooking::whereIn('service_id',$services)->where('request_status','accepted')->get();
        $title = "Current Bookings";
        return view('ServiceProvider.currentBooking')->with(compact('mybookings'))->with(compact('title'));
    }
    public function CompletedServices()
    {
        $services = Service::where('user_id',Auth::user()->id)->pluck('id');
        $mybookings = ServiceBooking::whereIn('service_id',$services)->where('request_status','completed')->get();
        $title = "Completed Orders";
        return view('ServiceProvider.bookingsHistory')->with(compact('mybookings'))->with(compact('title'));
    }
    public function RejectedServices()
    {
        $services = Service::where('user_id',Auth::user()->id)->pluck('id');
        $servicebookings = ServiceBooking::whereIn('service_id',$services)->where('request_status','rejected')->get();
        $title = "Current Bookings";
        return view('ServiceProvider.serviceRequest')->with(compact('servicebookings'));
    }

    public function dashboard(){

        $labels = array();
        $entries = array();

        $Services=Service::where('user_id',Auth::id())->get();
        $Services_count=$Services->count();

        $Request_cancelled=ServiceBooking::where('request_status','rejected')->get();
        $Request_cancelled_count=$Request_cancelled->count();

        $Request_received=ServiceBooking::where('request_status','requested')->get();
        $Request_received_count=$Request_received->count();

        $Complete_Bookings=ServiceBooking::where('request_status', 'accepted')->get();
        $Complete_Bookings_count=$Complete_Bookings->count();


        foreach ($Request_received as $request_received){
            if(!in_array($request_received->created_at->toDateString(), $labels)){
                $count = ServiceBooking::whereDate('created_at', $request_received->created_at)->count();
                $labels[] = $request_received->created_at->toDateString();
                $entries[] = $count;
            }
        }


        foreach ($Complete_Bookings as $complete_Booking){
            if(!in_array($complete_Booking->created_at->toDateString(), $labels)){
                $count = ServiceBooking::whereDate('created_at', $complete_Booking->created_at)->count();
                $labels[] = $complete_Booking->created_at->toDateString();
                $entries[] = $count;
            }
        }

        $chart_for_booking_request = new LineChart();
        $chart_for_booking_request->labels([]);
        $chart_for_booking_request->dataset('Requests Received', 'line', [])->color('yellow');

        $chart_for_complete_booking = new LineChart();
        $chart_for_complete_booking->labels([]);
        $chart_for_complete_booking->dataset('Service Bookings', 'line', [])->color('green');


//        $chart2 = new LineChart();
//        $chart2->labels([]);
//        $chart2->dataset('Requests Received', 'line', [])->color('green');
//
//        $chart3 = new LineChart();
//        $chart3->labels([]);
//        $chart3->dataset('Requests Received', 'line', [])->color('green');


        return view('ServiceProvider.dashboard')->with(compact('chart_for_booking_request'))->with(compact('chart_for_complete_booking'))->with(
            compact('Services_count'))->with(compact('Complete_Bookings_count'))->with(compact('Request_received_count'))->with(
                compact('Request_cancelled_count'));
    }

    public function response(Request $request)
    {
        session()->put('URL' , URL::previous());
        $booking = ServiceBooking::find($request->id);
        $booking->request_status = $request->status;
        $booking->save();
        $statusStr = $request->status;

        if ($statusStr === "accepted"){
            $notification = new Notification();
            $notification ->title = $booking->service->user->first_name." accepted your booking request";
            $notification ->action = "/AdminLte/Service/Bookings";
            $notification ->user_id = $booking->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === "rejected"){
            $notification = new Notification();
            $notification ->title = $booking->service->user->first_name." have rejected your booking request";
            $notification ->action = "/AdminLte/Service/Requests";
            $notification ->user_id = $booking->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }elseif ($statusStr === "completed"){
            $notification = new Notification();
            $notification ->title = $booking->service->user->first_name." have delivered the service and marked booking as completed";
            $notification ->action = "/AdminLte/Service/History";
            $notification ->user_id = $booking->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();
        }

        return redirect()->to(url(session()->get('URL')));
    }

    public function addService(Request $request)
    {
        $this->validate($request,[
            'title'=>'required|string',
            'Categories'=>'required',
            'description'=>'required|string',
            /*'tel_no'=>'required|numeric',*/
            'loc'=>'required'
        ]);

        $service = new Service();
        $service->service_category_id = $request->Categories;
        $service->title = $request->title;
        $service->about = $request->description;
        $service->contact = $request->tel_no;
        $service->isFeatured = false;
        $service->location_name = $request->loc;
        $service->lat = $request->lat;
        $service->lng = $request->lng;
        $service->price_per_person = $request->rent;
        if ($request->hasFile('picture'))
        {
            $photo = $request->picture;
            $path = '/storage/Service_images/';
            $name = str_random('8').'-'.$photo->getClientOriginalName();
            $photo->move(public_path().$path , $name);
            $service->picture = $path.$name;
        }
        $service->user_id = Auth::user()->id;
        $service->save();
        return redirect()->to(url('ServiceProvider/myservices'));
    }

    public function updateService(Request $request)
    {
        $service = Service::findorfail($request->id);
        if ($service && $service->user_id == Auth::user()->id)
        {
            $service->service_category_id = $request->Categories;
            $service->title = $request->title;
            $service->about = $request->description;
            $service->contact = $request->tel_no;
            $service->location_name = $request->loc;
            if($request->lat){
                $service->lat = $request->lat;
                $service->lng = $request->lng;
            }
            $service->price_per_person = $request->rent;
            if ($request->hasFile('picture'))
            {
                $photo = $request->picture;
                $path = '/storage/Service_images/';
                $name = str_random('8').'-'.$photo->getClientOriginalName();
                $photo->move(public_path().$path , $name);
                $service->picture = $path.$name;
            }
            $service->save();
        }
        return redirect()->back();
    }
    public function deleteService($id)
    {
        $service = Service::findorfail($id);
        if ($service && $service->user_id == Auth::user()->id)
        {
            $service->delete();
        }
        return redirect()->to(url('ServiceProvider/myservices'));

    }

    public function ChangeStatus($id,$isActive)
    {
        $service = Service::findorfail($id);
        if ($service && $service->user_id == Auth::User()->id) {
            $service->isActive = $isActive;
            $service->save();
        }
    }
    public function ShowService($id){
        $service = Service::findorfail($id);
        $categories = ServiceCategory::all();
        return view('ServiceProvider.viewService')->with(compact('service', 'categories'));
    }
    public function delete($id)
    {
        $service = Service::findorfail($id);
        if($service)
        {
            $service->delete();
        }

        return redirect()->to(url('/ServiceProvider/myservices'))->with('success','Ad is deleted!');

    }

    public function changeAvailability(Request $request){
        $status = $request->get('status');
        $id = $request->get('id');
        $service = Service::find($id);
        $service->isActive = $status;
        $service->save();
        return redirect()->back();
    }


    public function chat($id)
    {
        $req_by = ServiceBooking::findorfail($id);
        $chat = Chat::where('service_booking_id',$id)->Where('user_id',$req_by->user_id)->first();
        if (!$chat)
        {
            $chat = new Chat();
            $chat->service_booking_id = $id;
            $chat->user_id = $req_by->user_id;
            $chat->save();
        }
        return redirect()->to('ServiceProvider/chats/'.$chat->id);
    }

    public function getchats($id = null)
    {
        $services = Service::where('user_id',Auth::User()->id)->pluck('id');
        $bookingrequest = ServiceBooking::whereIn('service_id',$services)->pluck('id');

        $chats = Chat::whereIn('service_booking_id',$bookingrequest)->orwhere('user_id',Auth::user()->id)->latest()->get();


        return view('ServiceProvider.chat')->with(compact('chats','id'));
    }

}
