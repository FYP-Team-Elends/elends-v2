<?php

namespace ELends\Http\Controllers\Auth;

use ELends\Http\Controllers\Controller;
use ELends\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     *
     * @override authenticated of AuthenticatesUsers
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        session()->put('Prev_URL' , URL::previous());
        return view('auth.login');
    }




    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    protected function authenticated(Request $request, $user)
    {
        $redirectTo = '/home';
        session()->forget('RegisterRequest');

        if(session()->get('Prev_URL') === request()->root().'/') {
            if ($user->hasRole('Admin'))
            {
                return redirect('/Admin/DashBoard');
            }
            elseif ($user->hasRole('ServiceProvider'))
            {
                   return redirect('ServiceProvider/services');
            } else
            {
                return redirect('/DashBoard');
            }
        }
        else
        {
            return redirect(session()->get('Prev_URL'));
        }

    }



    /**
     * Obtain the user information from twitter.
     *
     * @return Response
     */
    public function handleProviderCallback($service)
    {

        if ($service == 'twitter') {
            $user = Socialite::driver($service)->user();
        }else{
            $user = Socialite::driver($service)->stateless()->user();
        }
        $findUser = User::where('email',$user->getEmail())->first();
        if ($findUser) {
            Auth::login($findUser);
        }else{
            $first_last_name = explode(' ',$user->getName());
            $newUser = new User();
            $newUser->email = $user->getEmail();
            $newUser->first_name =$first_last_name[0];
            $newUser->last_name =$first_last_name[1];
            $newUser->password = bcrypt('Secret');
            $newUser->save();
            Auth::login($newUser);
        }
        return redirect('home');
    }
}
