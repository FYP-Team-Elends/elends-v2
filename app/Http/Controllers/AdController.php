<?php

namespace ELends\Http\Controllers;

use Couchbase\PasswordAuthenticator;
use ELends\Ad;
use ELends\Booking;
use ELends\Category;
use ELends\Interest;
use ELends\Item;
use ELends\Notification;
use ELends\RecentAd;
use ELends\TimelineStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class AdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $myads = Ad::where('user_id',Auth::user()->id)->get();
        $myads = $myads->reverse();
        return view('SubmitAd')->with(compact('myads'));
    }

    public  function feed()
    {
        $ads = Ad::where([['item.user_id', '!=', Auth::id()]])->get();
        return view('Feeds')->with(compact('ads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \ELends\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function show_all()
    {
        $myads = Ad::where('user_id',Auth::user()->id)->where('ad_type', 'item')->get();
        $myads = $myads->reverse();
        return view('adminlte.allAds')->with(compact('myads'));
    }

    public function show($id)
    {

        $ad = Ad::findorfail($id);

        if (Auth::check())
        {
            $recent = RecentAd::where('ad_id',$ad->id)->first();
            if (!$recent)
            {
                $recent = new RecentAd();
                $recent->user_id = Auth::user()->id;
                $recent->ad_id = $ad->id;
                $recent->save();
            }
        }

        return view('productview')->with(compact('ad'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \ELends\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function edit(Ad $ad)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \ELends\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ad $ad)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \ELends\Ad  $ad
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ad $ad)
    {
        $ad = Ad::find($ad);
        //check for correct user
        if(auth()->user()->id !== $ad->user_id){
            return redirect('/posts')->with('error','Unauthorized page');
        }
        if ($ad->c != 'noimage.jpg'){
            //Delete Image
            Storage::delete('public/cover_images/'.$ad->cover_image);
        }
        $ad->delete();
        return redirect('/posts')->with('success','Post Removed');

}

    public function showAd($id)
    {
        //

        $ad = Ad::find($id);

        return view('productview1')->with(compact('ad'));
    }

    public function showAd1($id)
    {
        //
        $ad = Ad::find($id);
//        dd($item);
        return view('productview1')->with(compact('ad'));
    }

    public function submitAd(Request $request){
        $this->validate($request,[
            'title' => 'required',
            'description'=> 'required',
            'expirydate' => 'required',
            'rent' => 'required',
        ]);
        $ad = new Ad();
        $ad->title = $request->title;
        $ad->user_id = auth()->user()->id;
        $ad->rent = $request->rent;
        $ad->item_id = $request->id;
        $ad->ad_type = 'item';
        $ad->is_featured = false;
        $ad->description = $request->description;
        $ad->expiry_date = $request->expirydate;
        $ad->save();
        return redirect('AdminLte/myAds/viewAds')->with('success','Ad Submitted Successfully!');

    }

    public function feeds(){
        $ads = Ad::where([
            ['user_id', '!=', Auth::id()]])->get();
        $ads = $ads->reverse();
        return View('adminlte.Feeds')->with(compact('ads'));
    }

    public function deleteAd($id){
        $ad = Ad::find($id);
        $ad->delete();
        return redirect('AdminLte/myAds/viewAds')->with('msg','Ad Deleted');
    }

    public function viewAd($id){
        $ad = Ad::find($id);
        if($ad->user_id!=Auth::id()){
            $category = $ad->item->category;
            if (Interest::where(['user_id'=> Auth::id(),'category_id'=> $category->id])->get()->count() == 0) {
                $interest = new Interest();
                $interest->user_id = Auth::id();
                $interest->category_id = $category->id;
                $interest->save();
            }
            if(RecentAd::where(['user_id' => Auth::id(),'ad_id'=> $ad->id])->get()->count() == 0){
                $recent = new RecentAd();
                $recent->user_id = Auth::user()->id;
                $recent->ad_id = $id;
                $recent->save();
            }
        }
        return View('adminlte.viewProduct')->with(compact('ad'));
    }

    public function sendItemBookingRequest(Request $request){
        $checkBooking = Booking::where([['user_id', Auth::id()],['item_id', $request->item_id],
            ['timeline_status',"!=", TimelineStatus::getIntStatus(TimelineStatus::$REQUEST_CANCELLED)]])->get();
        if($checkBooking->count() == 0){
            $booking = new \ELends\Booking();
            $booking -> user_id = Auth::user()->id;
            $booking -> request_status = 'request-sent';
            $booking -> ad_id =  $request->ad_id;
            $booking -> booked_at = now();
            $booking -> booked_for = now();
            $booking -> item_id = $request->item_id;
            $booking->save();

            $timelineStatus = new TimelineStatus();
            $timelineStatus->booking_id= $booking->id;
            $timelineStatus->status = TimelineStatus::$REQUEST;
            $timelineStatus->message = $request->msg;
            $timelineStatus->save();

            $item = Item::find($request->item_id);

            $notification = new Notification();
            $notification ->title = "You have received a booking request.";
            $notification ->action = "/AdminLte/CurrentOrders";
            $notification ->user_id = $item->user_id;
            $notification ->isSeen = false;
            $notification ->isDeleted = false;
            $notification ->save();

            return redirect('timeline/'.$booking->id)->with('msg', 'Booking request sent');
        }
        else{
            return redirect('timeline/'.$checkBooking[0]->id)->with('msg', 'Booking request already sent');
        }
    }

    public function searchAds(Request $request)
    {
        $query = $request->get('query');
        $isPriceSet = $request->has('rent');
        $category = Category::where('category', 'like', '%'.$query . '%')->pluck('id');
        $items = Item::whereIn('category_id',$category)->pluck('id');
        if ($request->Events == "All"&&$query)
        {
            if($isPriceSet){
                $ads1 = Ad::where([['title', 'like', '%' . $query . '%'],['rent', '>=',$request->get('rent')]])->orwhereIn('item_id',$items)->get();
            }else{
                $ads1 = Ad::where('title', 'like', '%' . $query . '%')->orwhereIn('item_id',$items)->get();
            }
        }
        else if($query){
            if($isPriceSet){
                $ads1 = Ad::where([['title', 'like', '%' . $query . '%'],['rent', '>=',$request->get('rent')]])->where('event_id',$request->Events)->orwhereIn('item_id',$items)->get();
            }else{
                $ads1 = Ad::where('title', 'like', '%' . $query . '%')->where('event_id',$request->Events)->orwhereIn('item_id',$items)->get();
            }
        }else{
            $ads1 = Ad::all();
        }

        $ads = array();

        if($request->has('lat')){
            foreach($ads1 as $ad){
                $user = $ad->user()->get()[0];
                $calculatedDist = ServiceController::computeDistance($request->lat, $request->lng
                    ,$user->lat, $user->lng);
                if($calculatedDist<=5){
                    $ads[]=$ad;
                }
            }

        }else{
            $ads = $ads1;
        }

        //$ads = $ads->reverse();
        $result = array('query'=>$query, 'ads'=>$ads);
        return View('adminlte.browseItem')->with(compact('result'));
    }


    public function EditAd($id){
        $ad = Ad::find($id);
        return view('adminlte.EditAd', with (compact('ad')));
    }

    public function updateAd(Request $request){
        $id=$request->id;
        $ad=Ad::find($id);
        $ad->title= $request->title;
        $ad->rent=$request->rent;
        $ad->description=$request->description;
        $ad->save();
        return redirect('/viewproduct/'.$id)->with('msg','Product Updated');
    }


}
