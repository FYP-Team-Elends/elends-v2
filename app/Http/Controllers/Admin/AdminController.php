<?php

namespace ELends\Http\Controllers\Admin;


use ELends\Booking;
use ELends\Charts\BarChart;
use ELends\Charts\LineChart;
use ELends\Charts\ReportChart;
use ELends\Item;
use ELends\Notification;
use ELends\Registeras;
use ELends\Role;
use ELends\Service;
use ELends\ServiceBooking;
use ELends\TimelineStatus;
use Illuminate\Http\Request;
use ELends\Http\Controllers\Controller;
use Auth;
use ELends\User;
use ELends\Ad;
use Illuminate\Support\Collection;

class AdminController extends Controller
{

    function random_color_part() {
        return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
    }

    function random_color() {
        return '#'.self::random_color_part() . self::random_color_part() . self::random_color_part();
    }

    public function index()
    {

        $item = User::all();
        $itemcount = $item->count();
        $ad = Ad::all();
        $postedadcount = $ad->count();
        $servcie = Service::all();
        $servicescount = $servcie->count();
        $booking = Booking::all();
        $reqbookingadscount = $booking->count();

        /*
         * chart
         * */

        $colors = array();
        $citycounts = User::groupBy('city')
            ->selectRaw('count(*) as total, city')
            ->get();

        foreach ($citycounts as $citycount)
        {
            $colors[] = self::random_color();
        }

        $chart = new BarChart();
        $chart->labels($citycounts->pluck('city'));
        $chart->dataset('User Created', 'bar',$citycounts->pluck('total'))
            ->backgroundcolor($colors);

        return view('Admin.Dashboard')->with(compact('itemcount','postedadcount','servicescount','reqbookingadscount' , 'chart'));
    }

    public function request(){

     $regusers = Registeras::Paginate(15);
//        ->simplePaginate(15)

     return view('Admin.accountRequest')->with(compact('regusers'));

    }

    public function create()
    {

        $citycount = User::groupBy('city')
            ->selectRaw('count(*) as total, city')
            ->get();

        $chart = new BarChart();
        $chart->labels($citycount->pluck('city'));
        $chart->dataset('User Created', 'bar',$citycount->pluck('total'))
            ->backgroundcolor(['#00ff00','#0003FF']);

        return view('Admin.create')->with(compact('chart'));


    }


    public function AllUser()
    {
        $users= User::all();
        $roles=Role::all();
        return view('Admin.AllUsers')->with(compact('users','roles'));
    }

    public function ManageRoles(Request $request){
        $user = User::findorfail($request->user_id);
        $roles=Role::all();
        foreach ($roles as $role)
        {
            if($request[$role->name] == 'on')
            {
                if (!$user->hasRole($role->name))
                {
                    $user->attachRole($role);
                }
            }else
            {
                if ($user->hasRole($role->name))
                {
                    $user->detachRole($role);
                }
            }
        }
        $response = [
            'success' => true,
            'message' => $user->first_name.' '.$user->last_name." updated Successfully"
        ];
        return $response;
    }

    public function AcceptRequest(Request $request)
    {
        try{
            $reg = Registeras::findorfail($request->request_id);
            $user = User::findorfail($reg->user_id);
            $serviceProvider = Role::where('name',$request->request_for)->first();
            $user->attachRole($serviceProvider);
            $reg->delete();
            $response = [
                'success' => true,
                'message' => $user->first_name.' '.$user->last_name." Accepted for ".$request->request_for." Successfully"
            ];
            return $response;
        }catch (\Exception $exception)
        {
            $response = [
                'success' => false,
                'message' => $exception->getMessage()
            ];
        }

    }
    public function RejectRequest(Request $request)
    {
        $reg = Registeras::findorfail($request->request_id);
        $user = User::findorfail($reg->user_id);
        $user->delete();
        $reg->delete();
        $response = [
            'success' => true,
            'message' => $user->first_name.' '.$user->last_name." Rejected and her Account was suspended Successfully"
        ];
        return $response;
    }

    public function Reports(Request $request){
        $bookings = null;
        if($request->has('fromDate')&&$request->has('toDate')){
            $dateTo = $request->toDate;
            $dateFrom = $request->fromDate;
            $bookings = Booking::whereBetween('created_at', [date($dateFrom), date($dateTo)])->where('timeline_status', TimelineStatus::$COMPLETED)->get();
            $Service_Bookings = ServiceBooking::whereBetween('created_at', [date($dateFrom), date($dateTo)])->where('request_status', 'request-accepted')->get();
            $TotalUsers = User::whereBetween('created_at', [date($dateFrom), date($dateTo)])->get();
            $Posted_Ad=Ad::whereBetween('created_at', [date($dateFrom), date($dateTo)])->get();
            $Posted_Service=Service::whereBetween('created_at', [date($dateFrom), date($dateTo)])->get();
        }else{
            $bookings = Booking::whereMonth('created_at', '=', date('m'));
            $Service_Bookings = ServiceBooking::whereMonth('created_at', '=', date('m'));
            $TotalUsers = User::whereMonth('created_at', '=', date('m'));
            $Posted_Ad = User::whereMonth('created_at', '=', date('m'));
            $Posted_Service = User::whereMonth('created_at', '=', date('m'));
        }

        $labels = array();
        $entries = array();

        foreach ($bookings as $booking){
            if(!in_array($booking->created_at->toDateString(), $labels)){
                $count = Booking::whereDate('created_at', $booking->created_at)->count();
                $labels[] = $booking->created_at->toDateString();
                $entries[] = $count;
            }
        }

        foreach ($TotalUsers as $totalUser){
            if(!in_array($totalUser->created_at->toDateString(), $labels)){
                $count = User::whereDate('created_at', $totalUser->created_at)->count();
                $labels[] = $totalUser->created_at->toDateString();
                $entries[] = $count;
            }
        }

        foreach ($Service_Bookings as $service_Booking){
            if(!in_array($service_Booking->created_at->toDateString(), $labels)){
                $count = ServiceBooking::whereDate('created_at', $service_Booking->created_at)->count();
                $labels[] = $service_Booking->created_at->toDateString();
                $entries[] = $count;
            }
        }

        foreach ($Posted_Ad as $posted_ad){
            if(!in_array($posted_ad->created_at->toDateString(), $labels)){
                $count = Ad::whereDate('created_at', $posted_ad->created_at)->count();
                $labels[] = $posted_ad->created_at->toDateString();
                $entries[] = $count;
            }
        }

        foreach ($Posted_Service as $posted_service){
            if(!in_array($posted_service->created_at->toDateString(), $labels)){
                $count = Service::whereDate('created_at', $posted_service->created_at)->count();
                $labels[] = $posted_service->created_at->toDateString();
                $entries[] = $count;
            }
        }

//        $reportcounts = ServiceBooking::groupBy('request_status')
//            ->selectRaw('count(*) as total, request_status')
//            ->get();


        $report_chart_for_bookings = new ReportChart();
        $report_chart_for_bookings->labels($labels);
        $report_chart_for_bookings->dataset('Completed Orders', 'line', $entries)->backgroundcolor(['#008080']);

//        return view('Admin.GenerateReports')->with(compact('report_counts_for_booking','report_chart_for_bookings'));

        $report_chart_for_total_users = new ReportChart();
        $report_chart_for_total_users->labels($labels);
        $report_chart_for_total_users->dataset('Total Users', 'line', $entries)->backgroundcolor(['#0000FF']);

//        return view('Admin.GenerateReports')->with(compact('report_counreport_chart_for_bookingts_for_total_users','report_chart_for_total_users'));

        $report_chart_for_service_booking = new ReportChart();
        $report_chart_for_service_booking->labels($labels);
        $report_chart_for_service_booking->dataset('Completed Services', 'line', $entries)->backgroundcolor(['#800080']);

        $report_chart_for_posted_ad = new ReportChart();
        $report_chart_for_posted_ad->labels($labels);
        $report_chart_for_posted_ad->dataset('Posted Ads', 'line', $entries)->backgroundcolor(['#000000']);


        $report_chart_for_posted_service = new ReportChart();
        $report_chart_for_posted_service->labels($labels);
        $report_chart_for_posted_service->dataset('Posted Services', 'line', $entries)->backgroundcolor(['#800000']);

        return view('Admin.GenerateReports')->with(compact('report_chart_for_service_booking'))->with(
        compact('report_chart_for_total_users'))->with( compact('report_chart_for_bookings'))->with(
            compact('report_chart_for_posted_ad'))->with( compact('report_chart_for_posted_service'));

    }

    public function approveService(Request $request){
        $serviceId = $request->service_id;
        $service = Service::find($serviceId);
        $service->isApproved = true;
        $service->save();

        $notification = new Notification();
        $notification ->title = "Congratulations!! Your service is approved.";
        $notification ->action = "/ServiceProvider/myservices";
        $notification ->user_id = $service->user_id;
        $notification ->isSeen = false;
        $notification ->isDeleted = false;
        $notification ->save();

        return redirect()->back();
    }

    public function serviceRequests(){
        $services = Service::where('isApproved', 0)->latest()->get();
        return view('Admin.ServiceApprovalRequests')->with(compact('services'));
    }
    public function TotalItems(){
        $items=Item::all();
        return view('Admin.AllItems')->with(compact('items'));
    }

    public function TotalAds(){
        $ads=Ad::all();
        return view('Admin.AllAds')->with(compact('ads'));
    }

    public function TotalServices(){
        $services=Service::all();
        return view('Admin.AllServices')->with(compact('services'));
    }

    public function getUserProfile(Request $request){
        $id = $request->id;
        $user = User::find($id);
        $html = "<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
                    <div class=\"modal-dialog\">
                        <div class=\"modal-content\">
                            <div class=\"modal-header\">
                                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\"></button>
                                <h4 class=\"modal-title\" id=\"myModalLabel\">Profile</h4>
                            </div>
                            <div class=\"modal-body\">
                                <center>
                                    <img src=\"".$user->picture."\" width=\"140\" height=\"140\" border=\"0\" class=\"img-circle\"></a>
                                    <h3 class=\"media-heading\">'.$user->first_name.' '.$user->last_name.'</h3>
                                    <span class=\"label label-info\">Service Provider</span>
                                </center>
                                <hr>
                                <center>
                                    <p class=\"text-left\"><strong>Bio: </strong><br>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut sem dui, tempor sit amet commodo a, vulputate vel tellus.</p>
                                    <br>
                                </center>
                            </div>
                            <div class=\"modal-footer\">
                                <center>
                                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">I've heard enough about Joe</button>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
";
    }
}
