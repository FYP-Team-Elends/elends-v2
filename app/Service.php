<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function category()
    {
        return $this->belongsTo("ELends\ServiceCategory");
    }

    public function user()
    {
        return $this->belongsTo("ELends\User");
    }

    public function service_category(){
        return $this->belongsTo("ELends\ServiceCategory");
    }
}
