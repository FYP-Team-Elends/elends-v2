<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
   public function user()
   {
      return $this->belongsTo('ELends\User');
   }
    public function item()
    {
        return $this->belongsTo('ELends\Item');
    }

    public function ad()
    {
        return $this->belongsTo('ELends\Ad');
    }

}
