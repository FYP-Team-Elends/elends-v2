<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class SocialProvider extends Model
{
    //

    public function user(){
        return $this->belongsTo(User::class);
    }
}
