<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class RecentAd extends Model
{
    public function ad()
    {
        return $this->belongsTo('ELends\Ad');
    }
}
