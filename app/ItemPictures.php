<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class ItemPictures extends Model
{
    public function item()
    {
        return $this->belongsTo('ELends\Item');
    }
}
