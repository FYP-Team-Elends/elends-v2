<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class ServiceBooking extends Model
{
    public function service()
    {
        return $this->belongsTo('ELends\Service');
    }

    public function user()
    {
        return $this->belongsTo('ELends\User');
    }
}