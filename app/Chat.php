<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public function user()
    {
        return $this->belongsTo('ELends\User');
    }

   /* public function ad()
    {
        return $this->belongsTo('ELends\Ad');
    }*/

    public function booking()
    {
        return $this->belongsTo('ELends\Booking');
    }

    public function messages()
    {
        return $this->hasMany('ELends\Message');
    }

    public function servicebooking()
    {
        return $this->belongsTo('ELends\ServiceBooking');
    }


}
