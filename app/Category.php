<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    public function interest()
    {
        return $this->hasMany('ELends\Interest');
    }

    public function items()
    {
        return $this->hasMany('ELends\Item');
    }
}
