<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class Interest extends Model
{

    public function category()
    {
        return $this->belongsTo('ELends\Category');
    }
}
