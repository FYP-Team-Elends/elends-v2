<?php

namespace ELends;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{

    public function user()
    {
        return $this->belongsTo('ELends\User');
    }

    public function itemm(){

        return $this->hasOne('ELends\Category');
		
    }

    public function item()
    {
    	return $this->belongsto('ELends\Item');
    }



    public function favad()
    {
    	return $this->belongsto('ELends\FavoriteAd');
    }

    public function recentad()
    {
        return $this->belongsto('ELends\RecentAd');
    }

}
