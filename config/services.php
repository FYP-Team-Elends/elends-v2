<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => ELends\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'facebook' => [
        'client_id' => '549725062149553',
        'client_secret' => 'e51c9b93308db2ce99ac6125ef78b2dc',
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],
    'google' => [
        'client_id' => '89122999966-p1oumssn8mtti8gittoj0elk1l08oust.apps.googleusercontent.com',
        'client_secret' => '8sqpJRA0NvHUcWebLTDspf8J',
        'redirect' => 'http://localhost:8000/login/google/callback',

    ],

    'twitter' => [
        'client_id' => 'lHhqOeyDiu6x4vTtaUmueaYtP',
        'client_secret' => 'fRmjLQExCQdkZhq5XMMkpiBHzSRbQdg2Op2Pxc2OTvSlEqVWVP',
        'redirect' => 'http://www.localhost/login/twitter/callback',
    ],

];
